"""Generator for calculating turning points questions."""

from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.polynomial_equation import PolynomialEquation


class TurningPointEquation(BaseEquation):
    """Questions of the form y = ax^2 + bx + c where you are asked the turning point."""

    def __init__(self, turning_point_x: int, turning_point_y: int, quadratic_multiplier: int) -> None:
        """The quadratic multiplier is the c value in an equation of the form: y = c(x - a)^2 + b"""
        self.turning_point = (turning_point_x, turning_point_y)
        self.expanded_equation = self._expand_equation(turning_point_x, turning_point_y, quadratic_multiplier)

    def readable_format(self, mathjax: bool = False) -> str:
        equation = f"y = {self.expanded_equation.format_readable()}"
        equation = self.add_conditional_mathjax_wrapper(equation, mathjax)
        return "What are the coordinates of the turning point for the following equation: " + equation

    def readable_answer(self, mathjax: bool = False) -> str:
        return self.add_conditional_mathjax_inline(str(self.turning_point), mathjax)

    @staticmethod
    def _expand_equation(turning_point_x: int, turning_point_y: int, quadratic_multiplier: int) -> PolynomialEquation:
        """Expand an equation c(x - a)^2 + b into the standard form dx^2 + ex + f."""
        inner_bracket = PolynomialEquation([-turning_point_x, 1])
        squared = inner_bracket * inner_bracket
        multiplied = squared * quadratic_multiplier
        result = multiplied + PolynomialEquation([turning_point_y])
        return result


class TurningPointFactory(BaseFactory):
    NUMBER_OF_DIFFICULTY_LEVELS = 3

    @property
    def title(self) -> str:
        return "Calculate the Turning Point"

    def description(self, _: bool) -> str:
        return "Find the turning points in the following squations"

    def generate(self, difficulty_level: int = 0) -> TurningPointEquation:
        if difficulty_level == 0:
            quadratic_multiplier = 1
        elif difficulty_level == 1:
            quadratic_multiplier = -1
        else:
            quadratic_multiplier = self.generate_non_zero_int(-5, 5, not_one=True)

        turning_point_x = self.generate_non_zero_int(-5, 5)
        turning_point_y = self.generate_non_zero_int(-10, 10)

        return TurningPointEquation(turning_point_x, turning_point_y, quadratic_multiplier)
