"""Generate simultaneous equations."""

import random

from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.decorators import mathjax


class SimultaneousEquation(BaseEquation):
    """Stores a single generated simultaneous equation."""

    def __init__(
        self, x: int, y: int, x1_coefficient: int, y1_coefficient: int, x2_coefficient: int, y2_coefficient: int
    ) -> None:
        self.x = x
        self.y = y
        self.x1_coefficient = x1_coefficient
        self.x2_coefficient = x2_coefficient
        self.y1_coefficient = y1_coefficient
        self.y2_coefficient = y2_coefficient

    def readable_format(self, mathjax: bool = False) -> str:
        question = "Solve for x and y:\n"
        question += "\n".join(
            [
                self._format_linear_equation(self.x1_coefficient, self.y1_coefficient, self.result1, mathjax),
                self._format_linear_equation(self.x2_coefficient, self.y2_coefficient, self.result2, mathjax),
            ]
        )
        return question

    @mathjax(inline=True)
    def readable_answer(self, _: bool = False) -> str:
        return f"x = {self.x}; y = {self.y}"

    def display(self) -> None:
        print(self.readable_format())

    def mathjax(self) -> str:
        """Render the two equations in a MathJax friendly format for rendering in, e.g. a browser."""
        return self.readable_format(mathjax=True)

    @property
    def result1(self) -> int:
        return self.x1_coefficient * self.x + self.y1_coefficient * self.y

    @property
    def result2(self) -> int:
        return self.x2_coefficient * self.x + self.y2_coefficient * self.y

    def valid(self) -> bool:
        return self.x != 0 or self.y != 0


class SimultaneousEquationFactory(BaseFactory):
    def __init__(
        self,
        min_variable_value: int = -8,
        max_variable_value: int = 8,
        min_coefficient: int = -8,
        max_coefficient: int = 8,
    ) -> None:
        self.min_variable_value = min_variable_value
        self.max_variable_value = max_variable_value
        self.min_coefficient = min_coefficient
        self.max_coefficient = max_coefficient

    @property
    def title(self) -> str:
        return "Linear Simultaneous Equations"

    def description(self, mathjax: bool = False) -> str:
        if not mathjax:
            return "Find the values of x and y in the following pairs of equations"
        return r"Find the values of \(x\) and \(y\) in the following pairs of equations"

    def generate(self, _: int = 1) -> SimultaneousEquation:
        return SimultaneousEquation(
            random.randint(self.min_variable_value, self.max_variable_value),
            random.randint(self.min_variable_value, self.max_variable_value),
            self._generate_coefficient(),
            self._generate_coefficient(),
            self._generate_coefficient(),
            self._generate_coefficient(),
        )

    def _generate_coefficient(self) -> int:
        return self.generate_non_zero_int(self.min_coefficient, self.max_coefficient)
