"""Questions on the volume and surface area of compound shapes."""
import dataclasses
import enum
import random
from fractions import Fraction
from typing import Tuple

from point2d import Point2D

from images.cone_drawing import ShapeCone
from images.cylinder_drawing import ShapeCylinder
from images.sphere_drawing import ShapeSphere
from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.decorators import mathjax
from shared.pillow_image_equation import PillowImageEquation
from shared.pythagoras import find_pythagoras_hypotenuse
from shared.rectangle import Rectangle
from shared.units import Units


class CompoundShape(enum.Enum):
    ICE_CREAM = enum.auto()
    UPSIDE_DOWN_ICE_CREAM = enum.auto()
    PENCIL_UP = enum.auto()
    PENCIL_DOWN = enum.auto()
    RUBBER_UP = enum.auto()
    RUBBER_DOWN = enum.auto()

    def includes_hemisphere(self) -> bool:
        return self not in (CompoundShape.PENCIL_UP, CompoundShape.PENCIL_DOWN)

    def includes_cone(self) -> bool:
        return self not in (CompoundShape.RUBBER_UP, CompoundShape.RUBBER_DOWN)

    def includes_cylinder(self) -> bool:
        return self not in (CompoundShape.ICE_CREAM, CompoundShape.UPSIDE_DOWN_ICE_CREAM)


class QuestionType(enum.Enum):
    VOLUME = "volume"
    SURFACE_AREA = "surface area"


@dataclasses.dataclass
class CompoundShapeQuestion(PillowImageEquation):
    # The default image is square, bump that a bit as we're doing some slightly longer-thinner images.
    image_width: int = 150
    image_height: int = 225

    shape_type: CompoundShape = CompoundShape.ICE_CREAM
    question_type: QuestionType = QuestionType.VOLUME

    radius: int = 0
    cone_height: int = 0
    cylinder_height: int = 0
    units: Units = Units.CENTIMETERS

    def __post_init__(self) -> None:
        super().__post_init__()
        # Do some sanity checks.
        assert self.radius > 0
        assert self.cone_height > 0 or not self.shape_type.includes_cone()
        assert self.cylinder_height > 0 or not self.shape_type.includes_cylinder()

    def readable_format(self, mathjax: bool = False) -> str:
        return self._question_text(mathjax)

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        if self.question_type == QuestionType.VOLUME:
            value = self.volume_in_terms_of_pi()
            units_power = 3
        else:
            value = self.surface_area_in_terms_of_pi()
            units_power = 2
        return self._readable_text(value, units_power)

    @property
    def cone_diagonal(self) -> int:
        if self.cone_height == 0:
            return 0

        return find_pythagoras_hypotenuse(self.radius, self.cone_height)

    def _quantity(self, value: float, mathjax: bool) -> str:
        return self.add_conditional_mathjax_inline(f"{value}{self.units.value}", mathjax)

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        if self.shape_type == CompoundShape.ICE_CREAM:
            self._draw_ice_cream()
        elif self.shape_type == CompoundShape.UPSIDE_DOWN_ICE_CREAM:
            self._draw_upside_down_ice_cream()
        elif self.shape_type == CompoundShape.PENCIL_DOWN:
            self._draw_pencil_down()
        elif self.shape_type == CompoundShape.PENCIL_UP:
            self._draw_pencil_up()
        elif self.shape_type == CompoundShape.RUBBER_DOWN:
            self._draw_rubber_down()
        elif self.shape_type == CompoundShape.RUBBER_UP:
            self._draw_rubber_up()
        else:
            raise ValueError(f"Unrecognised compound shape: {self.shape_type}")

    def _draw_ice_cream(self) -> None:
        sphere = ShapeSphere(self._image, self.rectangle.top_left_square())
        sphere.draw_hemisphere(bottom=False, force_dotted_back_ellipse=True)

        self._draw_bottom_half_cone(sphere.space.centre_left)

    def _draw_upside_down_ice_cream(self) -> None:
        sphere = ShapeSphere(self._image, self.rectangle.bottom_right_square())
        sphere.draw_hemisphere(force_dotted_back_ellipse=True)

        self._draw_top_half_cone(sphere.space.centre_left)

    def _draw_pencil_up(self) -> None:
        cylinder = ShapeCylinder(self._image, self.rectangle.bottom_right_square())
        cylinder.draw_cylinder(force_dotted_back_ellipse=True)

        self._draw_top_half_cone(cylinder.space_no_ellipsis.top_left)

    def _draw_pencil_down(self) -> None:
        cylinder = ShapeCylinder(self._image, self.rectangle.top_left_square())
        cylinder.draw_cylinder()

        self._draw_bottom_half_cone(cylinder.space_no_ellipsis.bottom_left)

    def _draw_rubber_up(self) -> None:
        cylinder = ShapeCylinder(self._image, self.rectangle.bottom_right_square())
        cylinder.draw_cylinder(force_dotted_back_ellipse=True)

        sphere_space = Rectangle(cylinder.space_no_ellipsis.top_left, self.rectangle.top_right)
        sphere = ShapeSphere(self._image, sphere_space)
        sphere.draw_hemisphere_outside(bottom=False)

    def _draw_rubber_down(self) -> None:
        cylinder = ShapeCylinder(self._image, self.rectangle.top_left_square())
        cylinder.draw_cylinder(force_dotted_back_ellipse=False)

        sphere_space = Rectangle(cylinder.space_no_ellipsis.bottom_left, self.rectangle.bottom_right)
        sphere = ShapeSphere(self._image, sphere_space)
        sphere.draw_hemisphere_outside(bottom=True)

    def _draw_top_half_cone(self, bottom_left: Point2D) -> None:
        cone_space = Rectangle(bottom_left, self.rectangle.top_right)
        cone = ShapeCone(self._image, cone_space, include_ellipse=False, upwards=True)
        cone.draw_cone()

    def _draw_bottom_half_cone(self, top_left: Point2D) -> None:
        cone_space = Rectangle(top_left, self.rectangle.bottom_right)
        cone = ShapeCone(self._image, cone_space, include_ellipse=False, upwards=False)
        cone.draw_cone()

    def _question_text(self, mathjax: bool) -> str:
        if self.shape_type == CompoundShape.ICE_CREAM or self.shape_type == CompoundShape.UPSIDE_DOWN_ICE_CREAM:
            shape_names = "cone and hemisphere"
            first_height = ""
            second_height = f"and the height of the cone is {self._quantity(self.cone_height, mathjax)}, "
        elif self.shape_type == CompoundShape.PENCIL_DOWN or self.shape_type == CompoundShape.PENCIL_UP:
            shape_names = "cone and cylinder"
            first_height = f"the height of the cylinder is {self._quantity(self.cylinder_height, mathjax)}, "
            second_height = f"and the height of the cone is {self._quantity(self.cone_height, mathjax)}, "
        elif self.shape_type == CompoundShape.RUBBER_DOWN or self.shape_type == CompoundShape.RUBBER_UP:
            shape_names = "cylinder and hemisphere"
            first_height = ""
            second_height = f"and the height of the cylinder is {self._quantity(self.cylinder_height, mathjax)}, "
        else:
            raise ValueError(f"Unrecognised compound shape: {self.shape_type}")

        return (
            f"If the radius of both the {shape_names} is {self._quantity(self.radius, mathjax)}, "
            f"{first_height}{second_height}"
            f"what is the {self.question_type.value} of the shape?"
        )

    def surface_area_in_terms_of_pi(self) -> Fraction:
        area = Fraction(0)
        if self.shape_type.includes_cone():
            area += Fraction(self.radius * self.cone_diagonal)
        if self.shape_type.includes_cylinder():
            area += Fraction(self.radius * self.radius + 2 * self.radius * self.cylinder_height)
        if self.shape_type.includes_hemisphere():
            area += Fraction(2 * self.radius * self.radius)
        return area

    def volume_in_terms_of_pi(self) -> Fraction:
        volume = Fraction(0)
        if self.shape_type.includes_cone():
            volume += Fraction(self.radius * self.radius * self.cone_height, 3)
        if self.shape_type.includes_cylinder():
            volume += Fraction(self.radius * self.radius * self.cylinder_height)
        if self.shape_type.includes_hemisphere():
            volume += Fraction(2 * self.radius ** 3, 3)
        return volume

    def _readable_text(self, value_in_terms_of_pi: Fraction, units_power: int) -> str:
        if value_in_terms_of_pi.denominator == 1:
            number = str(value_in_terms_of_pi.numerator)
        else:
            number = f"\\frac{{{value_in_terms_of_pi.numerator}}}{{{value_in_terms_of_pi.denominator}}}"
        return f"{number} \\pi {self.units.value}^{units_power}"


class CompoundShapeFactory(BaseFactory):
    @property
    def title(self) -> str:
        return "Compound Shapes"

    def description(self, _: bool) -> str:
        return "Find the volume or surface area of the following compound shapes"

    def generate(self, _: int = 1) -> BaseEquation:
        shape_type: CompoundShape = random.choice(list(CompoundShape))
        question_type = random.choice(list(QuestionType))
        units = random.choice(list(Units))

        if question_type == QuestionType.SURFACE_AREA and shape_type.includes_cone():
            radius, cone_height = self._pick_pythag_sides()
        else:
            radius = random.randint(2, 9)
            cone_height = random.randint(2, 9) if shape_type.includes_cone() else 0
        cylinder_height = random.randint(2, 9) if shape_type.includes_cylinder() else 0

        return CompoundShapeQuestion(
            shape_type=shape_type,
            question_type=question_type,
            radius=radius,
            cone_height=cone_height,
            cylinder_height=cylinder_height,
            units=units,
        )

    @staticmethod
    def _pick_pythag_sides() -> Tuple[int, int]:
        # We want to shuffle the order, duplicate as a hacky but simple solution.
        small_triples = ((3, 4), (4, 3), (5, 12), (12, 5), (8, 15), (15, 8))
        return random.choice(small_triples)
