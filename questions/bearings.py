"""Questions on circle theorems."""
import dataclasses
import enum
import random
from typing import List

from images.circle_drawing import CircleDrawing
from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.pillow_image_equation import PillowImageEquation


class Unknown(enum.Enum):
    BEARING = "bearing"
    BACK_BEARING = "back-bearing"


@dataclasses.dataclass
class BearingsQuestion(PillowImageEquation):
    image_height: int = 200
    image_width: int = 200

    # For convenience, unlike the CircleDrawing, these are measured with 0 degrees pointing North.
    angle_degrees: int = 0
    unknown: Unknown = Unknown.BEARING

    def __post_init__(self) -> None:
        super().__post_init__()
        # We have to specify a default (parent has defaults), but this is really a required value.
        assert self.angle_degrees > 0
        assert self.angle_degrees not in (0, 180, 360)  # Gets too confusing!

    def readable_format(self, _: bool = False) -> str:
        if self.unknown == Unknown.BEARING:
            return f"What is the {self.unknown.value} of B from A?"
        return f"What is the {self.unknown.value} of A from B?"

    def readable_answer(self, mathjax: bool = False) -> str:
        answer = self.bearing_string if self.unknown == Unknown.BEARING else self.back_bearing_string
        if mathjax:
            answer += r"^\circ"
            return self.add_mathjax_wrapper(answer)
        return answer

    @property
    def angle_east_zero(self) -> int:
        return self._map_to_east_zero_degrees(self.angle_degrees)

    @property
    def back_bearing(self) -> int:
        if self.angle_degrees > 180:
            return 180 - (360 - self.angle_degrees)
        else:
            return 360 - (180 - self.angle_degrees)

    @property
    def bearing_string(self) -> str:
        return f"{self.angle_degrees:03d}"

    @property
    def back_bearing_string(self) -> str:
        return f"{self.back_bearing:03d}"

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        radius = min(self.rectangle.width, self.rectangle.height) // 2
        circle = CircleDrawing(self._image, self.rectangle.centre, radius, draw_circle=False)
        inner_circle = CircleDrawing(self._image, self.rectangle.centre, radius // 3, draw_circle=False)

        circle.draw_north_arrow()
        circle.label_origin(letter="A")

        circle.draw_origin_line(self.angle_east_zero)
        circle.draw_letter_circumference(self.angle_east_zero, "B")

        if self.angle_degrees < 180:
            # On the right hand-side, provide the 180-complement.
            circle.draw_dashed_origin_line(90)
            inner_circle.draw_label_arc(self.angle_east_zero, 90)
        elif self.angle_degrees > 270:
            # Near the top, provide the 360-complement.
            inner_circle.draw_label_arc(self.angle_east_zero, 270)
        else:
            # Angle is in the south-west, do the 180-complement on the left hand side.
            circle.draw_dashed_origin_line(90)
            inner_circle.draw_label_arc(90, self.angle_east_zero)

    @staticmethod
    def _map_to_east_zero_degrees(north_zero_degrees: int) -> int:
        return (north_zero_degrees - 90) % 360


class BearingsQuestionFactory(BaseFactory):
    NUMBER_OF_DIFFICULTY_LEVELS = 2

    @property
    def title(self) -> str:
        return "Bearings"

    def description(self, mathjax: bool) -> str:
        return "Calculate the bearings between two points"

    def generate(self, difficulty_level: int = 0) -> BaseEquation:
        unknown = Unknown.BEARING if difficulty_level == 0 else Unknown.BACK_BEARING
        return BearingsQuestion(angle_degrees=random.choice(self.sensible_degrees_values), unknown=unknown)

    @property
    def sensible_degrees_values(self) -> List[int]:
        """Pick values which produce a drawing which isn't too cramped => arc is < 25."""
        # We never draw an arc from 0 -> angle so can start small.
        values = list(range(5, 155, 5))
        values.extend(list(range(205, 335, 5)))
        return values
