"""Calculations involving compound interest and depreciation."""
import enum
import random

import names

from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory


class Unknown(enum.Enum):
    END_VALUE = enum.auto()
    NUMBER_OF_YEARS = enum.auto()
    START_VALUE = enum.auto()


class CompoundInterestQuestion(BaseEquation):
    """Represents a single question relating to compound interest."""

    def __init__(self, start_value: int, interest_rate: float, years: int, unknown: Unknown) -> None:
        self.start_value = start_value
        self.interest_rate = interest_rate
        self.years = years
        self.end_value = round(self.start_value * (1 + self.interest_rate / 100) ** self.years, 2)
        self.unknown = unknown

    def readable_format(self, _: bool = False) -> str:
        if self.unknown == Unknown.END_VALUE:
            if self.interest_rate > 0:
                return self._wording_interest_end_value()
            return self._wording_depreciation_end_value()
        elif self.unknown == Unknown.START_VALUE:
            if self.interest_rate > 0:
                return self._wording_interest_start_value()
            return self._wording_depreciation_start_value()
        else:
            if self.interest_rate > 0:
                return self._wording_interest_number_of_years()
            return self._wording_depreciation_number_of_years()

    def readable_answer(self, mathjax: bool = False) -> str:
        if self.unknown == Unknown.END_VALUE:
            string_answer = f"£{self.end_value:.2f}"
            string_answer = self.add_conditional_mathjax_inline(string_answer, mathjax)
            return string_answer
        elif self.unknown == Unknown.START_VALUE:
            string_answer = f"£{self.start_value:.2f}"
            string_answer = self.add_conditional_mathjax_inline(string_answer, mathjax)
            return string_answer
        else:
            return f"{self.years} years"

    def _wording_interest_end_value(self) -> str:
        name = names.get_first_name()
        return (
            f"{name} invests £{self.start_value:.2f} for {self.years} years at a compound interest rate of "
            f"{self.interest_rate}% per annum.  Calculate the value of the investment after {self.years} years."
        )

    def _wording_depreciation_end_value(self) -> str:
        name = names.get_first_name()
        asset = self._depreciation_asset()
        return (
            f"{name} has a {asset} worth £{self.start_value:.2f}.  Each year the value of the {asset} depreciates by "
            f"{-self.interest_rate}%.  Calculate how much the {asset} is worth after {self.years} years."
        )

    def _wording_interest_start_value(self) -> str:
        name = names.get_first_name()
        return (
            f"{name} invests in a bank account for {self.years} years at a compound interest rate of "
            f"{self.interest_rate}% per annum.  At the end of the period, {name}'s investment is worth "
            f"£{self.end_value:.2f}.  How much money did {name} start with?"
        )

    def _wording_depreciation_start_value(self) -> str:
        name = names.get_first_name()
        asset = self._depreciation_asset()
        return (
            f"{name} has a {asset} which depreciates by {-self.interest_rate}% each year.  At the end of 2020, the "
            f"{asset} is worth {self.end_value:.2f}.  How much was the {asset} worth and the end of "
            f"{2020 - self.years}?"
        )

    def _wording_interest_number_of_years(self) -> str:
        name = names.get_first_name()
        return (
            f"{name} invests £{self.start_value:.2f} in a bank account for a few years at a compound interest rate of "
            f"{self.interest_rate}% per annum.  At the end of the period, {name}'s investment is worth "
            f"£{self.end_value:.2f}.  How many years did {name} invest for?"
        )

    def _wording_depreciation_number_of_years(self) -> str:
        name = names.get_first_name()
        asset = self._depreciation_asset()
        return (
            f"{name} has a {asset} worth £{self.start_value:.2f}.  Each year the value of the {asset} depreciates by "
            f"{-self.interest_rate}%.  How many years until the {asset} is only worth {self.end_value:.2f}?"
        )

    @staticmethod
    def _depreciation_asset() -> str:
        return random.choice(["van", "car", "TV"])


class CompoundInterestFactory(BaseFactory):
    NUMBER_OF_DIFFICULTY_LEVELS = 3

    @property
    def title(self) -> str:
        return "Compound Interest and Depreciation"

    def description(self, _: bool = False) -> str:
        return "Calculate values involving compound interest and depreciation"

    def generate(self, difficulty_level: int = 0) -> BaseEquation:
        if difficulty_level == 0:
            return self.generate_easy()
        elif difficulty_level == 1:
            return self.generate_medium()
        elif difficulty_level == 2:
            return self.generate_hard()
        else:
            raise ValueError(f"Unsupported difficulty level: {difficulty_level}")

    def generate_easy(self) -> CompoundInterestQuestion:
        return self._generate_for_unknown(Unknown.END_VALUE)

    def generate_medium(self) -> CompoundInterestQuestion:
        return self._generate_for_unknown(Unknown.NUMBER_OF_YEARS)

    def generate_hard(self) -> CompoundInterestQuestion:
        return self._generate_for_unknown(Unknown.START_VALUE)

    @staticmethod
    def _generate_for_unknown(unknown: Unknown) -> CompoundInterestQuestion:
        start_value = 500 * random.randint(1, 10)
        years = random.randint(2, 5)

        # Want to cope with both interest and depreciation, so roll a dice to see which we do.
        if random.random() > 0.5:
            # Compound interest.  Small and positive.
            interest_rate = 0.5 * random.randint(3, 10)
        else:
            # Depreciation.  Larger and negative.
            interest_rate = -5 * random.randint(3, 8)

        return CompoundInterestQuestion(start_value, interest_rate, years, unknown)
