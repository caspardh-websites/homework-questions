"""Questions about expanding brackets."""

from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.decorators import mathjax
from shared.polynomial_equation import PolynomialEquation


class ExpandSingleBracketsEquation(BaseEquation):
    """Questions of the form a*(bx+c)."""

    def __init__(self, outside_term: int, bracket_poly: PolynomialEquation) -> None:
        self.outside_term = outside_term
        self.bracket_poly = bracket_poly

    def readable_format(self, _: bool = False) -> str:
        return f"Expand the following: {self._question_equation()}"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        answer = self.bracket_poly * PolynomialEquation(self.outside_term)
        return answer.format_readable()

    @mathjax()
    def _question_equation(self) -> str:
        equation = f"{self.outside_term}({self.bracket_poly.format_readable()})"
        return equation


class ExpandDoubleBracketsEquation(BaseEquation):
    """Questions of the form (ax+b)(cx+d)."""

    def __init__(self, bracket_one: PolynomialEquation, bracket_two: PolynomialEquation) -> None:
        self.bracket_one = bracket_one
        self.bracket_two = bracket_two

    def readable_format(self, _: bool = False) -> str:
        return f"Expand and simplify the following: {self._question_equation()}"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        answer = self.bracket_one * self.bracket_two
        return answer.format_readable()

    @mathjax()
    def _question_equation(self) -> str:
        equation = f"({self.bracket_one.format_readable()})({self.bracket_two.format_readable()})"
        return equation


class ExpandingBracketsFactory(BaseFactory):
    NUMBER_OF_DIFFICULTY_LEVELS = 3

    @property
    def title(self) -> str:
        return "Expanding brackets"

    def description(self, _: bool) -> str:
        return "Expand single and double brackets."

    def generate(self, difficulty_level: int = 0) -> BaseEquation:
        if difficulty_level == 0:
            return self.generate_easy()
        elif difficulty_level == 1:
            return self.generate_medium()
        elif difficulty_level == 2:
            return self.generate_hard()
        else:
            raise ValueError(f"Unsupported difficulty level: {difficulty_level}")

    def generate_easy(self) -> BaseEquation:
        return ExpandSingleBracketsEquation(
            self.generate_non_zero_int(-5, 5, not_one=True), PolynomialEquation.random(max_power=1),
        )

    def generate_medium(self) -> BaseEquation:
        return ExpandDoubleBracketsEquation(
            PolynomialEquation.random(max_power=1), PolynomialEquation.random(max_power=1),
        )

    def generate_hard(self) -> BaseEquation:
        return self.generate_medium()
