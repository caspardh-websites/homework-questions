"""Questions about factorising quadratics."""
import dataclasses

from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.decorators import mathjax
from shared.polynomial_equation import PolynomialEquation


@dataclasses.dataclass
class FactorisingQuadraticsEquation(BaseEquation):
    """Questions of the form factorising ax^2 + bx + c."""

    first_bracket: PolynomialEquation
    second_bracket: PolynomialEquation

    def readable_format(self, mathjax: bool = False) -> str:
        equation = self.add_conditional_mathjax_wrapper(f"{self.expanded}", mathjax)
        return f"Factorise the following: {equation}"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        return f"({self.first_bracket})({self.second_bracket})"

    @property
    def expanded(self) -> PolynomialEquation:
        return self.first_bracket * self.second_bracket


class FactorisingQuadraticFactory(BaseFactory):
    NUMBER_OF_DIFFICULTY_LEVELS = 3

    @property
    def title(self) -> str:
        return "Factorising Quadratics"

    def description(self, _: bool) -> str:
        return "Factorising quadratic equations into brackets."

    def generate(self, difficulty_level: int = 0) -> BaseEquation:
        if difficulty_level == 0:
            return self.generate_easy()
        elif difficulty_level == 1:
            return self.generate_medium()
        elif difficulty_level == 2:
            return self.generate_hard()
        else:
            raise ValueError(f"Unsupported difficulty level: {difficulty_level}")

    def generate_easy(self) -> FactorisingQuadraticsEquation:
        # Easy is when the x^2 is 1.
        first_constant = PolynomialEquation.random_coefficient(non_zero=True)
        # We don't want the second constant to be the negative of the first, that gives the special case of
        # x^2 - a^2 = 0, which is the "medium" case.
        second_constant = PolynomialEquation.random_coefficient(non_zero=True)
        while second_constant == -first_constant:
            second_constant = PolynomialEquation.random_coefficient(non_zero=True)

        first_poly = PolynomialEquation((first_constant, 1))
        second_poly = PolynomialEquation((second_constant, 1))
        return FactorisingQuadraticsEquation(first_poly, second_poly)

    def generate_medium(self) -> FactorisingQuadraticsEquation:
        # These are special cases when both are squares.
        linear_term = PolynomialEquation.random_coefficient(non_zero=True)
        constant_term = PolynomialEquation.random_coefficient(non_zero=True)
        first_poly = PolynomialEquation((constant_term, linear_term))
        second_poly = PolynomialEquation((-constant_term, linear_term))
        return FactorisingQuadraticsEquation(first_poly, second_poly)

    def generate_hard(self) -> FactorisingQuadraticsEquation:
        # Here we force at least one of the linear coefficients to be not-one.
        # Need to cope with the edge case of -x^2 for both which doesn't really work.
        first_poly = PolynomialEquation.random(1)
        while abs(first_poly.coef[1]) == 1:
            first_poly = PolynomialEquation.random(1)
        second_poly = PolynomialEquation.random(1)
        return FactorisingQuadraticsEquation(first_poly, second_poly)
