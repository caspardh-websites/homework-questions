"""Questions on circle theorems."""

import enum
import random

from shared.ct_base import CircleTheoremBase
from images.circle_drawing import CircleDrawing
from shared.circle_maths import Circle
from shared.base_factory import BaseFactory


class CircleTheoremCentreAngle(CircleTheoremBase):
    class Unknown(enum.Enum):
        CENTRE = enum.auto()
        CIRCUMFERENCE = enum.auto()

    def __init__(self, point_degrees: int, left_degrees: int, right_degrees: int, unknown: Unknown) -> None:
        super().__init__()
        self.point_degrees = point_degrees
        self.left_degrees = left_degrees
        self.right_degrees = right_degrees
        self.unknown = unknown

    def readable_format(self, mathjax: bool = False) -> str:
        if self.unknown == self.Unknown.CIRCUMFERENCE:
            return self._format_question_known_unknown("AOC", self.origin_angle, "ABC", mathjax)
        else:
            return self._format_question_known_unknown("ABC", self.edge_angle, "AOC", mathjax)

    def readable_answer(self, mathjax: bool = False) -> str:
        if self.unknown == self.Unknown.CIRCUMFERENCE:
            return self._format_answer("ABC", self.edge_angle, mathjax)
        else:
            return self._format_answer("AOC", self.origin_angle, mathjax)

    @property
    def edge_angle(self) -> int:
        """The angle between the lines at the circumference."""
        return Circle.angle(self.left_degrees, self.point_degrees, self.right_degrees)

    @property
    def origin_angle(self) -> int:
        """The angle around the origin."""
        return Circle.origin_angle(self.left_degrees, self.right_degrees)

    @property
    def is_valid(self) -> bool:
        # Otherwise you end up with fractional answers.
        return self.origin_angle % 2 == 0

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        radius = min(self.rectangle.width, self.rectangle.height) // 2
        circle = CircleDrawing(self._image, self.rectangle.centre, radius)
        circle.draw_origin_line(self.left_degrees)
        circle.draw_origin_line(self.right_degrees)
        circle.draw_chord(self.point_degrees, self.left_degrees)
        circle.draw_chord(self.point_degrees, self.right_degrees)
        circle.draw_letter_circumference(self.left_degrees, "A")
        circle.draw_letter_circumference(self.point_degrees, "B")
        circle.draw_letter_circumference(self.right_degrees, "C")
        circle.label_origin()


class CircleTheoremCentreAngleFactory(BaseFactory):
    @property
    def title(self) -> str:
        return "Circle Theorems: Centre Angle"

    def description(self, mathjax: bool) -> str:
        return "Find the other angle in these circles"

    def generate(self, _: int = 1) -> CircleTheoremCentreAngle:
        point = random.randint(0, 359)
        # Force the values past the diameter.
        anti_clockwise = point + random.randrange(110, 140, 2)
        clockwise = point - random.randrange(110, 140, 2)
        unknown = random.choice(list(CircleTheoremCentreAngle.Unknown))
        question = CircleTheoremCentreAngle(point, anti_clockwise, clockwise, unknown)
        # We know it should be valid because we've forced the lower angle to be a multiple of 2.
        assert question.is_valid
        return question
