"""Questions on using Pythagoras in 3D shapes."""
import math
import random

from images.cuboid_drawing import ShapeCuboid
from images.cylinder_drawing import ShapeCylinder
from images.triangular_prism_drawing import ShapeTriangularPrism
from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.decorators import mathjax
from shared.pillow_image_equation import PillowImageEquation
from shared.units import Units


class VolumeCuboid(PillowImageEquation):
    def __init__(self, width: float, depth: float, height: float, units: Units = Units.CENTIMETERS,) -> None:
        super().__init__()
        self.width = width
        self.depth = depth
        self.height = height
        self.units = units

        self.answer = width * depth * height

    def readable_format(self, _: bool = False) -> str:
        return "What is the volume of this cuboid?"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        return f"{self.answer}{self.units.value}^3"

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        drawing = ShapeCuboid(self._image, self.image_height // 2, self.bottom_left)
        drawing.draw_cube()
        drawing.label_bottom_side(self.width, self.units)
        drawing.label_depth(self.depth, self.units)
        drawing.label_height(self.height, self.units)


class VolumeTriangularPrism(PillowImageEquation):
    def __init__(self, base: float, height: float, depth: float, units: Units) -> None:
        super().__init__()
        self.base = base
        self.height = height
        self.depth = depth
        self.units = units

        self.answer = base * height * depth / 2

    def readable_format(self, _: bool = False) -> str:
        return "What is the volume of this triangular prism?"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        return f"{self.answer}{self.units.value}^3"

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        drawing = ShapeTriangularPrism(self._image, self.image_height * 2 // 3, self.bottom_left)
        drawing.draw_prism()
        drawing.draw_height()
        drawing.label_base(self.base, self.units)
        drawing.label_depth(self.depth, self.units)
        drawing.label_height(self.height, self.units)


class VolumeCylinder(PillowImageEquation):
    def __init__(self, radius: float, height: float, units: Units) -> None:
        super().__init__()
        self.radius = radius
        self.height = height
        self.units = units

        self.answer = math.pi * radius * radius * height

    def readable_format(self, _: bool = False) -> str:
        return "What is the volume of this cylinder?"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        return f"{self.answer: .3f}{self.units.value}^3"

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        # Allow a bit of padding on the right hand side to allow for printing the dimensions and units.
        space = self.rectangle.with_padding(right=10)
        drawing = ShapeCylinder(self._image, space)
        drawing.draw_cylinder()
        drawing.draw_radius()
        drawing.label_height_value(self.height, self.units)
        drawing.label_radius_value(self.radius, self.units)


class PrismVolumeFactory(BaseFactory):
    @property
    def title(self) -> str:
        return "Volume of Prisms"

    def description(self, _: bool) -> str:
        return "Find the volume of a prism"

    def generate(self, _: int = 1) -> BaseEquation:
        dice_roll = random.randint(1, 3)
        units = random.choice(list(Units))
        if dice_roll == 1:
            return VolumeCuboid(self._single_side(), self._single_side(), self._single_side(), units)
        elif dice_roll == 2:
            return VolumeTriangularPrism(self._single_side(), self._single_side(), self._single_side(), units)
        else:
            return VolumeCylinder(self._single_side(), self._single_side(), units)

    @staticmethod
    def _single_side() -> float:
        return random.randint(2, 10) / 2
