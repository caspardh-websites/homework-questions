"""Questions on the sine rule."""

import enum
import math
import random
from typing import Dict, Tuple, List

from shared.base_factory import BaseFactory
from shared.triangle_quantities import Angle, Side, TriangleQuantity
from shared.triangle_unknowns import TriangleUnknowns
from shared.units import Units


class QuestionType(enum.Enum):
    SAS = enum.auto()
    SSS = enum.auto()

    def get_knowns(self, starting_side: Side) -> Tuple[TriangleQuantity, TriangleQuantity, TriangleQuantity]:
        if self == QuestionType.SAS:
            return starting_side, starting_side.clockwise_angle, starting_side.clockwise_side
        elif self == QuestionType.SSS:
            return starting_side, starting_side.clockwise_side, starting_side.anti_clockwise_side
        raise RuntimeError(f"Unrecognised QuestionType: {self}")

    def get_possible_unknowns(self, starting_side: Side) -> List[TriangleQuantity]:
        if self == QuestionType.SAS:
            return [starting_side.anti_clockwise_side]
        elif self == QuestionType.SSS:
            return [angle for angle in Angle]
        raise RuntimeError(f"Unrecognised QuestionType: {self}")


class CosineRuleEquation(TriangleUnknowns):
    """Handles a cosine rule question."""

    def __init__(
        self, given_values: Dict[TriangleQuantity, float], unknown: TriangleQuantity, units: Units = Units.CENTIMETERS,
    ) -> None:
        super().__init__(given_values, unknown, units)
        self._calculated_values = self.known_values.copy()
        self._solve_triangle()

    def get_value(self, key: TriangleQuantity) -> float:
        return self._calculated_values[key]

    def _solve_triangle(self) -> None:
        # If we're missing a side, find that first to make the rest of the code simpler.
        self._find_final_side()

        # Now we can walk through and solve for each angle we don't have.
        for angle in Angle:  # type: Angle
            if angle in self._calculated_values:
                continue
            opposite_side = self._calculated_values[angle.corresponding_side]
            # The order of these two is irrelevant
            side_a = self._calculated_values[angle.clockwise_side]
            side_b = self._calculated_values[angle.anti_clockwise_side]
            self._calculated_values[angle] = self.solve_for_angle(opposite_side, side_a, side_b)

    def _find_final_side(self) -> None:
        """Find the side in the SAS case (Side-Angle-Side)"""
        given_sides = [item for item in self._calculated_values if isinstance(item, Side)]
        if len(given_sides) == 3:
            return
        given_angles = [item for item in self._calculated_values if isinstance(item, Angle)]
        assert len(given_sides) == 2
        assert len(given_angles) == 1
        given_angle = given_angles[0]
        angle_value = self._calculated_values[given_angle]
        known_side_a = self._calculated_values[given_angle.clockwise_side]
        known_side_b = self._calculated_values[given_angle.anti_clockwise_side]
        missing_side_value = self.solve_for_side(angle_value, known_side_a, known_side_b)
        self._calculated_values[given_angle.corresponding_side] = missing_side_value

    @staticmethod
    def solve_for_side(angle_c: float, side_a: float, side_b: float) -> float:
        return math.sqrt(side_a ** 2 + side_b ** 2 - 2 * side_a * side_b * math.cos(math.radians(angle_c)))

    @staticmethod
    def solve_for_angle(opposite_side: float, side_a: float, side_b: float) -> float:
        fraction = (side_a ** 2 + side_b ** 2 - opposite_side ** 2) / (2 * side_a * side_b)
        angle = math.degrees(math.acos(fraction))
        if angle <= 0:
            raise ValueError(f"Invalid triangle, side isn't positive: {angle}")
        return angle


class CosineRuleFactory(BaseFactory):
    @property
    def title(self) -> str:
        return "Cosine Rule"

    def description(self, mathjax: bool) -> str:
        return "Find angles and sides in a triangle"

    def generate(self, _: int = 1) -> CosineRuleEquation:
        while True:
            try:
                return self._generate_single()
            except ValueError:
                # This is typically because we've generated a triangle that doesn't add up.  Just cheat and try again!
                continue

    @staticmethod
    def _generate_single() -> CosineRuleEquation:
        units = random.choice(list(Units))  # type: Units
        starting_side = random.choice(list(Side))  # type: Side
        question_type = random.choice(list(QuestionType))  # type: QuestionType
        knowns_keys = question_type.get_knowns(starting_side)
        knowns = {}  # type: Dict[TriangleQuantity, float]
        first_side = 0.0
        for key in knowns_keys:
            if isinstance(key, Angle):
                knowns[key] = random.randint(40, 120)
            elif isinstance(key, Side):
                # If we've already done a side, pick one that is close, to ensure a valid triangle.
                if first_side == 0:
                    # Allow floats to 1dp to make things a bit more interesting.
                    knowns[key] = random.randint(1, 50) / 10
                    first_side = knowns[key]
                else:
                    knowns[key] = round(first_side * random.uniform(0.6, 1.4), 1)
            else:
                raise RuntimeError(f"Unexpected key type: {type(key)}")
        unknown = random.choice(question_type.get_possible_unknowns(starting_side))
        return CosineRuleEquation(knowns, unknown, units)
