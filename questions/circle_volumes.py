"""Questions on the volume and surface of circle based 3D shapes, e.g. spheres, cones, etc."""
import dataclasses
import enum
import random
from fractions import Fraction
from typing import Tuple, Optional

from images.cone_drawing import ShapeCone
from images.cylinder_drawing import ShapeCylinder
from images.sphere_drawing import ShapeSphere
from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.pillow_image_equation import PillowImageEquation
from shared.pythagoras import find_pythagoras_hypotenuse
from shared.units import Units


class QuestionType(enum.Enum):
    VOLUME = "volume"
    SURFACE_AREA = "surface area"


class Unknown(enum.Enum):
    RADIUS = "radius"
    HEIGHT = "height"
    TOTAL = "total"


class Shape(enum.Enum):
    SPHERE = "sphere"
    HEMISPHERE = "hemisphere"
    CONE = "cone"
    CYLINDER = "cylinder"

    @property
    def has_height(self) -> bool:
        return self in (Shape.CONE, Shape.CYLINDER)

    def random_unknown(self) -> Unknown:
        unknown: Unknown = random.choice(list(Unknown))
        while unknown == Unknown.HEIGHT and not self.has_height:
            unknown = random.choice(list(Unknown))
        return unknown


@dataclasses.dataclass
class CircleVolumeQuestion(PillowImageEquation):
    # The default image is square, bump that a bit as we're doing some slightly longer-thinner images.
    image_width: int = 150
    image_height: int = 225

    shape_type: Shape = Shape.SPHERE
    question_type: QuestionType = QuestionType.VOLUME
    unknown: Unknown = Unknown.TOTAL
    show_radius_as_height: bool = False

    radius: int = 0
    height: int = 0
    units: Units = Units.CENTIMETERS

    def __post_init__(self) -> None:
        super().__post_init__()
        # Do some sanity checks.
        assert self.radius > 0
        assert self.height > 0 or self.shape_type not in (Shape.CONE, Shape.CYLINDER)
        assert not self.show_radius_as_height or (self.shape_type.has_height and self.radius_in_terms_of_height)

    def readable_format(self, mathjax: bool = False) -> str:
        return self._question_text(mathjax)

    def readable_answer(self, mathjax: bool = False) -> str:
        if self.unknown == Unknown.RADIUS:
            return self.radius_text(mathjax)
        if self.unknown == Unknown.HEIGHT:
            return self.height_text(mathjax)
        if self.unknown == Unknown.TOTAL:
            return self.total_value_text(mathjax)
        raise ValueError(f"Unrecognised unknown type: {self.unknown}")

    @property
    def cone_diagonal(self) -> int:
        if self.height == 0:
            return 0

        return find_pythagoras_hypotenuse(self.radius, self.height)

    @property
    def radius_in_terms_of_height(self) -> Fraction:
        if self.radius % self.height == 0:
            return Fraction(self.radius // self.height)
        if self.height % self.radius == 0:
            return Fraction(1, self.height // self.radius)
        raise ValueError(f"One of radius ({self.radius}) or height ({self.height}) must be a multiple of the other")

    def _quantity(self, value: float, mathjax: bool) -> str:
        return self.add_conditional_mathjax_inline(f"{value}{self.units.value}", mathjax)

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        if self.shape_type == Shape.SPHERE:
            sphere = ShapeSphere(self._image, self.rectangle.middle_square())
            sphere.draw_sphere()
            sphere.label_radius("r")
        elif self.shape_type == Shape.HEMISPHERE:
            hemisphere = ShapeSphere(self._image, self.rectangle.middle_square())
            hemisphere.draw_hemisphere()
            hemisphere.label_radius("r")
        elif self.shape_type == Shape.CONE:
            cone = ShapeCone(self._image, self.rectangle)
            cone.draw_cone()
            cone.label_height("h")
            cone.label_radius("r")
        elif self.shape_type == Shape.CYLINDER:
            cylinder = ShapeCylinder(self._image, self.rectangle)
            cylinder.draw_cylinder()
            cylinder.label_height("h")
            cylinder.label_radius("r")
        else:
            raise ValueError(f"Unrecognised shape type: {self.shape_type}")

    def _question_text(self, mathjax: bool) -> str:
        text = f"Consider the {self.shape_type.value} below, where:"
        if self.unknown != Unknown.RADIUS:
            text += self.radius_text(mathjax)
        if self.unknown != Unknown.HEIGHT and self.shape_type.has_height:
            text += self.height_text(mathjax)
        if self.unknown != Unknown.TOTAL:
            text += self.total_value_text(mathjax)

        unknown_name = self.unknown.value if self.unknown != Unknown.TOTAL else self.question_type.value
        text += f"What is the {unknown_name} of the {self.shape_type.value}?"
        return text

    def surface_area_in_terms_of_pi(self) -> Fraction:
        if self.shape_type == Shape.SPHERE:
            return Fraction(4 * self.radius ** 2)
        elif self.shape_type == Shape.HEMISPHERE:
            return Fraction(3 * self.radius ** 2)
        elif self.shape_type == Shape.CONE:
            return Fraction(self.radius * self.cone_diagonal + self.radius ** 2)
        elif self.shape_type == Shape.CYLINDER:
            return Fraction(2 * self.radius ** 2 + 2 * self.radius * self.height)
        else:
            raise ValueError(f"Unrecognised shape type: {self.shape_type}")

    def volume_in_terms_of_pi(self) -> Fraction:
        if self.shape_type == Shape.SPHERE:
            return Fraction(4 * self.radius ** 3, 3)
        elif self.shape_type == Shape.HEMISPHERE:
            return Fraction(2 * self.radius ** 3, 3)
        elif self.shape_type == Shape.CONE:
            return Fraction(self.radius * self.radius * self.height, 3)
        elif self.shape_type == Shape.CYLINDER:
            return Fraction(self.radius * self.radius * self.height)
        else:
            raise ValueError(f"Unrecognised shape type: {self.shape_type}")

    def radius_text(self, mathjax: bool) -> str:
        if not self.show_radius_as_height:
            return self.add_conditional_mathjax_wrapper(f"r = {self.radius}{self.units.value}", mathjax)
        else:
            if self.radius_in_terms_of_height == 1:
                coefficient = ""
            else:
                coefficient = self.format_numeric_fraction(self.radius_in_terms_of_height)
            text = f"r = {coefficient}h"
            return self.add_conditional_mathjax_wrapper(text, mathjax)

    def height_text(self, mathjax: bool) -> str:
        return self.add_conditional_mathjax_wrapper(f"h = {self.height}{self.units.value}", mathjax)

    def total_value_text(self, mathjax: bool) -> str:
        if self.question_type == QuestionType.VOLUME:
            value_in_terms_of_pi = self.volume_in_terms_of_pi()
            units_power = 3
        else:
            value_in_terms_of_pi = self.surface_area_in_terms_of_pi()
            units_power = 2

        number = self.format_numeric_fraction(value_in_terms_of_pi)

        text = f"{self.question_type.value} = {number} \\pi {self.units.value}^{units_power}"
        return self.add_conditional_mathjax_wrapper(text, mathjax)


class CircleVolumeFactory(BaseFactory):
    NUMBER_OF_DIFFICULTY_LEVELS = 3

    @property
    def title(self) -> str:
        return "Circle Volumes"

    def description(self, _: bool) -> str:
        return "Find the volume or surface area of the following circle based volumes"

    def generate(self, difficulty_level: int = 0) -> BaseEquation:
        if difficulty_level == 0:
            return self.generate_easy()
        elif difficulty_level == 1:
            return self.generate_medium()
        elif difficulty_level == 2:
            return self.generate_hard()
        else:
            raise ValueError(f"Unsupported difficulty level: {difficulty_level}")

    def generate_easy(self) -> BaseEquation:
        # Only work forwards for easy questions.
        return self._generate_common(Unknown.TOTAL)

    def generate_medium(self) -> BaseEquation:
        # Force working back for medium questions.  This is a super-lazy way of doing it, but...
        question = self._generate_common()
        while question.unknown == Unknown.TOTAL:
            question = self._generate_common()
        return question

    def generate_hard(self) -> BaseEquation:
        # Want to show radius as height => radius can't be the unknown.
        # If total is the unknown then it falls back to the basic case quite simply => height has to be the unknown.
        unknown: Unknown = Unknown.HEIGHT
        shape_type: Shape = random.choice((Shape.CONE, Shape.CYLINDER))

        # You can't do the relative stuff and still have valid pythag triples, so force volume for cones.
        if shape_type == Shape.CYLINDER:
            question_type = random.choice(list(QuestionType))
        else:
            question_type = QuestionType.VOLUME

        # Also means that radius has to be a multiple of height or vice-versa.  Flip a coin to decide which.
        if random.randint(1, 2) == 1:
            radius = random.randint(1, 4)
            height = radius * random.randint(1, 3)
        else:
            height = random.randint(1, 4)
            radius = height * random.randint(1, 3)

        # These are both normal.
        units = random.choice(list(Units))

        return CircleVolumeQuestion(
            shape_type=shape_type,
            question_type=question_type,
            radius=radius,
            height=height,
            units=units,
            unknown=unknown,
            show_radius_as_height=True,
        )

    def _generate_common(self, unknown: Optional[Unknown] = None) -> CircleVolumeQuestion:
        shape_type: Shape = random.choice(list(Shape))
        question_type = random.choice(list(QuestionType))
        units = random.choice(list(Units))

        if question_type == QuestionType.SURFACE_AREA and shape_type == Shape.CONE:
            radius, height = self._pick_pythag_sides()
        else:
            radius = random.randint(2, 9)
            height = random.randint(2, 9) if shape_type in (Shape.CONE, Shape.CYLINDER) else 0

        if unknown is None:
            unknown = shape_type.random_unknown()

        return CircleVolumeQuestion(
            shape_type=shape_type,
            question_type=question_type,
            radius=radius,
            height=height,
            units=units,
            unknown=unknown,
            show_radius_as_height=False,
        )

    @staticmethod
    def _pick_pythag_sides() -> Tuple[int, int]:
        # We want to shuffle the order, duplicate as a hacky but simple solution.
        small_triples = ((3, 4), (4, 3), (5, 12), (12, 5), (8, 15), (15, 8))
        return random.choice(small_triples)
