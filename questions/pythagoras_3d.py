"""Questions on using Pythagoras in 3D shapes."""
import enum
import math
import random

from images.cuboid_drawing import ShapeCuboid
from images.pyramid_drawing import ShapePyramid
from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.decorators import mathjax
from shared.pillow_image_equation import PillowImageEquation
from shared.units import Units


class CuboidUnknown(enum.Enum):
    FULL_DIAGONAL = "full diagonal"
    HALF_DIAGONAL = "half diagonal"


class PyramidUnknown(enum.Enum):
    HEIGHT = "height"
    SQUARE_BASE = "square_base"


class Pythagoras3dCuboid(PillowImageEquation):
    """Handles a Pythagoras 3D question."""

    def __init__(
        self,
        width: int,
        depth: int,
        height: int,
        units: Units = Units.CENTIMETERS,
        unknown: CuboidUnknown = CuboidUnknown.FULL_DIAGONAL,
    ) -> None:
        super().__init__()
        self.width = width
        self.depth = depth
        self.height = height
        self.units = units
        self.unknown = unknown

        required_depth = depth if self.unknown == CuboidUnknown.FULL_DIAGONAL else depth / 2
        bottom_diagonal_squared = width * width + required_depth * required_depth
        self.squared_answer = bottom_diagonal_squared + height * height
        self.answer = math.sqrt(self.squared_answer)

    def readable_format(self, _: bool = False) -> str:
        return f"What is the length of {self.unknown.value} PQ?"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        return f"\\sqrt{{{self.squared_answer}}} = {self.answer: .3f}{self.units.value}"

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        drawing = ShapeCuboid(self._image, self.image_width // 2, self.bottom_left)
        drawing.draw_cube()
        drawing.label_bottom_side(self.width, self.units)
        drawing.label_depth(self.depth, self.units)
        drawing.label_height(self.height, self.units)
        drawing.label_top_left_vertex("P")

        if self.unknown == CuboidUnknown.FULL_DIAGONAL:
            drawing.label_bottom_right_vertex("Q")
            drawing.draw_top_left_bottom_right_diagonal()
        else:
            drawing.label_bottom_middle_point("Q")
            drawing.draw_top_left_middle_right_diagonal()


class Pythagoras3dPyramidHeight(PillowImageEquation):
    def __init__(self, width: int, depth: int, slope: int, units: Units = Units.CENTIMETERS,) -> None:
        super().__init__()
        self.width = width
        self.depth = depth
        self.slope = slope
        self.units = units

        bottom_diagonal_squared = width * width + depth * depth
        half_bottom_diagonal_squared = bottom_diagonal_squared / 4
        self.squared_answer = slope * slope - half_bottom_diagonal_squared
        self.answer = math.sqrt(self.squared_answer)

    def readable_format(self, _: bool = False) -> str:
        return "What is the height of the apex of pyramid from the base?"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        return f"\\sqrt{{{self.squared_answer}}} = {self.answer: .3f}{self.units.value}"

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        drawing = ShapePyramid(self._image, int(self.image_width * 2 // 3), self.image_height, self.bottom_left)
        drawing.draw_pyramid()
        drawing.label_base_width(self.width, self.units)
        drawing.label_depth(self.depth, self.units)
        drawing.label_slope(self.slope, self.units)


class Pythagoras3dPyramidBase(PillowImageEquation):
    def __init__(self, height: int, slope: int, units: Units = Units.CENTIMETERS,) -> None:
        super().__init__()
        self.height = height
        self.slope = slope
        self.units = units

        half_bottom_diagonal_squared = slope * slope - height * height
        bottom_diagonal_squared = half_bottom_diagonal_squared * 4
        square_base_squared = bottom_diagonal_squared / 2
        self.squared_answer = square_base_squared
        self.answer = math.sqrt(square_base_squared)

    def readable_format(self, _: bool = False) -> str:
        return (
            "If the following is a square based pyramid with the given height and slope, "
            "how long is the side of each square?"
        )

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        return f"\\sqrt{{{self.squared_answer}}} = {self.answer: .3f}{self.units.value}"

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        drawing = ShapePyramid(self._image, int(self.image_height * 2 // 3), self.image_height, self.bottom_left)
        drawing.draw_pyramid()
        drawing.label_height(self.height, self.units)
        drawing.label_slope(self.slope, self.units)
        drawing.draw_height()


class Pythagoras3dFactory(BaseFactory):
    @property
    def title(self) -> str:
        return "Pythagoras 3D"

    def description(self, mathjax: bool) -> str:
        return "Find missing lengths in shapes by using Pythagoras"

    def generate(self, _: int = 1) -> BaseEquation:
        generators = [self.generate_random_pyramid, self.generate_random_cuboid]
        generator = random.choice(generators)
        return generator()

    @staticmethod
    def generate_random_pyramid() -> BaseEquation:
        unknown = random.choice(list(PyramidUnknown))
        units = random.choice(list(Units))

        if unknown == PyramidUnknown.HEIGHT:
            width = random.randint(2, 5)
            depth = random.randint(2, 5)
            # The maximum of the above to gives a half-base-diagonal of 12.5, so 4*4 is strictly bigger.
            slope = random.randint(4, 7)
            return Pythagoras3dPyramidHeight(width, depth, slope, units)
        else:
            slope = random.randint(4, 7)
            height = random.randint(2, slope - 1)
            return Pythagoras3dPyramidBase(height, slope, units)

    @staticmethod
    def generate_random_cuboid() -> Pythagoras3dCuboid:
        width = random.randint(2, 5)
        depth = random.randint(2, 5)
        slope = random.randint(2, 5)
        units = random.choice(list(Units))
        unknown = random.choice(list(CuboidUnknown))
        return Pythagoras3dCuboid(width, depth, slope, units, unknown)
