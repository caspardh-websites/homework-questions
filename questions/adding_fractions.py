"""Questions about adding fractions."""
import random
from typing import Optional, Tuple

from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.decorators import mathjax
from shared.polynomial_equation import PolynomialEquation


class AddingFractions(BaseEquation):
    """Questions of the form a/b + c/d where any of a, b, c, d can be a linear equation."""

    def __init__(
        self,
        first_top: PolynomialEquation,
        first_bottom: PolynomialEquation,
        second_top: PolynomialEquation,
        second_bottom: Optional[PolynomialEquation],
    ) -> None:
        self.first_top = first_top
        self.first_bottom = first_bottom
        self.second_top = second_top
        self.second_bottom = second_bottom

    def readable_format(self, _: bool = False) -> str:
        return f"Add the following fractions and simplify as much as possible: {self._question_equation()}"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        second_bottom = PolynomialEquation(1) if self.second_bottom is None else self.second_bottom
        top = self.first_top * second_bottom + self.first_bottom * self.second_top
        bottom = self.first_bottom * second_bottom
        answer = f"\\frac{{{top.format_readable()}}}{{{bottom.format_readable()}}}"
        return answer

    @mathjax(inline=False)
    def _question_equation(self) -> str:
        lhs = f"\\frac{{{self.first_top.format_readable()}}}{{{self.first_bottom.format_readable()}}}"
        if self.second_bottom is not None:
            rhs = f"\\frac{{{self.second_top.format_readable()}}}{{{self.second_bottom.format_readable()}}}"
        else:
            rhs = self.second_top.format_readable()
        return f"{lhs} + {rhs}"


class AddingFractionsFactory(BaseFactory):
    NUMBER_OF_DIFFICULTY_LEVELS = 3

    @property
    def title(self) -> str:
        return "Adding fractions"

    def description(self, _: bool) -> str:
        return "Add and simplify the following fractions."

    def generate(self, difficulty_level: int = 0) -> BaseEquation:
        if difficulty_level == 0:
            return self.generate_easy()
        elif difficulty_level == 1:
            return self.generate_medium()
        elif difficulty_level == 2:
            return self.generate_hard()
        else:
            raise ValueError(f"Unsupported difficulty level: {difficulty_level}")

    def generate_easy(self) -> BaseEquation:
        dice = random.randint(1, 2)
        if dice == 1:
            return self._generate_equation(0, 0, 0, None)
        else:
            return self._generate_equation(0, 0, 0, 0)

    def generate_medium(self) -> BaseEquation:
        dice = random.randint(1, 2)
        if dice == 1:
            return self._generate_equation(1, 0, 1, 0)
        else:
            return self._generate_equation(0, 1, 0, 1)

    def generate_hard(self) -> BaseEquation:
        return self._generate_equation(1, 1, 1, 1)

    @staticmethod
    def _generate_non_dividing_pair(
        top_max_power: int, bottom_max_power: int
    ) -> Tuple[PolynomialEquation, PolynomialEquation]:
        top = PolynomialEquation.random(top_max_power)
        bottom = PolynomialEquation.random_non_factor(bottom_max_power, top)
        return top, bottom

    @staticmethod
    def _generate_equation(
        first_top_power: int, first_bottom_power: int, second_top_power: int, second_bottom_power: Optional[int]
    ) -> AddingFractions:
        first_top, first_bottom = AddingFractionsFactory._generate_non_dividing_pair(
            first_top_power, first_bottom_power
        )

        second_bottom: Optional[PolynomialEquation] = None
        if second_bottom_power is not None:
            second_top, second_bottom = AddingFractionsFactory._generate_non_dividing_pair(
                second_top_power, second_bottom_power
            )
        else:
            second_top = PolynomialEquation.random(second_top_power)

        return AddingFractions(first_top, first_bottom, second_top, second_bottom)
