"""Questions on circle theorems."""

import enum
import random

from shared.ct_base import CircleTheoremBase
from images.circle_drawing import CircleDrawing
from shared.circle_maths import Circle
from shared.base_factory import BaseFactory
from shared.decorators import mathjax


class CircleTheoremDiameterTriangle(CircleTheoremBase):
    class Unknown(enum.Enum):
        LEFT = enum.auto()
        RIGHT = enum.auto()

    def __init__(self, point_degrees: int, left_degrees: int, unknown: Unknown) -> None:
        super().__init__()
        self.point_degrees = point_degrees
        self.left_degrees = left_degrees
        self.right_degrees = left_degrees + 180
        self.unknown = unknown

    def readable_format(self, _: bool = False) -> str:
        wording = (
            f"Angle {self._known_angle_name()} = {self._known_angle()}, "
            f"what is the value of angle {self._unknown_angle_name()}?"
        )
        return wording

    def readable_answer(self, _: bool = False) -> str:
        return f"{self._unknown_angle_name()} = {self._unknown_angle()}"

    @property
    def left_angle(self) -> int:
        return Circle.angle(self.point_degrees, self.left_degrees, self.right_degrees)

    @property
    def right_angle(self) -> int:
        return Circle.angle(self.point_degrees, self.right_degrees, self.left_degrees)

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        radius = min(self.rectangle.width, self.rectangle.height) // 2
        circle = CircleDrawing(self._image, self.rectangle.centre, radius)
        circle.draw_origin_line(self.left_degrees)
        circle.draw_origin_line(self.right_degrees)
        circle.draw_chord(self.point_degrees, self.left_degrees)
        circle.draw_chord(self.point_degrees, self.right_degrees)
        circle.draw_letter_circumference(self.left_degrees, "A")
        circle.draw_letter_circumference(self.point_degrees, "B")
        circle.draw_letter_circumference(self.right_degrees, "C")
        circle.label_origin()

    @mathjax()
    def _known_angle_name(self) -> str:
        if self.unknown == self.Unknown.LEFT:
            return "BCA"
        return "BAC"

    @mathjax()
    def _unknown_angle_name(self) -> str:
        if self.unknown == self.Unknown.LEFT:
            return "BAC"
        return "BCA"

    @mathjax(degrees=True)
    def _known_angle(self) -> str:
        if self.unknown == self.Unknown.LEFT:
            return str(self.right_angle)
        return str(self.left_angle)

    @mathjax(degrees=True)
    def _unknown_angle(self) -> str:
        if self.unknown == self.Unknown.LEFT:
            return str(self.left_angle)
        return str(self.right_angle)


class CircleTheoremDiameterTriangleFactory(BaseFactory):
    @property
    def title(self) -> str:
        return "Circle Theorems: Diameter Triangle"

    def description(self, mathjax: bool) -> str:
        return "Find the angles in triangles on the equator"

    def generate(self, _: int = 1) -> CircleTheoremDiameterTriangle:
        point = random.randint(0, 359)
        left_degrees = point + random.randint(45, 135)
        unknown = random.choice(list(CircleTheoremDiameterTriangle.Unknown))
        return CircleTheoremDiameterTriangle(point, left_degrees, unknown)
