"""Questions on circle theorems."""

import enum
import random

from images.circle_drawing import CircleDrawing
from shared.base_factory import BaseFactory
from shared.circle_maths import Circle
from shared.ct_base import CircleTheoremBase
from shared.decorators import mathjax


class CircleTheoremChordTriangles(CircleTheoremBase):
    class Unknown(enum.Enum):
        POINT_B = enum.auto()
        POINT_C = enum.auto()

    def __init__(self, a_degrees: int, b_degrees: int, c_degrees: int, d_degrees: int, unknown: Unknown) -> None:
        super().__init__()
        self.a_degrees = a_degrees
        self.b_degrees = b_degrees
        self.c_degrees = c_degrees
        self.d_degrees = d_degrees
        self.unknown = unknown

    def readable_format(self, _: bool = False) -> str:
        wording = (
            f"Angle {self._known_angle_name()} = {self._known_angle()}, "
            f"what is the value of angle {self._unknown_angle_name()}?"
        )
        return wording

    def readable_answer(self, _: bool = False) -> str:
        wording = f"{self._unknown_angle_name()} = {self._unknown_angle()}"
        return wording

    @property
    def b_angle(self) -> int:
        """The angle between the lines at the circumference."""
        return Circle.angle(self.a_degrees, self.b_degrees, self.d_degrees)

    @property
    def c_angle(self) -> int:
        """The angle around the origin."""
        return Circle.angle(self.a_degrees, self.c_degrees, self.d_degrees)

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        radius = min(self.rectangle.width, self.rectangle.height) // 2
        circle = CircleDrawing(self._image, self.rectangle.centre, radius)
        circle.draw_chord(self.a_degrees, self.b_degrees)
        circle.draw_chord(self.b_degrees, self.d_degrees)
        circle.draw_chord(self.d_degrees, self.c_degrees)
        circle.draw_chord(self.c_degrees, self.a_degrees)
        circle.draw_letter_circumference(self.a_degrees, "A")
        circle.draw_letter_circumference(self.b_degrees, "B")
        circle.draw_letter_circumference(self.c_degrees, "C")
        circle.draw_letter_circumference(self.d_degrees, "D")

    @mathjax()
    def _known_angle_name(self) -> str:
        if self.unknown == self.Unknown.POINT_B:
            return "ACD"
        return "ABD"

    @mathjax()
    def _unknown_angle_name(self) -> str:
        if self.unknown == self.Unknown.POINT_B:
            return "ABD"
        return "ACD"

    @mathjax(degrees=True)
    def _known_angle(self) -> str:
        if self.unknown == self.Unknown.POINT_B:
            return str(self.c_angle)
        return str(self.b_angle)

    def _unknown_angle(self) -> str:
        # For this theorem, the unknown angle is always equal to the known angle.
        return self._known_angle()


class CircleTheoremChordTrianglesFactory(BaseFactory):
    @property
    def title(self) -> str:
        return "Circle Theorems: Chord Triangles"

    def description(self, mathjax: bool) -> str:
        return "Find the other angle in these circles"

    def generate(self, _: int = 1) -> CircleTheoremChordTriangles:
        point_a = random.randint(0, 359)
        point_b = point_a + random.randint(45, 135)
        point_c = point_b + random.randint(45, 135)
        point_d = point_c + random.randint(45, min(135, (point_a + 360 - point_c - 45)))
        unknown = random.choice(list(CircleTheoremChordTriangles.Unknown))
        question = CircleTheoremChordTriangles(point_a, point_b, point_c, point_d, unknown)
        return question
