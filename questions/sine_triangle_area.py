"""Questions on finding the area of a triangle using sine."""
import math
import random
from typing import Dict

from shared.base_factory import BaseFactory
from shared.triangle_equation import TriangleEquation
from shared.triangle_quantities import Angle, TriangleQuantity, Side
from shared.units import Units


class SineAreaEquation(TriangleEquation):
    """Handles a sine rule question."""

    def __init__(self, given_values: Dict[TriangleQuantity, float], units: Units = Units.CENTIMETERS,) -> None:
        super().__init__(given_values, [], units)

    def readable_format(self, mathjax: bool = False) -> str:
        return "Given the following triangle, what is the area?"

    def readable_answer(self, mathjax: bool = False) -> str:
        return self.add_conditional_mathjax_inline(f"Area = {self.area(): .3f}", mathjax)

    def area(self) -> float:
        # Area = 1/2 * a * b * sin(C)
        area = 0.5
        for value_type, value in self.known_values.items():
            if isinstance(value_type, Side):
                area *= value
            elif isinstance(value_type, Angle):
                area *= math.sin(math.radians(value))
            else:
                raise RuntimeError(f"Unexpected value type: {value_type}")
        return area


class SineAreaFactory(BaseFactory):
    @property
    def title(self) -> str:
        return "Area of a triangle using Sine"

    def description(self, mathjax: bool) -> str:
        return "Find the area of a triangle from an angle and the adjacent sides"

    def generate(self, _: int = 1) -> SineAreaEquation:
        return self._generate_single()

    @staticmethod
    def _generate_single() -> SineAreaEquation:
        """Attempt to generate a single equation.  This will throw a ValueError if it isn't a valid question."""
        units = random.choice(list(Units))  # type: Units
        starting_angle = random.choice(list(Angle))  # type: Angle

        known_values: Dict[TriangleQuantity, float] = {
            starting_angle: SineAreaFactory._random_angle(),
            starting_angle.clockwise_side: SineAreaFactory._random_side(),
            starting_angle.anti_clockwise_side: SineAreaFactory._random_side(),
        }
        return SineAreaEquation(known_values, units)

    @staticmethod
    def _random_angle() -> int:
        # Keep it roughly sensible.
        return random.randint(40, 120)

    @staticmethod
    def _random_side() -> float:
        # Allow 1dp floats, but don't go more than that.
        return random.randint(10, 50) / 10
