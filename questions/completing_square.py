"""Generator for completing the square questions."""
import fractions
import math

from shared.base_factory import BaseFactory
from shared.base_equation import BaseEquation
from shared.decorators import mathjax


class CompleteSquareEquation(BaseEquation):
    """Questions of the form ax^2 + bx + c = 0 where you can complete the square."""

    def __init__(self, quadratic_coefficient: int, linear_coefficient: int, constant_term: int) -> None:
        self.quadratic_coefficient = quadratic_coefficient
        self.linear_coefficient = linear_coefficient
        self.constant_term = constant_term

    def readable_format(self, mathjax: bool = False) -> str:
        values = [(self.quadratic_coefficient, 2), (self.linear_coefficient, 1), (self.constant_term, 0)]
        return "Complete the square: " + self.format_single_variable_equation(values, 0, mathjax)

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        denominator = 2 * self.quadratic_coefficient
        assert self.linear_coefficient != 0, "Can't cope with zero linear coefficients, need to handle no first term"
        first_term_fraction = fractions.Fraction(-self.linear_coefficient, denominator)
        first_term = self.render_fraction(first_term_fraction)

        squared_term_fraction = fractions.Fraction(self.discriminant(), denominator * denominator)
        if self.perfect_square(squared_term_fraction.numerator) and self.perfect_square(
            squared_term_fraction.denominator
        ):
            second_term_fraction = fractions.Fraction(
                math.isqrt(squared_term_fraction.numerator), math.isqrt(squared_term_fraction.denominator)
            )
            second_term = self.render_fraction(second_term_fraction)
        else:
            second_term_fraction = squared_term_fraction
            second_term = f"\\sqrt{{{self.render_fraction(second_term_fraction)}}}"

        answer = f"{first_term} \\pm {second_term}"

        return answer

    def valid(self) -> bool:
        # We don't want the maths to be too hard, so ensure the quadratic divides the linear.
        if self.linear_coefficient % self.quadratic_coefficient != 0:
            return False
        # We can be more lenient on the constant term.  Don't go further than 0.25s though.
        if (4 * self.constant_term) % self.quadratic_coefficient != 0:
            return False
        # Only want real solutions.
        if self.discriminant() < 0:
            return False
        return True

    def discriminant(self) -> int:
        return self.linear_coefficient ** 2 - 4 * self.quadratic_coefficient * self.constant_term

    @staticmethod
    def perfect_square(number: int) -> bool:
        root = math.isqrt(number)
        return root * root == number

    @staticmethod
    def render_fraction(fraction: fractions.Fraction) -> str:
        # The fraction always gets normalised so the denominator is positive.
        if fraction.denominator == 1:
            return str(fraction.numerator)
        return f"\\frac{{{fraction.numerator}}}{{{fraction.denominator}}}"


class CompleteSquareFactory(BaseFactory):
    NUMBER_OF_DIFFICULTY_LEVELS = 3

    @property
    def title(self) -> str:
        return "Completing the Square"

    def description(self, mathjax: bool) -> str:
        if not mathjax:
            return "Find all solutions to x in the following by completing the square"
        return r"Find all solutions to \(x\) in the following by completing the square"

    def generate(self, difficulty_level: int = 0) -> CompleteSquareEquation:
        if difficulty_level == 0:
            quadratic_coefficient = 1
        else:
            quadratic_coefficient = self.generate_non_zero_int(-5, 5, not_one=True)

        # We want the linear term to be divisible by the quadratic so you can divide through if needed.
        # Then, first term is -b/2a, so ensure not a fraction if easy, and force a fraction if hard.
        multiplier = self.generate_non_zero_int(-4, 4)
        if difficulty_level < 2:
            # Make it a little easier by ensuring the first term isn't a fraction.
            linear_coefficient = quadratic_coefficient * multiplier * 2
        else:
            linear_coefficient = quadratic_coefficient * (multiplier * 2 + 1)

        # Just brute force this, is simpler for now!
        constant_term = quadratic_coefficient * self.generate_non_zero_int(-5, 5)
        equation = CompleteSquareEquation(quadratic_coefficient, linear_coefficient, constant_term)
        while equation.discriminant() < 0:
            equation.constant_term = self.generate_non_zero_int(-20, 20)

        return equation
