"""Questions on the sine rule."""

import enum
import math
import random
from typing import Dict, Tuple, List

from shared.base_factory import BaseFactory
from shared.triangle_quantities import Angle, Side, TriangleQuantity
from shared.triangle_unknowns import TriangleUnknowns
from shared.units import Units


class QuestionType(enum.Enum):
    AAS = enum.auto()
    ASA = enum.auto()
    SSA = enum.auto()

    def get_knowns(self, starting_angle: Angle) -> Tuple[TriangleQuantity, TriangleQuantity, TriangleQuantity]:
        if self == QuestionType.AAS:
            return starting_angle, starting_angle.anti_clockwise_angle, starting_angle.corresponding_side
        elif self == QuestionType.ASA:
            return starting_angle, starting_angle.anti_clockwise_side, starting_angle.anti_clockwise_angle
        elif self == QuestionType.SSA:
            return starting_angle, starting_angle.clockwise_side, starting_angle.corresponding_side
        raise RuntimeError(f"Unrecognised QuestionType: {self}")

    def get_possible_unknowns(self, starting_angle: Angle) -> List[TriangleQuantity]:
        if self == QuestionType.AAS:
            return [starting_angle.clockwise_side, starting_angle.anti_clockwise_side]
        elif self == QuestionType.ASA:
            return [starting_angle.corresponding_side, starting_angle.clockwise_side]
        elif self == QuestionType.SSA:
            return [
                starting_angle.anti_clockwise_angle,
                starting_angle.clockwise_angle,
                starting_angle.anti_clockwise_side,
            ]
        raise RuntimeError(f"Unrecognised QuestionType: {self}")


class SineRuleEquation(TriangleUnknowns):
    """Handles a sine rule question."""

    def __init__(
        self, given_values: Dict[TriangleQuantity, float], unknown: TriangleQuantity, units: Units = Units.CENTIMETERS,
    ) -> None:
        super().__init__(given_values, unknown, units)
        self._calculated_values = self.known_values.copy()
        self._solve_triangle()

    def get_value(self, key: TriangleQuantity) -> float:
        return self._calculated_values[key]

    def _solve_triangle(self) -> None:
        # If we've got 2 angles, do some quick maths to find the last one.
        self._try_populate_third_angle()

        # We can now guarantee to have a corresponding pair somewhere, otherwise the question is impossible!
        ratio_side_over_angle = 0.0
        for angle in Angle:
            try:
                side_value = self._calculated_values[angle.corresponding_side]
                angle_value = self._calculated_values[angle]
                ratio_side_over_angle = side_value / math.sin(math.radians(angle_value))
                break
            except KeyError:
                continue
        assert ratio_side_over_angle != 0, f"Impossible question, not enough information in {self._calculated_values}"

        # Now we can repeatedly work through each pair filling in the other one until we're done.
        while len(self._calculated_values) < 6:
            for angle in Angle:
                side = angle.corresponding_side
                if angle not in self._calculated_values or side in self._calculated_values:
                    continue
                angle_value = self._calculated_values[angle]
                self._calculated_values[side] = self.solve_for_side_with_ratio(angle_value, ratio_side_over_angle)
            for side in Side:
                angle = side.corresponding_angle
                if side not in self._calculated_values or angle in self._calculated_values:
                    continue
                side_value = self._calculated_values[side]
                self._calculated_values[angle] = self.solve_for_angle_with_ratio(side_value, ratio_side_over_angle)
            self._try_populate_third_angle()

    @staticmethod
    def solve_for_side(angle_a: float, side_a: float, angle_b: float) -> float:
        return SineRuleEquation.solve_for_side_with_ratio(angle_b, side_a / math.sin(math.radians(angle_a)))

    @staticmethod
    def solve_for_angle(angle_a: float, side_a: float, side_b: float) -> float:
        return SineRuleEquation.solve_for_angle_with_ratio(side_b, side_a / math.sin(math.radians(angle_a)))

    @staticmethod
    def solve_for_side_with_ratio(angle: float, ratio_side_over_angle: float) -> float:
        return math.sin(math.radians(angle)) * ratio_side_over_angle

    @staticmethod
    def solve_for_angle_with_ratio(side: float, ratio_side_over_angle: float) -> float:
        ratio_angle_over_side = 1 / ratio_side_over_angle
        return math.degrees(math.asin(side * ratio_angle_over_side))

    def _try_populate_third_angle(self):
        """Attempts to populate the third angle if the other two are known."""
        missing_angles = [angle for angle in Angle if angle not in self._calculated_values]
        if len(missing_angles) != 1:
            return
        missing_angle = missing_angles[0]
        missing_value = 180 - sum(value for key, value in self._calculated_values.items() if isinstance(key, Angle))
        if missing_value <= 0:
            raise ValueError(f"Invalid triangle, 3rd angle isn't positive: {missing_value}")
        self._calculated_values[missing_angle] = missing_value


class SineRuleFactory(BaseFactory):
    @property
    def title(self) -> str:
        return "Sine Rule"

    def description(self, mathjax: bool) -> str:
        return "Find angles and sides in a triangle"

    def generate(self, _: int = 1) -> SineRuleEquation:
        while True:
            try:
                return self._generate_single()
            except (ValueError, ZeroDivisionError):
                # This is typically because we've generated a triangle that doesn't add up.  Just cheat and try again!
                continue

    @staticmethod
    def _generate_single() -> SineRuleEquation:
        """Attempt to generate a single equation.  This will throw a ValueError if it isn't a valid question."""
        units = random.choice(list(Units))  # type: Units
        starting_angle = random.choice(list(Angle))  # type: Angle
        question_type = random.choice(list(QuestionType))  # type: QuestionType
        knowns_keys = question_type.get_knowns(starting_angle)
        knowns = {}  # type: Dict[TriangleQuantity, float]
        for key in knowns_keys:
            if isinstance(key, Angle):
                knowns[key] = random.randint(40, 120)
            elif isinstance(key, Side):
                # Allow floats to 1dp to make things a bit more interesting.
                knowns[key] = random.randint(1, 50) / 10
            else:
                raise RuntimeError(f"Unexpected key type: {type(key)}")
        unknown = random.choice(question_type.get_possible_unknowns(starting_angle))
        return SineRuleEquation(knowns, unknown, units)
