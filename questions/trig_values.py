"""Knowing common trig fractions off by heart."""
import dataclasses
import enum
import random
from typing import Dict, Iterable

from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.decorators import mathjax


class TrigFunction(enum.Enum):
    SIN = "sin"
    COS = "cos"
    TAN = "tan"


VALUES_TABLE: Dict[TrigFunction, Dict[int, str]] = {
    TrigFunction.SIN: {0: "0", 30: r"\frac{1}{2}", 45: r"\frac{1}{\sqrt{2}}", 60: r"\frac{\sqrt{3}}{2}", 90: "1"},
    TrigFunction.COS: {0: "1", 30: r"\frac{\sqrt{3}}{2}", 45: r"\frac{1}{\sqrt{2}}", 60: r"\frac{1}{2}", 90: "0"},
    TrigFunction.TAN: {0: "0", 30: r"\frac{1}{\sqrt{3}}", 45: "1", 60: r"\sqrt{3}", 90: "undefined"},
}


@dataclasses.dataclass
class TrigValues(BaseEquation):
    """Know the trig fractions off by heart."""

    trig_function: TrigFunction
    angle: int

    def readable_format(self, mathjax: bool = False) -> str:
        equation = self.add_conditional_mathjax_wrapper(f"{self.trig_function.value}({self.angle})", mathjax)
        return f"Without looking it up, what is the value of: {equation}"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        return VALUES_TABLE[self.trig_function][self.angle]


class TrigValuesFactory(BaseFactory):
    @property
    def title(self) -> str:
        return "Known Trig Values"

    def description(self, _: bool) -> str:
        return "Learn common trig values off by heart."

    def generate(self, _: int = 0) -> BaseEquation:
        trig_function = random.choice(list(TrigFunction))
        angle = random.choice(list(VALUES_TABLE[TrigFunction.SIN].keys()))
        return TrigValues(trig_function, angle)

    def generate_set(
        self, number_questions: int = BaseFactory.DEFAULT_NUM_QUESTIONS, mixed_difficulty: bool = True
    ) -> Iterable[BaseEquation]:
        # Override generate set to prevent getting duplicates in a set.
        all_questions = list(self._all_question_combinations())
        random.shuffle(all_questions)
        for ii, question in enumerate(all_questions):
            if ii == number_questions:
                return
            yield question

    @staticmethod
    def _all_question_combinations() -> Iterable[BaseEquation]:
        for trig_function in TrigFunction:
            for angle in VALUES_TABLE[trig_function]:
                yield TrigValues(trig_function, angle)
