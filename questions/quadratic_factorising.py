"""Generator for questions that involve factorising a quadratic into brackets."""

from shared.base_equation import BaseEquation


class QuadraticFactorisingEquation(BaseEquation):
    def __init__(self, quadratic_coefficient: int, linear_coefficient: int, constant_term: int) -> None:
        self.quadratic_coefficient = quadratic_coefficient
        self.linear_coefficient = linear_coefficient
        self.constant_term = constant_term

    def readable_format(self, mathjax: bool = False) -> str:
        values = [(self.quadratic_coefficient, 2), (self.linear_coefficient, 1), (self.constant_term, 0)]
        return self.format_single_variable_equation(values, 0, mathjax)
