"""Simultaneous equations with one linear and one quadratic.  Initially no coefficients on the quadratics."""

import logging
import random
from typing import Tuple

from questions.completing_square import CompleteSquareEquation
from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory

log = logging.getLogger(__name__)


class SimultaneousQuadratic(BaseEquation):
    """Stores a basic simultaneous equation where one is quadratic.

    Specifically of the form:
      x^2 + y^2 = c
      a*x + b*y = d
    """

    def __init__(self, x: int, y: int, x_linear_coefficient: int, y_linear_coefficient: int) -> None:
        self.x = x
        self.y = y
        self.x_linear_coefficient = x_linear_coefficient
        self.y_linear_coefficient = y_linear_coefficient

    @property
    def result_linear(self) -> int:
        return self.x_linear_coefficient * self.x + self.y_linear_coefficient * self.y

    @property
    def result_quadratic(self) -> int:
        return self.x * self.x + self.y * self.y

    def readable_format(self, mathjax: bool = False) -> str:
        question = "Solve for x and y:\n"
        question += "\n".join(
            [
                self._format_quadratic(mathjax),
                self._format_linear_equation(
                    self.x_linear_coefficient, self.y_linear_coefficient, self.result_linear, mathjax
                ),
            ]
        )
        return question

    def readable_answer(self, mathjax: bool = False) -> str:
        first_answer = self.add_conditional_mathjax_inline(f"x = {self.x}; y = {self.y}", mathjax)
        try:
            other_x, other_y = self.find_other_solution()
            other_answer = self.add_conditional_mathjax_inline(f"x = {other_x}; y = {other_y}", mathjax)
            return f"{first_answer} or {other_answer}"
        except ValueError:
            return first_answer

    def valid(self) -> bool:
        if self.x == 0 and self.y == 0:
            # Not a very exciting case!
            return False
        # Check we don't end up with a horrible completing the square.
        # These are the coefficients from the resulting quadratic after substitution.
        a = self.x_linear_coefficient ** 2 + self.y_linear_coefficient ** 2
        b = -2 * self.x_linear_coefficient * self.result_linear
        c = self.result_linear ** 2 - self.result_quadratic * self.y_linear_coefficient
        return CompleteSquareEquation(a, b, c).valid()

    def _format_quadratic(self, mathjax: bool) -> str:
        output = f"x^2 + y^2 = {self.result_quadratic}"
        if mathjax:
            output = self.add_mathjax_wrapper(output)
        return output

    def find_other_solution(self) -> Tuple[int, int]:
        # Because the squared values have no coefficients, you can only have the same absolute integer values.
        # The only question then becomes, which number is x, which is y, and what the minus signs are.
        possibles = (
            (self.x, -self.y),
            (-self.x, self.y),
            (-self.x, -self.y),
            (self.y, self.x),
            (self.y, -self.x),
            (-self.y, self.x),
            (-self.y, -self.x),
        )

        for possible_x, possible_y in possibles:
            if (self.x, self.y) == (possible_x, possible_y):
                continue
            other = SimultaneousQuadratic(possible_x, possible_y, self.x_linear_coefficient, self.y_linear_coefficient)
            if other.result_linear == self.result_linear and other.result_quadratic == self.result_quadratic:
                return possible_x, possible_y
        raise ValueError("Failed to find valid other solution")


class SimultaneousQuadraticFactory(BaseFactory):
    def __init__(
        self,
        min_variable_value: int = -5,
        max_variable_value: int = 5,
        min_linear_coefficient: int = -8,
        max_linear_coefficient: int = 8,
    ) -> None:
        self.min_variable_value = min_variable_value
        self.max_variable_value = max_variable_value
        self.min_linear_coefficient = min_linear_coefficient
        self.max_linear_coefficient = max_linear_coefficient

    @property
    def title(self) -> str:
        return "Simultaneous Equations with one Quadratic"

    def description(self, mathjax: bool = False) -> str:
        if not mathjax:
            return "Find all possible values of x and y in the following pairs of equations"
        return r"Find all possible values of \(x\) and \(y\) in the following pairs of equations"

    def generate(self, _: int = 1) -> SimultaneousQuadratic:
        equation = self._generate_possible_equation()
        attempts = 1
        while not equation.valid():
            equation = self._generate_possible_equation()
            attempts += 1
        log.debug("Generated simultaneous quadratic in %d attempts", attempts)
        return equation

    def _generate_possible_equation(self) -> SimultaneousQuadratic:
        return SimultaneousQuadratic(
            random.randint(self.min_variable_value, self.max_variable_value),
            random.randint(self.min_variable_value, self.max_variable_value),
            self.generate_non_zero_int(self.min_linear_coefficient, self.max_linear_coefficient),
            self.generate_non_zero_int(self.min_linear_coefficient, self.max_linear_coefficient),
        )
