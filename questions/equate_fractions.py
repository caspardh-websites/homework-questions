"""Questions equating fractions."""
import random
from typing import Optional

from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.decorators import mathjax
from shared.polynomial_equation import PolynomialEquation


class EquatingFractions(BaseEquation):
    """Questions of the form a/b = c/d where any of a, b, c, d can be a linear equation."""

    def __init__(
        self,
        first_top: PolynomialEquation,
        first_bottom: PolynomialEquation,
        second_top: PolynomialEquation,
        second_bottom: Optional[PolynomialEquation],
    ) -> None:
        self.first_top = first_top
        self.first_bottom = first_bottom
        self.second_top = second_top
        self.second_bottom = second_bottom

    def readable_format(self, _: bool = False) -> str:
        return f"Simplify the following (write as a polynomial in x equal to zero): {self._question_equation()}"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        second_bottom = PolynomialEquation(1) if self.second_bottom is None else self.second_bottom
        lhs = self.first_top * second_bottom
        rhs = self.first_bottom * self.second_top
        answer = lhs - rhs
        return f"{answer.format_readable()} = 0"

    @mathjax(inline=False)
    def _question_equation(self) -> str:
        lhs = f"\\frac{{{self.first_top.format_readable()}}}{{{self.first_bottom.format_readable()}}}"
        if self.second_bottom is not None:
            rhs = f"\\frac{{{self.second_top.format_readable()}}}{{{self.second_bottom.format_readable()}}}"
        else:
            rhs = self.second_top.format_readable()
        return f"{lhs} = {rhs}"


class EquatingFractionsFactory(BaseFactory):
    NUMBER_OF_DIFFICULTY_LEVELS = 3

    @property
    def title(self) -> str:
        return "Equating fractions"

    def description(self, _: bool) -> str:
        return "Solve the following equations involving fractions."

    def generate(self, difficulty_level: int = 0) -> BaseEquation:
        if difficulty_level == 0:
            return self.generate_easy()
        elif difficulty_level == 1:
            return self.generate_medium()
        elif difficulty_level == 2:
            return self.generate_hard()
        else:
            raise ValueError(f"Unsupported difficulty level: {difficulty_level}")

    def generate_easy(self) -> BaseEquation:
        dice = random.randint(1, 2)
        if dice == 1:
            return EquatingFractions(
                PolynomialEquation.random(max_power=1),
                PolynomialEquation.random(max_power=0),
                PolynomialEquation.random(max_power=0),
                None,
            )
        else:
            return EquatingFractions(
                PolynomialEquation.random(max_power=1),
                PolynomialEquation.random_single_power(1),
                PolynomialEquation.random(max_power=0),
                None,
            )

    def generate_medium(self) -> BaseEquation:
        return EquatingFractions(
            PolynomialEquation.random(max_power=1),
            PolynomialEquation.random(max_power=0),
            PolynomialEquation.random(max_power=1),
            PolynomialEquation.random(max_power=0),
        )

    def generate_hard(self) -> BaseEquation:
        dice = random.randint(1, 2)
        if dice == 1:
            return EquatingFractions(
                PolynomialEquation.random(max_power=0),
                PolynomialEquation.random(max_power=1),
                PolynomialEquation.random(max_power=0),
                PolynomialEquation.random(max_power=1),
            )
        else:
            return EquatingFractions(
                PolynomialEquation.random(max_power=1),
                PolynomialEquation.random(max_power=1),
                PolynomialEquation.random(max_power=1),
                PolynomialEquation.random(max_power=1),
            )
