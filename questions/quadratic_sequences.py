"""Generate simultaneous equations."""

import random
from typing import List

from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory
from shared.decorators import mathjax


class QuadraticSequence(BaseEquation):
    """Stores a single generated quadratic sequence."""

    def __init__(self, quadratic_coefficient: int, linear_coefficient: int, constant_term: int) -> None:
        self.quadratic_coefficient = quadratic_coefficient
        self.linear_coefficient = linear_coefficient
        self.constant_term = constant_term

    def readable_format(self, _: bool = False) -> str:
        sequence = ", ".join(str(number) for number in self._generate_sequence())
        sequence = self.add_conditional_mathjax_inline(sequence, True)
        return f"What is this sequence: {sequence}"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        return self.format_single_variable_equation(
            ((self.quadratic_coefficient, 2), (self.linear_coefficient, 1), (self.constant_term, 0)),
            mathjax=False,
            variable="n",
        )

    def _generate_sequence(self, num_terms: int = 5) -> List[int]:
        return [self._sequence_term(ii) for ii in range(1, num_terms + 1)]

    def _sequence_term(self, term: int) -> int:
        return self.quadratic_coefficient * term ** 2 + self.linear_coefficient * term + self.constant_term


class QuadraticSequenceFactory(BaseFactory):
    NUMBER_OF_DIFFICULTY_LEVELS = 3

    MAX_COEFFICIENT = 5

    @property
    def title(self) -> str:
        return "Quadratic sequences"

    def description(self, mathjax: bool = False) -> str:
        return "Work out the formula for the nth term of the sequence"

    def generate(self, difficulty_level: int = 0) -> BaseEquation:
        if difficulty_level == 0:
            return self.generate_easy()
        elif difficulty_level == 1:
            return self.generate_medium()
        elif difficulty_level == 2:
            return self.generate_hard()
        else:
            raise ValueError(f"Unsupported difficulty level: {difficulty_level}")

    def generate_easy(self) -> QuadraticSequence:
        # To make it easy use no n^2 coefficient and force one of the others to zero.
        non_quadratic_coefficients = [0, random.randint(-self.MAX_COEFFICIENT, self.MAX_COEFFICIENT)]
        random.shuffle(non_quadratic_coefficients)
        return QuadraticSequence(1, non_quadratic_coefficients[0], non_quadratic_coefficients[1])

    def generate_medium(self) -> QuadraticSequence:
        # To make it harder, allow quadratic to be 1 or 2, and allow others to be non-zero.
        return QuadraticSequence(
            random.randint(1, 3),
            random.randint(-self.MAX_COEFFICIENT, self.MAX_COEFFICIENT),
            random.randint(-self.MAX_COEFFICIENT, self.MAX_COEFFICIENT),
        )

    def generate_hard(self) -> QuadraticSequence:
        return QuadraticSequence(
            self.generate_non_zero_int(-self.MAX_COEFFICIENT, self.MAX_COEFFICIENT),
            random.randint(-self.MAX_COEFFICIENT, self.MAX_COEFFICIENT),
            random.randint(-self.MAX_COEFFICIENT, self.MAX_COEFFICIENT),
        )
