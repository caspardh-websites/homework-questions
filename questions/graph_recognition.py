"""Recognise different types of standard graph."""
import dataclasses
import enum
import functools
import math
import pathlib
from typing import List, Callable

import numpy

from images.graph import Graph, TrigConfig, PolynomialConfig, TanConfig, ReciprocalConfig, GraphConfig
from shared.base_factory import BaseFactory
from shared.decorators import mathjax
from shared.image_equation import ImageEquation


class RecogniseBasicGraphQuestion(ImageEquation):
    """Handles a basic graph recognition question, i.e. one without any variability in the equation."""

    def __init__(self, graph: Graph, answer_string: str) -> None:
        super().__init__()
        self._graph = graph
        self.answer_string = answer_string

    def readable_format(self, _: bool = False) -> str:
        return "What is this graph?"

    @mathjax()
    def readable_answer(self, _: bool = False) -> str:
        return self.answer_string

    def _create_image(self, image_path: pathlib.Path) -> None:
        self._graph.generate_image(image_path, image_size_pixels=(150, 150))


@dataclasses.dataclass
class GraphDetails(object):
    # This display string can have up to 2 format variables:
    #  - horizontal_translation
    #  - vertical_translation
    display_string: str
    # This is a function which takes a horizontal offset, vertical offset, and then the x value to produce a y value.
    graph_function: Callable[[int, int, float], float]
    graph_config: GraphConfig

    supports_horizontal_offset: bool = True
    supports_vertical_offset: bool = True

    def graph_question(self, horizontal_offset: int = 0, vertical_offset: int = 0) -> RecogniseBasicGraphQuestion:
        # Ignore mypy due to bug: https://github.com/python/mypy/issues/5485
        func_with_offsets = functools.partial(self.graph_function, horizontal_offset, vertical_offset)  # type: ignore
        numpy_function = numpy.frompyfunc(func_with_offsets, 1, 1)
        filled_in_string = self.display_string.format(
            horizontal_translation=self.translation_string(horizontal_offset),
            vertical_translation=self.translation_string(vertical_offset),
        )

        graph = Graph(numpy_function, self.graph_config)
        if horizontal_offset != 0:
            graph.mark_x_points.append(-horizontal_offset)
        if vertical_offset != 0:
            graph.mark_y_points.append(vertical_offset)

        question = RecogniseBasicGraphQuestion(graph, filled_in_string)
        return question

    @staticmethod
    def translation_string(value: int) -> str:
        if value == 0:
            return ""
        if value > 0:
            return f" + {value}"
        return f" - {-value}"


GRAPHS = (
    GraphDetails(
        "y = x{vertical_translation}",
        lambda _, vertical, x: x + vertical,
        PolynomialConfig,
        supports_horizontal_offset=False,
    ),
    GraphDetails(
        "y = -x{vertical_translation}",
        lambda _, vertical, x: -x + vertical,
        PolynomialConfig,
        supports_horizontal_offset=False,
    ),
    GraphDetails(
        "y = (x{horizontal_translation})^2{vertical_translation}",
        lambda horizontal, vertical, x: (x + horizontal) ** 2 + vertical,
        PolynomialConfig,
    ),
    GraphDetails(
        "y = -(x{horizontal_translation})^2{vertical_translation}",
        lambda horizontal, vertical, x: -((x + horizontal) ** 2) + vertical,
        PolynomialConfig,
    ),
    GraphDetails(
        "y = (x{horizontal_translation})^3{vertical_translation}",
        lambda horizontal, vertical, x: (x + horizontal) ** 3 + vertical,
        PolynomialConfig,
    ),
    GraphDetails(
        "y = -(x{horizontal_translation})^3{vertical_translation}",
        lambda horizontal, vertical, x: -((x + horizontal) ** 3) + vertical,
        PolynomialConfig,
    ),
    GraphDetails(
        "y = \\frac{{1}}{{x{horizontal_translation}}}{vertical_translation}",
        lambda horizontal, vertical, x: 1 / (x + horizontal) + vertical,
        ReciprocalConfig,
    ),
    GraphDetails(
        "y = -\\frac{{1}}{{x{horizontal_translation}}}{vertical_translation}",
        lambda horizontal, vertical, x: -1 / (x + horizontal) + vertical,
        ReciprocalConfig,
    ),
    GraphDetails(
        "y = sin(x)",
        lambda _, __, x: math.sin(x),
        TrigConfig,
        supports_horizontal_offset=False,
        supports_vertical_offset=False,
    ),
    GraphDetails(
        "y = cos(x)",
        lambda _, __, x: math.cos(x),
        TrigConfig,
        supports_horizontal_offset=False,
        supports_vertical_offset=False,
    ),
    GraphDetails(
        "y = tan(x)",
        lambda _, __, x: math.tan(x),
        TanConfig,
        supports_horizontal_offset=False,
        supports_vertical_offset=False,
    ),
)


class DifficultyLevel(enum.Enum):
    NO_TRANSLATIONS = 0
    VERTICAL_TRANSLATIONS = 1
    HORIZONTAL_TRANSLATIONS = 2
    BOTH_TRANSLATIONS = 3

    @staticmethod
    def graphs_for_level(difficulty_level: "DifficultyLevel") -> List[GraphDetails]:
        if difficulty_level == DifficultyLevel.NO_TRANSLATIONS:
            return list(GRAPHS)
        if difficulty_level == DifficultyLevel.VERTICAL_TRANSLATIONS:
            return [graph for graph in GRAPHS if graph.supports_vertical_offset]
        if difficulty_level == DifficultyLevel.HORIZONTAL_TRANSLATIONS:
            return [graph for graph in GRAPHS if graph.supports_horizontal_offset]
        if difficulty_level == DifficultyLevel.BOTH_TRANSLATIONS:
            return [graph for graph in GRAPHS if graph.supports_horizontal_offset and graph.supports_horizontal_offset]
        raise ValueError(f"Unrecognised difficulty level: {difficulty_level}")


class RecogniseGraphFactory(BaseFactory):
    NUMBER_OF_DIFFICULTY_LEVELS = 4

    def __init__(self) -> None:
        super().__init__()
        for difficulty_level in DifficultyLevel:
            self.avoid_repeating_variable(
                "graph_details", DifficultyLevel.graphs_for_level(difficulty_level), difficulty_level.value
            )

    @property
    def title(self) -> str:
        return "Recognise Graphs"

    def description(self, _: bool) -> str:
        return "Recognise common graph types"

    def generate(self, difficulty_level: int = 0) -> RecogniseBasicGraphQuestion:
        graph_details = self.get_next_variables(difficulty_level=difficulty_level)["graph_details"]
        assert isinstance(graph_details, GraphDetails)

        vertical_translation = 0
        horizontal_translation = 0
        if difficulty_level in (DifficultyLevel.VERTICAL_TRANSLATIONS.value, DifficultyLevel.BOTH_TRANSLATIONS.value):
            vertical_translation = self.generate_non_zero_int(-5, 5)
        if difficulty_level in (DifficultyLevel.HORIZONTAL_TRANSLATIONS.value, DifficultyLevel.BOTH_TRANSLATIONS.value):
            horizontal_translation = self.generate_non_zero_int(-5, 5)

        question = graph_details.graph_question(horizontal_translation, vertical_translation)
        return question
