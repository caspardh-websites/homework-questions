"""Base class for all circle theorem equations."""

from shared.base_equation import BaseEquation
from shared.pillow_image_equation import PillowImageEquation


class CircleTheoremBase(PillowImageEquation):
    def readable_format(self, mathjax: bool = False) -> str:
        raise NotImplementedError

    def readable_answer(self, mathjax: bool = False) -> str:
        raise NotImplementedError

    def _create_pillow_image(self) -> None:
        raise NotImplementedError

    def _radius(self) -> int:
        return self.image_height // 2

    @staticmethod
    def _format_question_known_unknown(known_name: str, known_angle: int, unknown_name: str, mathjax: bool) -> str:
        unknown_name = BaseEquation.add_conditional_mathjax_inline(unknown_name, mathjax)
        degree_symbol = CircleTheoremBase._degrees_symbol(mathjax)

        equation = f"{known_name} = {known_angle}{degree_symbol}"
        equation = BaseEquation.add_conditional_mathjax_inline(equation, mathjax)

        wording = f"Angle {equation}, what is the value of angle {unknown_name}?"
        return wording

    @staticmethod
    def _format_answer(unknown_name: str, answer_angle: int, mathjax: bool) -> str:
        degree_symbol = CircleTheoremBase._degrees_symbol(mathjax)
        equation = f"{unknown_name} = {answer_angle}{degree_symbol}"
        equation = BaseEquation.add_conditional_mathjax_inline(equation, mathjax)
        return equation

    @staticmethod
    def _degrees_symbol(mathjax: bool = True) -> str:
        if mathjax:
            return r"^{\circ}"
        else:
            return " degrees"
