"""Stores a polynomial equation and provides convenient wrapper methods."""
import random

from numpy.polynomial import Polynomial
from numpy.polynomial.polynomial import polydiv


class PolynomialEquation(Polynomial):
    @staticmethod
    def random(max_power: int) -> "PolynomialEquation":
        powers = [PolynomialEquation.random_coefficient() for _ in range(max_power + 1)]
        return PolynomialEquation(powers)

    @staticmethod
    def random_single_power(power: int) -> "PolynomialEquation":
        powers = [0 for _ in range(0, power)]
        powers.append(PolynomialEquation.random_coefficient())
        return PolynomialEquation(powers)

    @staticmethod
    def random_non_factor(max_power: int, other: "PolynomialEquation") -> "PolynomialEquation":
        """Returns a polynomial which doesn't exactly divide other."""
        poly = PolynomialEquation.random(max_power)
        safety_check = 0
        while PolynomialEquation.exactly_divide(other, poly):
            if safety_check > 100:
                raise ValueError(f"Failed to find non-factor of power {max_power} under {other.format_readable()}")
            safety_check += 1

            poly = PolynomialEquation.random(max_power)
        return poly

    @staticmethod
    def random_coefficient(non_zero: bool = True, not_one: bool = False) -> int:
        # Cheat!  Can probably do it nicer, but this works for now.
        coefficient = random.randint(-5, 5)
        while (coefficient == 0 and non_zero) or (coefficient == 1 and not_one):
            coefficient = random.randint(-5, 5)
        return coefficient

    @staticmethod
    def exactly_divide(top: "PolynomialEquation", bottom: "PolynomialEquation") -> bool:
        # numpy only cares about whether the polynomials divide.  We also care about whether if leaves integer
        # coefficients.
        coefficients, remainder = polydiv(top.coef, bottom.coef)
        if any(remainder):
            return False
        return all(int(coefficient) == coefficient for coefficient in coefficients)

    def __repr__(self) -> str:
        return f"PolynomialEquation({self.coef})"

    def __str__(self) -> str:
        return self.format_readable()

    def format_readable(self) -> str:
        equation = ""
        # Coefficients are given in increasing power, so constants first.
        previous_coefficient_negative = False
        for power, coefficient in enumerate(self.coef):
            if coefficient == 0:
                continue

            # numpy naturally uses floats, we often use ints so switch if we can.
            if int(coefficient) == coefficient:
                coefficient = int(coefficient)

            if power == 0:
                format_power = ""
            elif power == 1:
                format_power = "x"
            else:
                format_power = f"x^{str(power)}"

            if power == 0:
                format_coefficient = str(abs(coefficient))
            elif abs(coefficient) == 1:
                format_coefficient = ""
            else:
                format_coefficient = str(abs(coefficient))

            if not equation:
                linking_sign = ""
            elif previous_coefficient_negative:
                linking_sign = " - "
            else:
                linking_sign = " + "

            equation = f"{format_coefficient}{format_power}{linking_sign}" + equation

            previous_coefficient_negative = coefficient < 0

        if previous_coefficient_negative:
            equation = "-" + equation

        return equation
