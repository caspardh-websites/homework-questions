"""This provides a base interface for an equation to be held / displayed, as well as some helper methods."""

import pathlib
from fractions import Fraction
from typing import Tuple, Sequence, Optional


class BaseEquation(object):
    def readable_format(self, mathjax: bool = False) -> str:
        raise NotImplementedError

    def readable_answer(self, mathjax: bool = False) -> str:
        raise NotImplementedError

    @property
    def has_image(self) -> bool:
        return False

    def generate_image(self, image_path: pathlib.Path) -> None:
        raise RuntimeError("Attempting to retrieve the image name for an equation with an image")

    @staticmethod
    def _format_linear_equation(x_coefficient: int, y_coefficient: int, result: int, mathjax: bool) -> str:
        """Format an equation so it is readable."""
        x_value = BaseEquation.format_first_coefficient(x_coefficient)
        join = BaseEquation.format_non_first_coefficient(y_coefficient)
        output = f"{x_value}x{join}y = {result}"
        if mathjax:
            output = BaseEquation.add_mathjax_wrapper(output)
        return output

    @staticmethod
    def format_single_variable_equation(
        values: Sequence[Tuple[int, int]],
        equals: Optional[int] = None,
        mathjax: Optional[bool] = True,
        variable: str = "x",
    ) -> str:
        """Format a single variable equation, the values tuple is (coefficient, power)."""
        equation = ""
        for coefficient, power in values:
            if coefficient == 0:
                continue
            constant_term = power == 0
            if not equation:
                human_coefficint = BaseEquation.format_first_coefficient(coefficient, constant_term)
            else:
                human_coefficint = BaseEquation.format_non_first_coefficient(coefficient, constant_term)
            if power == 0:
                human_variable = ""
            elif power == 1:
                human_variable = variable
            else:
                human_variable = f"{variable}^{power}"
            equation += f"{human_coefficint}{human_variable}"
        assert equation, "No coefficients given?!"
        if equals is not None:
            equation += f" = {equals}"
        if mathjax:
            equation = BaseEquation.add_mathjax_wrapper(equation)
        return equation

    @staticmethod
    def add_mathjax_wrapper(to_wrap: str) -> str:
        return BaseEquation.add_conditional_mathjax_wrapper(to_wrap, True)

    @staticmethod
    def add_conditional_mathjax_wrapper(to_wrap: str, mathjax: bool) -> str:
        if not mathjax:
            return to_wrap
        return r"\[" + to_wrap + r"\]"

    @staticmethod
    def add_conditional_mathjax_inline(to_wrap: str, mathjax: bool) -> str:
        if not mathjax:
            return to_wrap
        return r"\(" + to_wrap + r"\)"

    @staticmethod
    def format_first_coefficient(coefficient: int, constant_term: bool = False) -> str:
        """Formats the coefficient in a sequence so it is more human readable."""
        if constant_term:
            return str(coefficient)
        if coefficient == 1:
            return ""
        if coefficient == -1:
            return "-"
        return str(coefficient)

    @staticmethod
    def format_non_first_coefficient(coefficient: int, constant_term: bool = False) -> str:
        """Format a later coefficient to be more naturally readable."""
        assert coefficient != 0, "Code isn't written to cope with this yet"
        sign = "+" if coefficient > 0 else "-"
        readable = "" if not constant_term and abs(coefficient) == 1 else str(abs(coefficient))
        return f" {sign} {readable}"

    @staticmethod
    def format_numeric_fraction(fraction: Fraction) -> str:
        if fraction.denominator == 1:
            number = str(fraction.numerator)
        else:
            number = f"\\frac{{{fraction.numerator}}}{{{fraction.denominator}}}"
        return number
