"""Handles drawing of a circle with a given set of knowns and unknowns."""

from typing import Dict, List

from images.circle_drawing import CircleDrawing
from shared.pillow_image_equation import PillowImageEquation
from shared.triangle_quantities import Angle, Side, TriangleQuantity
from shared.units import Units


class TriangleEquation(PillowImageEquation):
    """Common logic for dealing with a generic triangle based question.

    It assumes a triangle, always drawn as an equilateral with a horizontal bottom edge.  Angles are as follows:
     - bottom left = A
     - top middle = B
     - bottom right = C
    Corresponding sides are opposite.
    """

    # Triangle is counter-clockwise from cartesian x-axis, but drawing origin is top-level, so becomes clockwise.
    _VERTEX_LOCATIONS = {
        Angle.A: 150,
        Angle.B: 270,
        Angle.C: 30,
    }

    def __init__(
        self,
        known_values: Dict[TriangleQuantity, float],
        unknowns: List[TriangleQuantity],
        units: Units = Units.CENTIMETERS,
    ) -> None:
        super().__init__()
        self.known_values = known_values
        self.unknowns = unknowns
        self.units = units

    @property
    def _radius(self) -> int:
        return self.image_width // 2

    def readable_format(self, mathjax: bool = False) -> str:
        raise NotImplementedError

    def readable_answer(self, mathjax: bool = False) -> str:
        raise NotImplementedError

    def _side_str(self, side: Side) -> str:
        return f"{self.known_values[side]}{self.units.value}"

    def _create_pillow_image(self) -> None:
        self._image = self._new_image()
        drawing = CircleDrawing(self._image, self.centre, self._radius, draw_circle=False)
        drawing.draw_chords(*list(self._VERTEX_LOCATIONS.values()), back_to_start=True)
        self._label_knowns(drawing)
        self._label_unknowns(drawing)

    def _label_knowns(self, drawing: CircleDrawing) -> None:
        for known, value in self.known_values.items():
            if isinstance(known, Angle):
                drawing.draw_letter_circumference(self._VERTEX_LOCATIONS[known], str(value))
            elif isinstance(known, Side):
                self._label_side(drawing, known, self._side_str(known))
            else:
                raise RuntimeError(f"Unexpected type for known: {type(known)}")

    def _label_unknowns(self, drawing: CircleDrawing) -> None:
        for unknown in self.unknowns:
            if isinstance(unknown, Angle):
                drawing.draw_letter_circumference(self._VERTEX_LOCATIONS[unknown], unknown.name)
            elif isinstance(unknown, Side):
                self._label_side(drawing, unknown, unknown.name)
            else:
                raise RuntimeError(f"Unexpected type for self.unknown: {type(unknown)}")

    def _label_side(self, drawing: CircleDrawing, side: Side, label: str) -> None:
        chord_angles = side.bounding_angles
        chord_start = self._VERTEX_LOCATIONS[chord_angles[0]]
        chord_end = self._VERTEX_LOCATIONS[chord_angles[1]]
        # The position is the left hand side of the string, so if we're labelling side C (on the left) we need some
        # extra padding to compensate.
        margin = CircleDrawing.LABEL_OFFSET
        if side == Side.c and len(label) > 2:
            margin *= 2
        drawing.label_chord(chord_start, chord_end, label, margin)
