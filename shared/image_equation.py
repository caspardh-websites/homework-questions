"""A common base for all equations involving creating images."""
import pathlib

from shared.base_equation import BaseEquation


class ImageEquation(BaseEquation):
    def __init__(self):
        self._last_saved_image = None

    def readable_format(self, mathjax: bool = False) -> str:
        raise NotImplementedError

    def readable_answer(self, mathjax: bool = False) -> str:
        raise NotImplementedError

    @property
    def has_image(self) -> bool:
        return True

    def generate_image(self, image_path: pathlib.Path) -> None:
        """Generate the image to display this question."""
        if self._last_saved_image is None:
            self._create_image(image_path)
        assert image_path.exists(), "The _create_image override should have created the file"
        self._last_saved_image = image_path

    def _create_image(self, image_path: pathlib.Path) -> None:
        raise NotImplementedError
