"""Base for questions for which some sides / angles are known and the question is find another side / angle."""

from typing import Dict

from shared.triangle_equation import TriangleEquation
from shared.triangle_quantities import Side, TriangleQuantity, Angle
from shared.units import Units


class TriangleUnknowns(TriangleEquation):
    def __init__(
        self, given_values: Dict[TriangleQuantity, float], unknown: TriangleQuantity, units: Units = Units.CENTIMETERS,
    ) -> None:
        super().__init__(given_values, [unknown], units)
        self.unknown = unknown

    def readable_format(self, mathjax: bool = False) -> str:
        name = self.add_conditional_mathjax_inline(self.unknown.name, mathjax)
        return f"Given the triangle below, what is the value of {name}?"

    def readable_answer(self, mathjax: bool = False) -> str:
        answer = f"{self.unknown.name} = {self.get_value(self.unknown):.3f}"
        if isinstance(self.unknown, Side):
            answer += self.units.value
        else:
            assert isinstance(self.unknown, Angle)
            answer += " degrees" if not mathjax else r"^\circ"
        return self.add_conditional_mathjax_inline(answer, mathjax)

    def get_value(self, key: TriangleQuantity) -> float:
        raise NotImplementedError
