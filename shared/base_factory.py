"""Base class to provide an interface and helper functions for factory classes."""

import random
from typing import Iterable, Optional, Dict

from shared.base_equation import BaseEquation


class BaseFactory(object):
    DEFAULT_NUM_QUESTIONS = 10

    # Subclasses can override this as needed.
    NUMBER_OF_DIFFICULTY_LEVELS = 1

    def __init__(self) -> None:
        self._master_repeating_variables: Dict[int, Dict[str, list]] = {}
        self._next_repeating_variables: Dict[int, Dict[str, list]] = {}

    @property
    def title(self) -> str:
        raise NotImplementedError

    def description(self, mathjax: bool) -> str:
        raise NotImplementedError

    def generate(self, difficulty_level: int = 0) -> BaseEquation:
        raise NotImplementedError

    def generate_random_difficulty(self) -> BaseEquation:
        dice = random.randint(1, self.NUMBER_OF_DIFFICULTY_LEVELS)
        return self.generate(dice)

    def generate_set(self, total_questions: int = DEFAULT_NUM_QUESTIONS) -> Iterable[BaseEquation]:
        # Bias towards the harder questions.
        for difficulty_level in range(self.NUMBER_OF_DIFFICULTY_LEVELS):
            number = total_questions // self.NUMBER_OF_DIFFICULTY_LEVELS
            if difficulty_level > total_questions % self.NUMBER_OF_DIFFICULTY_LEVELS:
                number += 1
            for _ in range(number):
                yield self.generate(difficulty_level)

    def avoid_repeating_variable(self, key: str, variable: list, difficulty_level: Optional[int] = None) -> None:
        """Register a variable which we want to avoid repeating too much.  None adds to all difficulty levels."""
        if difficulty_level is None:
            for level in range(self.NUMBER_OF_DIFFICULTY_LEVELS):
                self.avoid_repeating_variable(key, variable, level)
            return

        if difficulty_level not in self._master_repeating_variables:
            self._master_repeating_variables[difficulty_level] = {}
            self._next_repeating_variables[difficulty_level] = {}

        self._master_repeating_variables[difficulty_level][key] = variable[:]
        self._next_repeating_variables[difficulty_level][key] = []

    def get_next_variables(self, difficulty_level: int = 0) -> Dict[str, object]:
        variables: Dict[str, object] = {}
        for key, values in self._next_repeating_variables[difficulty_level].items():
            if not values:
                values = self._master_repeating_variables[difficulty_level][key][:]
                random.shuffle(values)
                self._next_repeating_variables[difficulty_level][key] = values
            variables[key] = values.pop()
        return variables

    @staticmethod
    def generate_non_zero_int(minimum: int, maximum: int, not_one: bool = False) -> int:
        # Cheat!  Can probably do it nicer, but this works for now.
        coefficient = 0
        while coefficient == 0 or (coefficient == 1 and not_one):
            coefficient = random.randint(minimum, maximum)
        return coefficient
