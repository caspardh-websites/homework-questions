"""Basic enum to deal with units."""

import enum


class Units(enum.Enum):
    METERS = "m"
    CENTIMETERS = "cm"
    MILLIMETERS = "mm"
