"""Convenience enums for dealing with triangle angles and sides."""

import enum
from typing import Union, Tuple, Iterable


class Angle(enum.Enum):
    A = 0
    B = 1
    C = 2

    @property
    def corresponding_side(self) -> "Side":
        return Side(self.value)

    @property
    def anti_clockwise_angle(self) -> "Angle":
        return Angle((self.value - 1) % 3)

    @property
    def clockwise_angle(self) -> "Angle":
        return Angle((self.value + 1) % 3)

    @property
    def anti_clockwise_side(self) -> "Side":
        return self.corresponding_side.clockwise_side

    @property
    def clockwise_side(self) -> "Side":
        return self.corresponding_side.anti_clockwise_side

    def other_angles(self) -> Iterable["Angle"]:
        yield from self.corresponding_side.bounding_angles


class Side(enum.Enum):
    a = 0
    b = 1
    c = 2

    @property
    def corresponding_angle(self) -> Angle:
        return Angle(self.value)

    @property
    def bounding_angles(self) -> Tuple[Angle, Angle]:
        if self == Side.a:
            return Angle.B, Angle.C
        if self == Side.b:
            return Angle.C, Angle.A
        if self == Side.c:
            return Angle.B, Angle.A
        raise RuntimeError(f"Unrecognised Side: {self}")

    @property
    def anti_clockwise_angle(self) -> Angle:
        return self.corresponding_angle.clockwise_angle

    @property
    def clockwise_angle(self) -> Angle:
        return self.corresponding_angle.anti_clockwise_angle

    @property
    def anti_clockwise_side(self) -> "Side":
        return Side((self.value - 1) % 3)

    @property
    def clockwise_side(self) -> "Side":
        return Side((self.value + 1) % 3)


TriangleQuantity = Union[Angle, Side]
