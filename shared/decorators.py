"""Convenience decorators."""
from functools import wraps
from typing import Callable


def mathjax(inline: bool = True, degrees: bool = False) -> Callable:
    def mathjax_decorator(math_result: Callable) -> Callable[..., str]:
        @wraps(math_result)
        def wrap_mathjax(*args, **kwargs) -> str:
            result = math_result(*args, **kwargs)
            wrap_open = r"\(" if inline else r"\["
            wrap_close = r"\)" if inline else r"\]"
            degrees_symbol = r"^{\circ}" if degrees else ""
            result = f"{wrap_open}{result}{degrees_symbol}{wrap_close}"
            return result

        return wrap_mathjax

    return mathjax_decorator
