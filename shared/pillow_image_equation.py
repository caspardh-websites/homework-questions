"""Base class for all circle theorem equations."""
import dataclasses
import pathlib
from typing import Optional

from PIL import Image
from point2d import Point2D

from shared.image_equation import ImageEquation
from shared.rectangle import Rectangle


@dataclasses.dataclass
class PillowImageEquation(ImageEquation):
    image_height: int = 150
    image_width: int = 150
    padding_percent: float = 1.3

    def __post_init__(self):
        super().__init__()
        self._image = None  # type: Optional[Image]

        # Construct a Rectangle to provide convenient access to different points.
        top_left = Point2D(self._margin_width, self._margin_height)
        bottom_right = top_left + Point2D(self.image_width, self.image_height)
        self.rectangle = Rectangle(top_left, bottom_right)

    @property
    def _margin_width(self) -> int:
        return int((self.image_width * self.padding_percent - self.image_width) / 2)

    @property
    def _margin_height(self) -> int:
        return int((self.image_height * self.padding_percent - self.image_height) / 2)

    @property
    def bottom_left(self) -> Point2D:
        return Point2D(self._margin_width, self.image_height + self._margin_height)

    @property
    def top_right(self) -> Point2D:
        return Point2D(self._margin_width + self.image_width, self._margin_height)

    @property
    def centre(self) -> Point2D:
        return Point2D(self._margin_width + self.image_width // 2, self._margin_height + self.image_height // 2)

    def readable_format(self, mathjax: bool = False) -> str:
        raise NotImplementedError

    def readable_answer(self, mathjax: bool = False) -> str:
        raise NotImplementedError

    def _create_image(self, image_path: pathlib.Path) -> None:
        self._create_pillow_image()
        assert self._image is not None, "Failed to create pillow image"
        self._image.save(image_path)

    def _create_pillow_image(self) -> None:
        raise NotImplementedError

    def _new_image(self) -> Image:
        width_with_padding = self.image_width + 2 * self._margin_width
        height_with_padding = self.image_height + 2 * self._margin_height
        return Image.new("RGB", (width_with_padding, height_with_padding), color="white")
