"""Handles maths for points on circles."""

import math
from point2d import Point2D


class Circle(object):
    @staticmethod
    def angle(a_degrees: int, b_degrees: int, c_degrees: int) -> int:
        """Find the angle ABC in degrees."""
        centre = Circle.circumference_point(b_degrees)
        a_point = Circle.circumference_point(a_degrees)
        c_point = Circle.circumference_point(c_degrees)
        return Circle._angle_three_points(a_point, centre, c_point)

    @staticmethod
    def origin_angle(a_degrees: int, b_degrees: int) -> int:
        """Find the angle AOB in degrees."""
        a_point = Circle.circumference_point(a_degrees)
        b_point = Circle.circumference_point(b_degrees)
        origin = Point2D(0, 0)
        return Circle._angle_three_points(a_point, origin, b_point)

    @staticmethod
    def circumference_point(degrees: int) -> Point2D:
        """Returns the point `degrees` round on a unit circle."""
        return Point2D(r=1, a=math.radians(degrees))

    @staticmethod
    def _angle_three_points(a: Point2D, b: Point2D, c: Point2D) -> int:
        """The return angle ABC."""
        a_vector = a - b
        c_vector = c - b
        angle = a_vector.a - c_vector.a
        degrees = round(math.degrees(angle))
        # The angle might be positive or negative depending on which way round we happened to put points A and C.
        # We always want the positive.
        degrees = abs(degrees)
        # Always want the acute angle.
        if degrees <= 180:
            return degrees
        return 360 - degrees
