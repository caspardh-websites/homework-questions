"""Utility methods for Pythagoras."""
import math


def find_pythagoras_hypotenuse(side_a: int, side_b: int) -> int:
    side_c = math.sqrt(side_a * side_a + side_b * side_b)
    integer_side_c = int(round(side_c, 0))
    if side_c != integer_side_c:
        raise ValueError(f"Non-pythag triple values: {side_a}, {side_b}")
    return integer_side_c
