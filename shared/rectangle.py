"""Utility class for dealing with rectangles."""
from typing import Tuple, List

from point2d import Point2D


class Rectangle(object):
    def __init__(self, corner: Point2D, opposite_corner: Point2D) -> None:
        left_x = min(corner.x, opposite_corner.x)
        right_x = max(corner.x, opposite_corner.x)
        top_y = min(corner.y, opposite_corner.y)
        bottom_y = max(corner.y, opposite_corner.y)
        centre_x = left_x + (right_x - left_x) // 2
        centre_y = top_y + (bottom_y - top_y) // 2

        self.height = bottom_y - top_y
        self.width = right_x - left_x

        self.top_left = Point2D(left_x, top_y)
        self.top_right = Point2D(right_x, top_y)
        self.bottom_left = Point2D(left_x, bottom_y)
        self.bottom_right = Point2D(right_x, bottom_y)

        self.centre = Point2D(centre_x, centre_y)
        self.centre_left = Point2D(left_x, centre_y)
        self.centre_right = Point2D(right_x, centre_y)
        self.centre_top = Point2D(centre_x, top_y)
        self.centre_bottom = Point2D(centre_x, bottom_y)

    @property
    def extents(self) -> List[Tuple[int, int]]:
        """Designed to be used in conjunction with various PIL draw APIs."""
        return [self.top_left.cartesian(), self.bottom_right.cartesian()]

    def top_left_square(self) -> "Rectangle":
        size = min(self.height, self.width)
        bottom_right = self.top_left + Point2D(size, size)
        return Rectangle(self.top_left, bottom_right)

    def bottom_right_square(self) -> "Rectangle":
        size = min(self.height, self.width)
        top_left = self.bottom_right + Point2D(-size, -size)
        return Rectangle(top_left, self.bottom_right)

    def middle_square(self) -> "Rectangle":
        if self.height < self.width:
            top_left = self.centre_top + Point2D(-self.height // 2, 0)
            bottom_right = self.centre_top + Point2D(self.height // 2, 0)
        else:
            top_left = self.centre_left + Point2D(0, -self.width // 2)
            bottom_right = self.centre_right + Point2D(0, self.width // 2)
        return Rectangle(top_left, bottom_right)

    def with_padding(self, *, left: int = 0, top: int = 0, right: int = 0, bottom: int = 0) -> "Rectangle":
        top_left = self.top_left + Point2D(left, top)
        bottom_right = self.bottom_right - Point2D(right, bottom)
        return Rectangle(top_left, bottom_right)
