"""Utility script to setup logging in a common way.  Designed to be copy and pasted between projects."""

import logging.config
import pathlib
import os
import logging.handlers
import sys
import google.cloud.logging
from google.cloud.logging.handlers import CloudLoggingHandler
from google.cloud.logging.handlers import setup_logging as setup_gae_logging
from flask.logging import default_handler

logger = logging.getLogger()

DEFAULT_LOG_DIRECTORY = (pathlib.Path(__file__).parent / "tmp").resolve()
DEFAULT_NAME = "cdhapp"

LOG_FORMAT_STR = "[%(asctime)s] %(filename)15s:%(lineno)03d %(name)6.6s:%(levelname)-8s %(message)s"
DEFAULT_FORMATTER = logging.Formatter(LOG_FORMAT_STR)


def _debug_handler(log_path: pathlib.Path) -> logging.Handler:
    """Retrieves a debug log handler with the given parameters."""
    if not log_path.parent.exists():
        log_path.parent.mkdir(parents=True)
    ten_mb = 10485760
    handler = logging.handlers.RotatingFileHandler(str(log_path), maxBytes=ten_mb, backupCount=20)
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(DEFAULT_FORMATTER)
    return handler


def _stdout_handler() -> logging.Handler:
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)
    handler.setFormatter(DEFAULT_FORMATTER)
    return handler


def _gae_handler(app_name: str) -> logging.Handler:
    client = google.cloud.logging.Client()
    handler = CloudLoggingHandler(client, name=app_name)
    handler.setLevel(logging.INFO)
    handler.setFormatter(DEFAULT_FORMATTER)
    return handler


def running_in_gae() -> bool:
    return "GAE_APPLICATION" in os.environ


def setup_logging(app_name: str = DEFAULT_NAME) -> None:
    logger.setLevel(logging.DEBUG)
    if not running_in_gae():
        local_log_file = DEFAULT_LOG_DIRECTORY / f"{app_name}.log"
        logger.addHandler(_debug_handler(local_log_file))
        logger.addHandler(_stdout_handler())
        logger.addHandler(default_handler)
    else:
        setup_gae_logging(_gae_handler(app_name), excluded_loggers=("werkzeug",))
