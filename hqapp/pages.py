import random
from typing import Union

from flask import render_template, url_for, redirect
from werkzeug import Response

from hqapp import app
from hqapp import images
from question_sets.adhoc_sets.revision_200729 import RevisionSet200729
from question_sets.circle_theorems import CircleTheoremSet
from question_sets.revision_set import RevisionSet
from question_sets.sine_cosine import SineCosine
from questions.adding_fractions import AddingFractionsFactory
from questions.bearings import BearingsQuestionFactory
from questions.circle_volumes import CircleVolumeFactory
from questions.completing_square import CompleteSquareFactory
from questions.compound_shapes import CompoundShapeFactory
from questions.cosine_rule import CosineRuleFactory
from questions.ct_centre_angle import CircleTheoremCentreAngleFactory
from questions.ct_chord_triangles import CircleTheoremChordTrianglesFactory
from questions.ct_diameter_triangle import CircleTheoremDiameterTriangleFactory
from questions.ct_quad import CircleTheoremQuadrilateralFactory
from questions.equate_fractions import EquatingFractionsFactory
from questions.expanding_brackets import ExpandingBracketsFactory
from questions.factorising_quadratics import FactorisingQuadraticFactory
from questions.graph_recognition import RecogniseGraphFactory
from questions.interest_depreciation import CompoundInterestFactory
from questions.pythagoras_3d import Pythagoras3dFactory
from questions.quadratic_sequences import QuadraticSequenceFactory
from questions.simulataneous_equations import SimultaneousEquationFactory
from questions.simultaneous_quadratic import SimultaneousQuadraticFactory
from questions.sine_rule import SineRuleFactory
from questions.sine_triangle_area import SineAreaFactory
from questions.trig_values import TrigValuesFactory
from questions.turning_points import TurningPointFactory
from questions.volume_prisms import PrismVolumeFactory

SINGLE_QUESTIONS = {
    "simeq": SimultaneousEquationFactory,
    "simquad": SimultaneousQuadraticFactory,
    "completesquare": CompleteSquareFactory,
    "turningpoint": TurningPointFactory,
    "quadseq": QuadraticSequenceFactory,
    "ctcentreangle": CircleTheoremCentreAngleFactory,
    "ctdiametertriangle": CircleTheoremDiameterTriangleFactory,
    "ctchordtriangles": CircleTheoremChordTrianglesFactory,
    "ctquad": CircleTheoremQuadrilateralFactory,
    "sinerule": SineRuleFactory,
    "cosinerule": CosineRuleFactory,
    "sinearea": SineAreaFactory,
    "graphs": RecogniseGraphFactory,
    "trigvalues": TrigValuesFactory,
    "pythag3d": Pythagoras3dFactory,
    "prismvolume": PrismVolumeFactory,
    "circlevolumes": CircleVolumeFactory,
    "compoundshapes": CompoundShapeFactory,
    "expandbrackets": ExpandingBracketsFactory,
    "factorisequad": FactorisingQuadraticFactory,
    "equatefractions": EquatingFractionsFactory,
    "addingfractions": AddingFractionsFactory,
    "compoundinterest": CompoundInterestFactory,
    "bearings": BearingsQuestionFactory,
}

MIXED_QUESTIONS = {
    "basiccircletheorem": CircleTheoremSet,
    "sinecosine": SineCosine,
    "revision": RevisionSet,
    "revision_200729": RevisionSet200729,
}

app.jinja_env.globals["SINGLE_QUESTIONS"] = SINGLE_QUESTIONS
app.jinja_env.globals["MIXED_QUESTIONS"] = MIXED_QUESTIONS
app.jinja_env.globals["IMAGE_MANAGER"] = images.ImageManager()


@app.route("/")
def home():
    return render_template("home.html")


@app.route("/name/<set_name>")
@app.route("/name/<set_name>/seed/<int:seed>")
@app.route("/name/<set_name>/answers/<int:show_answers>")
@app.route("/name/<set_name>/seed/<int:seed>/answers/<int:show_answers>")
def question_sets(set_name: str, seed: int = 0, show_answers: int = 0) -> Union[str, Response]:
    # Use a seed of zero to say, pick a random seed.  Redirect so it is obvious what the seed is in the URL.
    if seed == 0:
        random.seed()
        new_set_seed = random.randint(1000, 9999)
        return redirect(url_for("question_sets", set_name=set_name, seed=new_set_seed, show_answers=show_answers))

    if set_name in SINGLE_QUESTIONS:
        question_set = SINGLE_QUESTIONS[set_name]()
    else:
        question_set = MIXED_QUESTIONS[set_name]()

    sister_answers_url = url_for("question_sets", set_name=set_name, seed=seed, show_answers=abs(show_answers - 1))

    # Generate a URL for another random set.  Need to actually have a random seed for that.
    random.seed()
    new_set_seed = random.randint(1000, 9999)
    random_url = url_for("question_sets", set_name=set_name, seed=new_set_seed, show_answers=show_answers)

    # Now fix the seed for this set.  The above should guarantee it is non-trivial.
    assert seed > 0
    random.seed(seed)
    image_manager: images.ImageManager = app.jinja_env.globals["IMAGE_MANAGER"]
    image_manager.clean_generated_images()

    return render_template(
        "single_question.html",
        question_set=question_set,
        image_manager=image_manager,
        show_answers=bool(show_answers),
        sister_answers_link=sister_answers_url,
        new_random_link=random_url,
    )
