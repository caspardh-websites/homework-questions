"""Functions to allow the core code to save images, and then make them retrievable in the HTML."""

import logging
import os
import pathlib
import random
import string
import tempfile
import time
from typing import Optional

from flask import send_from_directory, url_for
from google.cloud import storage

from hqapp import app
from shared.base_equation import BaseEquation


class ImageManager(object):
    """Handles image generation and storage."""

    BUCKET_ENV_KEY = "BUCKET_NAME"
    BUCKET_FOLDER = "tap"

    def __init__(self) -> None:
        self.image_folder = tempfile.TemporaryDirectory()

    def clean_generated_images(self) -> None:
        """Clean up all previously generated images.  They are super quick to generate so do this liberally."""
        self.image_folder.cleanup()
        self.image_folder = tempfile.TemporaryDirectory()

    def image_src(self, equation: BaseEquation) -> str:
        """Triggers the equation to generate an image.  Returns the name to reference using `generated_image`."""
        image_path = self._generate_unique_path()
        logging.info("Generating image to path %s", image_path)
        equation.generate_image(image_path)
        if self._cloud_save_enabled():
            return self._save_image_to_cloud_storage(image_path)
        return url_for("generated_image", image_name=image_path.name)

    def _generate_unique_path(self, prefix: str = "") -> pathlib.Path:
        """Generates a unique location to store a filename."""
        time_str = str(time.time()).replace(".", "")
        suffix = self._random_string()
        while self._construct_filepath(prefix, time_str, suffix).exists():
            suffix = self._random_string()
        return self._construct_filepath(prefix, time_str, suffix)

    def _construct_filepath(self, prefix: str, time_str: str, suffix: str) -> pathlib.Path:
        filename = f"image_{time_str}_{suffix}.png"
        if prefix:
            filename = f"{prefix}_{filename}"
        return pathlib.Path(self.image_folder.name) / filename

    @staticmethod
    def _random_string(num_chars: int = 16) -> str:
        return "".join(random.choice(string.ascii_lowercase) for _ in range(num_chars))

    @staticmethod
    def _save_image_to_cloud_storage(image_path: pathlib.Path) -> str:
        bucket = ImageManager._cloud_bucket()
        destination_name = f"{ImageManager.BUCKET_FOLDER}/{image_path.name}"
        blob = bucket.blob(destination_name)
        blob.upload_from_filename(str(image_path.resolve()))
        return ImageManager._cloud_image_url(image_path.name)

    @staticmethod
    def _cloud_bucket_name() -> Optional[str]:
        return os.environ.get(ImageManager.BUCKET_ENV_KEY)

    @staticmethod
    def _cloud_bucket():
        storage_client = storage.Client()
        return storage_client.bucket(ImageManager._cloud_bucket_name())

    @staticmethod
    def _cloud_save_enabled() -> bool:
        return ImageManager._cloud_bucket_name() is not None

    @staticmethod
    def _cloud_image_url(image_name: str) -> str:
        return f"https://storage.cloud.google.com/{ImageManager._cloud_bucket_name()}/tap/{image_name}"


@app.route("/generated_images/<path:image_name>")
def generated_image(image_name: str):
    """Allows HTML code to use `url_for('generated_image', filename='my_image.png')"""
    image_manager: ImageManager = app.jinja_env.globals["IMAGE_MANAGER"]
    return send_from_directory(image_manager.image_folder.name, image_name, as_attachment=True)
