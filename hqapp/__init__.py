import importlib
import pathlib

from flask import Flask
from flask_sslify import SSLify

from setup_logging import setup_logging

ROOT_FOLDER = pathlib.Path(__file__).parent.parent


setup_logging()

# Setup the app so it can be imported
app = Flask(__name__)
app.secret_key = "a42a444b-4d45-4103-970d-883fdb98657d"
SSLify(app)

importlib.import_module("hqapp.pages")
