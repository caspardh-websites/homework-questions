"""Allow the app to be run by Flask."""

# This is a little tedious but keeps the linting happy.
import importlib

module = importlib.import_module("hqapp")
app = getattr(module, "app")
