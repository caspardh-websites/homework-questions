"""Draw some basic 3D shapes."""

from PIL import Image
from point2d import Point2D

from images.drawing import Drawing
from shared.rectangle import Rectangle
from shared.units import Units


class ShapeCylinder(Drawing):
    def __init__(self, base_image: Image, space: Rectangle) -> None:
        super().__init__(base_image)
        self.total_space = space

        ellipse_half_height = self.perspective_ellipse_height(space.width) // 2
        self.space_no_ellipsis = space.with_padding(top=ellipse_half_height, bottom=ellipse_half_height)

    def draw_cylinder(self, *, force_dotted_back_ellipse: bool = False) -> None:
        space = self.space_no_ellipsis
        self.draw_perspective_ellipse(space.top_left, space.top_right, dotted_back=force_dotted_back_ellipse)
        self.draw_perspective_ellipse(space.bottom_left, space.bottom_right, dotted_back=True)
        self.draw_line(space.top_left, space.bottom_left)
        self.draw_line(space.top_right, space.bottom_right)

    def draw_radius(self) -> None:
        self.draw_dashed_line(self.space_no_ellipsis.centre_top, self.space_no_ellipsis.top_right)

    def label_height_value(self, length: float, units: Units) -> None:
        self.label_height(f"{length}{units.value}")

    def label_radius_value(self, length: float, units: Units) -> None:
        self.label_radius(f"{length}{units.value}")

    def label_height(self, label: str) -> None:
        height_middle = self.total_space.centre_right + Drawing.LABEL_OFFSET_RIGHT_OF_LINE
        self.draw_label(height_middle, label)

    def label_radius(self, label: str) -> None:
        self.draw_radius()
        depth_middle = (
            self.space_no_ellipsis.centre_top
            + Point2D(self.space_no_ellipsis.width // 4, 0)
            + Drawing.LABEL_OFFSET_ABOVE_LINE
        )
        self.draw_label(depth_middle, label)
