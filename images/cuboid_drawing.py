"""Draw some basic 3D shapes."""
import math

from PIL import Image
from point2d import Point2D

from images.drawing import Drawing
from shared.units import Units


class ShapeCuboid(Drawing):
    def __init__(self, base_image: Image, side_length: int, bottom_left: Point2D) -> None:
        super().__init__(base_image)
        self.side_length = side_length

        # Calculate the remaining front vertices.
        self.front_bottom_left = bottom_left
        self.front_top_left = self.front_bottom_left - Point2D(0, self.side_length)
        self.front_bottom_right = self.front_bottom_left + Point2D(self.side_length, 0)
        self.front_top_right = self.front_bottom_left + Point2D(self.side_length, -self.side_length)
        # Calculate the back vertices.
        diagonal_back = Point2D(a=-math.pi / 4, r=self.side_length)
        self.back_bottom_left = self.front_bottom_left + diagonal_back
        self.back_bottom_right = self.front_bottom_right + diagonal_back
        self.back_top_left = self.front_top_left + diagonal_back
        self.back_top_right = self.front_top_right + diagonal_back
        # Calculate some convenient midway points.
        half_diagonal_back = Point2D(diagonal_back)
        half_diagonal_back.r /= 2
        self.front_bottom_middle = self.front_bottom_left + Point2D(self.side_length // 2, 0)
        self.depth_bottom_middle = self.front_bottom_right + half_diagonal_back
        self.back_right_middle = self.back_bottom_right + Point2D(0, -self.side_length // 2)

    def draw_cube(self) -> None:
        # Draw front face.
        self.draw_line(
            self.front_bottom_left,
            self.front_bottom_right,
            self.front_top_right,
            self.front_top_left,
            back_to_start=True,
        )
        # Draw remaining visible lines.
        self.draw_line(
            self.front_bottom_right,
            self.back_bottom_right,
            self.back_top_right,
            self.back_top_left,
            self.front_top_left,
        )
        self.draw_line(self.front_top_right, self.back_top_right)
        # Draw hidden lines.
        self.draw_dashed_line(self.front_bottom_left, self.back_bottom_left, self.back_top_left)
        self.draw_dashed_line(self.back_bottom_left, self.back_bottom_right)

    def label_bottom_side(self, length: float, units: Units) -> None:
        bottom_middle = self.front_bottom_middle + Point2D(0, 5)
        self.draw_label(bottom_middle, f"{length}{units.value}")

    def label_depth(self, length: float, units: Units) -> None:
        depth_middle = self.depth_bottom_middle + Point2D(5, 0)
        self.draw_label(depth_middle, f"{length}{units.value}")

    def label_height(self, length: float, units: Units) -> None:
        height_middle = self.back_right_middle + Point2D(5, 0)
        self.draw_label(height_middle, f"{length}{units.value}")

    def label_top_left_vertex(self, label: str) -> None:
        bottom_left = self.front_top_left + Point2D(-8, -8)
        self.draw_label(bottom_left, label)

    def label_bottom_right_vertex(self, label: str) -> None:
        bottom_left = self.back_bottom_right + Point2D(5, 5)
        self.draw_label(bottom_left, label)

    def label_bottom_middle_point(self, label: str) -> None:
        bottom_left = self.depth_bottom_middle + Point2D(0, -15)
        self.draw_label(bottom_left, label)

    def draw_top_left_bottom_right_diagonal(self) -> None:
        self.draw_single_dashed_line(self.front_top_left, self.back_bottom_right, 3, 3)

    def draw_top_left_middle_right_diagonal(self) -> None:
        self.draw_single_dashed_line(self.front_top_left, self.depth_bottom_middle, 3, 3)
