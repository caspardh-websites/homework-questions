"""Utility for questions which want to draw graphs."""
import dataclasses
import math
import pathlib
from typing import Optional, Tuple, Callable, List

import matplotlib.pyplot as plt
import numpy


@dataclasses.dataclass
class GraphConfig(object):
    x_min: float
    x_max: float
    x_step: float = 0.01
    y_min: Optional[float] = None
    y_max: Optional[float] = None
    discontinuity_check: Optional[Callable[[float, float], bool]] = None


class Graph(object):
    def __init__(self, equation_callback: numpy.ufunc, graph_config: GraphConfig) -> None:
        self.equation_callback = equation_callback
        self.graph_config = graph_config
        self.mark_x_points: List[float] = []
        self.mark_y_points: List[float] = []

    def generate_image(self, image_path: pathlib.Path, image_size_pixels: Optional[Tuple[int, int]] = None) -> None:
        x_values = numpy.arange(self.graph_config.x_min, self.graph_config.x_max, self.graph_config.x_step)
        y_values = self.equation_callback(x_values)

        if self.graph_config.discontinuity_check is not None:
            y_values[1:][numpy.diff(y_values) > 1] = numpy.nan

        figure, axes = plt.subplots()

        self._setup_axes_limits()
        self._setup_axis_style(axes)
        axes.plot(x_values, y_values)

        if image_size_pixels is not None:
            # Default DPI is 80 pixels per inch.
            figure.set_size_inches(image_size_pixels[0] / 80, image_size_pixels[1] / 80)

        plt.savefig(image_path)
        plt.close()

    def _setup_axis_style(self, axes) -> None:
        # Set the x and y axis to go through the zero position of the other.
        axes.spines["left"].set_position("zero")
        axes.spines["bottom"].set_position("zero")

        # Turn off any axis at the other edges.
        axes.spines["right"].set_color("none")
        axes.spines["top"].set_color("none")

        # Turn off all ticks and labels for those ticks.
        axes.xaxis.set_ticks(self.mark_x_points)
        axes.yaxis.set_ticks(self.mark_y_points)

    def _setup_axes_limits(self) -> None:
        # The x limits are done by definition given that is the range we defined in the data.
        if self.graph_config.y_min is None:
            return

        plt.ylim(self.graph_config.y_min, self.graph_config.y_max)


TrigConfig = GraphConfig(x_min=-2 * math.pi, x_max=2 * math.pi)

TanConfig = GraphConfig(
    x_min=-2 * math.pi, x_max=2 * math.pi, y_min=-2, y_max=2, discontinuity_check=lambda x, y: x > 0 > y
)

PolynomialConfig = GraphConfig(x_min=-10, x_max=10, y_min=-10, y_max=10)

ReciprocalConfig = GraphConfig(x_min=-6, x_max=6, y_min=-10, y_max=10, discontinuity_check=lambda x, y: y > 0 > x)
