"""Draw some basic 3D shapes."""

from PIL import Image
from point2d import Point2D

from images.drawing import Drawing
from shared.rectangle import Rectangle


class ShapeCone(Drawing):
    def __init__(
        self, base_image: Image, space: Rectangle, *, include_ellipse: bool = True, upwards: bool = True
    ) -> None:
        super().__init__(base_image)
        self.space = space
        self.include_ellipse = include_ellipse
        self.upwards = upwards

        half_ellipse_height = self.perspective_ellipse_height(space.width) // 2
        if upwards:
            self.cone_point = space.centre_top
            self.left_point = space.bottom_left
            ellipse_offset = Point2D(0, -half_ellipse_height)
        else:
            self.cone_point = space.centre_bottom
            self.left_point = space.top_left
            ellipse_offset = Point2D(0, half_ellipse_height)

        if include_ellipse:
            self.left_point += ellipse_offset
        self.right_point = self.left_point + Point2D(space.width, 0)
        self.centre_base = self.left_point + Point2D(space.width // 2, 0)

    def draw_cone(self) -> None:
        self.draw_line(self.left_point, self.cone_point, self.right_point)
        if self.include_ellipse:
            self.draw_perspective_ellipse(self.left_point, self.right_point, dotted_back=self.upwards)

    def label_height(self, label: str) -> None:
        self.draw_dashed_line(self.centre_base, self.cone_point)
        location = self.space.centre + Drawing.LABEL_OFFSET_RIGHT_OF_LINE
        self.draw_label(location, label)

    def label_radius(self, label: str) -> None:
        self.draw_dashed_line(self.centre_base, self.right_point)
        location = self.centre_base + Point2D(self.space.width // 4, 0) + Drawing.LABEL_OFFSET_ABOVE_LINE
        self.draw_label(location, label)
