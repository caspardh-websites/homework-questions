"""Draw some basic 3D shapes."""
import math

from PIL import Image
from point2d import Point2D

from images.drawing import Drawing
from shared.units import Units


class ShapeTriangularPrism(Drawing):
    def __init__(self, base_image: Image, side_length: int, bottom_left: Point2D) -> None:
        super().__init__(base_image)
        self.side_length = side_length

        # Calculate the front vertices.
        self.front_left = bottom_left
        self.front_right = self.front_left + Point2D(self.side_length, 0)
        right_slope = Point2D(a=-math.pi * 2 / 3, r=side_length)
        self.front_top = self.front_right + right_slope

        # Calculate the back vertices.
        diagonal_back = Point2D(a=-math.pi / 4, r=self.side_length)
        self.back_right = self.front_right + diagonal_back
        self.back_top = self.back_right + right_slope

        # Calculate some convenient midway points.
        half_diagonal_back = Point2D(diagonal_back)
        half_diagonal_back.r /= 2
        self.front_middle = self.front_left + Point2D(self.side_length // 2, 0)
        self.depth_middle = self.front_right + half_diagonal_back
        self.back_right_middle = self.back_right + Point2D(0, -self.side_length // 2)
        height = self.front_middle.y - self.front_top.y
        self.height_middle = self.front_middle + Point2D(0, -height // 2)

    def draw_prism(self) -> None:
        # Draw front face.
        self.draw_line(self.front_left, self.front_right, self.front_top, back_to_start=True)
        # Draw remaining visible lines.
        self.draw_line(self.front_right, self.back_right, self.back_top, self.front_top)

    def draw_height(self) -> None:
        self.draw_dashed_line(self.front_middle, self.front_top)

    def label_base(self, length: float, units: Units) -> None:
        bottom_middle = self.front_middle + Point2D(0, 5)
        self.draw_label(bottom_middle, f"{length}{units.value}")

    def label_depth(self, length: float, units: Units) -> None:
        depth_middle = self.depth_middle + Point2D(5, 0)
        self.draw_label(depth_middle, f"{length}{units.value}")

    def label_height(self, length: float, units: Units) -> None:
        height_middle = self.height_middle + Point2D(5, 5)
        self.draw_label(height_middle, f"{length}{units.value}")
