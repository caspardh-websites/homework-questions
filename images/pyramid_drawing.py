"""Draw some basic 3D shapes."""
import math

from PIL import Image
from point2d import Point2D

from images.drawing import Drawing
from shared.units import Units


class ShapePyramid(Drawing):
    """Draws a square-based pyramid."""

    def __init__(self, base_image: Image, base_length: int, apex_height: int, bottom_left: Point2D) -> None:
        super().__init__(base_image)
        self.base_length = base_length

        # Calculate the vertices of the base of the pyramid.
        self.base_front_left = bottom_left
        self.base_front_right = self.base_front_left + Point2D(self.base_length, 0)

        diagonal_back = Point2D(a=-math.pi / 4, r=self.base_length)
        self.base_back_left = self.base_front_left + diagonal_back
        self.base_back_right = self.base_front_right + diagonal_back

        # Calculate the apex.
        total_width_midpoint = (self.base_back_right.x - self.base_front_left.x) / 2
        self.apex = Point2D(self.base_front_left.x + total_width_midpoint, self.base_front_left.y - apex_height)

        # Calculate some handy midway points.
        half_diagonal_back = Point2D(diagonal_back)
        half_diagonal_back.r /= 2
        self.base_front_middle = self.base_front_left + Point2D(base_length // 2, 0)
        self.base_right_middle = self.base_front_right + half_diagonal_back
        self.base_middle = self.base_front_middle + half_diagonal_back
        slope_vector = self.base_back_right - self.apex
        self.slope_middle_back_right = Point2D(self.apex.x + slope_vector.x / 2, self.apex.y + slope_vector.y / 2)

    def draw_pyramid(self) -> None:
        # Draw visible edges.
        self.draw_line(self.base_front_left, self.base_front_right, self.base_back_right, self.apex, back_to_start=True)
        # Draw remaining visible line.
        self.draw_line(self.base_front_right, self.apex)
        # Draw hidden lines.
        self.draw_dashed_line(self.base_front_left, self.base_back_left, self.base_back_right)
        self.draw_dashed_line(self.base_back_left, self.apex)

    def draw_height(self) -> None:
        self.draw_single_dashed_line(self.base_middle, self.apex, 3, 3)

    def label_base_width(self, length: int, units: Units) -> None:
        bottom_middle = self.base_front_middle + Point2D(0, 5)
        self.draw_label(bottom_middle, f"{length}{units.value}")

    def label_depth(self, length: int, units: Units) -> None:
        depth_middle = self.base_right_middle + Point2D(5, 0)
        self.draw_label(depth_middle, f"{length}{units.value}")

    def label_slope(self, length: int, units: Units) -> None:
        slope_middle = self.slope_middle_back_right + Point2D(5, -5)
        self.draw_label(slope_middle, f"{length}{units.value}")

    def label_height(self, length: int, units: Units) -> None:
        base_middle_offset = self.base_middle + Point2D(-15, 0)
        self.draw_label(base_middle_offset, f"{length}{units.value}")
