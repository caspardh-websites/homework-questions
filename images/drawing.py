"""Base class to provide utility drawing functions."""
from typing import Tuple

from PIL import Image, ImageDraw
from point2d import Point2D


class Drawing(object):
    LINE_WIDTH = 3

    # These are values determined via trial and error.
    LABEL_OFFSET_RIGHT_OF_LINE = Point2D(5, 0)
    LABEL_OFFSET_ABOVE_LINE = Point2D(0, -12)

    def __init__(self, base_image: Image) -> None:
        self.base_image = base_image
        self._drawing = ImageDraw.Draw(self.base_image)

    def draw_label(self, position: Point2D, label: str) -> None:
        self._drawing.text(position.cartesian(), label, fill="black")

    def draw_line(self, *args: Point2D, back_to_start: bool = False) -> None:
        points = [point.cartesian() for point in args]
        if back_to_start:
            points.append(points[0])
        self._drawing.line(points, fill="black", width=self.LINE_WIDTH)

    def draw_arc(
        self, arc_top_left: Point2D, arc_bottom_right: Point2D, start_degrees: float, end_degrees: float
    ) -> None:
        self._drawing.arc(
            [arc_top_left.cartesian(), arc_bottom_right.cartesian()],
            start_degrees,
            end_degrees,
            fill="black",
            width=self.LINE_WIDTH,
        )

    def draw_ellipse(self, ellipse_top_left: Point2D, ellipse_bottom_right: Point2D) -> None:
        self.draw_arc(ellipse_top_left, ellipse_bottom_right, 0, 360)

    def draw_half_ellipse(self, ellipse_top_left: Point2D, ellipse_bottom_right: Point2D, bottom: bool = True) -> None:
        if bottom:
            start = 0
            end = 180
        else:
            start = 180
            end = 360
        self.draw_arc(ellipse_top_left, ellipse_bottom_right, start, end)

    def draw_dashed_half_ellipse(
        self, ellipse_top_left: Point2D, ellipse_bottom_right: Point2D, bottom: bool = True
    ) -> None:
        if bottom:
            start = 0
            end = 180
        else:
            start = 180
            end = 360
        self.draw_dashed_arc(ellipse_top_left, ellipse_bottom_right, start, end)

    def draw_dashed_arc(
        self, arc_top_left: Point2D, arc_bottom_right: Point2D, start_degrees: int, end_degrees: int
    ) -> None:
        dash_degrees = 10
        gap_degrees = 7
        length = end_degrees - start_degrees
        num_gaps, tweaked_dash, tweaked_gap = self._tweak_dash_gap(dash_degrees, gap_degrees, length)
        for ii in range(num_gaps):
            start = start_degrees + ii * (tweaked_dash + tweaked_gap)
            end = start + tweaked_dash
            self.draw_arc(arc_top_left, arc_bottom_right, start, end)
        self.draw_arc(arc_top_left, arc_bottom_right, end_degrees - tweaked_dash, end_degrees)

    def draw_dashed_line(self, *args: Point2D, back_to_start: bool = False) -> None:
        points = list(args)
        for from_point, to_point in zip(points[:-1], points[1:]):
            self.draw_single_dashed_line(from_point, to_point)
        if back_to_start:
            self.draw_single_dashed_line(points[-1], points[0])

    def draw_single_dashed_line(
        self, start: Point2D, end: Point2D, dash_length_pixels: int = 10, gap_length_pixels: int = 7
    ) -> None:
        delta = end - start
        length = delta.r
        # Even out the dashes and gaps so it evenly spreads over the line.
        num_gaps, tweaked_dash, tweaked_gap = self._tweak_dash_gap(dash_length_pixels, gap_length_pixels, length)
        tweaked_cycle = tweaked_dash + tweaked_gap
        # Now draw the line.
        unit_delta = Point2D(delta)
        unit_delta.r = 1
        for ii in range(num_gaps):
            dash_start = start + unit_delta * ii * tweaked_cycle
            dash_end = dash_start + unit_delta * tweaked_dash
            self.draw_line(dash_start, dash_end)
        dash_start = end - unit_delta * tweaked_dash
        self.draw_line(dash_start, end)

    @staticmethod
    def perspective_ellipse_height(width: int) -> int:
        # Determined from trial and error.
        return width // 3

    def draw_perspective_ellipse(self, left_point: Point2D, right_point: Point2D, *, dotted_back: bool) -> None:
        """Draw a circle in perspective, as an ellipse.  Left and right points must be on a horizontal line."""
        assert left_point.y == right_point.y, "Can only draw perspective ellipse on a horizontal line"
        # The height of the ellipse being 1/3 of the width was determined from trial and error.
        # We need the top-left and bottom-right to draw the ellipse.
        ellipse_half_height = self.perspective_ellipse_height(right_point.x - left_point.x) // 2
        half_height_up = Point2D(0, -ellipse_half_height)
        top_left = left_point + half_height_up
        bottom_right = right_point - half_height_up
        if not dotted_back:
            self.draw_ellipse(top_left, bottom_right)
        else:
            # Front is always full.
            self.draw_half_ellipse(top_left, bottom_right, bottom=True)
            self.draw_dashed_half_ellipse(top_left, bottom_right, bottom=False)

    @staticmethod
    def _tweak_dash_gap(dash: int, gap: int, total: int) -> Tuple[int, float, float]:
        # Always 1 more dash than a gap.
        number_gaps = int((total - dash) / (dash + gap))
        # The above rounds down so will always be under (or exact).  Multiply up to make it exact.
        length_tweak = total / (number_gaps * (dash + gap) + dash)
        tweaked_dash = dash * length_tweak
        tweaked_gap = gap * length_tweak
        return number_gaps, tweaked_dash, tweaked_gap
