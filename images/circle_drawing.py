"""Class used for drawing circles, and lines / points on it."""

import math

from PIL import Image
from point2d import Point2D

from images.drawing import Drawing

LINE_WIDTH = 3


class CircleDrawing(Drawing):
    DEFAULT_RADIUS = 100
    LABEL_OFFSET = 15

    def __init__(
        self, base_image: Image, origin: Point2D, radius: int = DEFAULT_RADIUS, draw_circle: bool = True
    ) -> None:
        super().__init__(base_image)
        self.origin = origin
        self.radius = radius
        self.top_left = origin + Point2D(-radius, -radius)
        self.bottom_right = origin + Point2D(radius, radius)
        if draw_circle:
            self.draw_circle()

    def draw_origin_line(self, degrees: int) -> None:
        circumference_point = self._circumference_point(degrees)
        self.draw_line(self.origin, circumference_point)

    def draw_dashed_origin_line(self, degrees: int) -> None:
        circumference_point = self._circumference_point(degrees)
        self.draw_dashed_line(self.origin, circumference_point)

    def draw_north_arrow(self, add_label: bool = True) -> None:
        circumference_point = self.origin + Point2D(0, -self.radius)
        left_tip = circumference_point + Point2D(-5, 5)
        right_tip = circumference_point + Point2D(5, 5)
        self.draw_line(self.origin, circumference_point)
        self.draw_line(left_tip, circumference_point, right_tip)

        if add_label:
            self.draw_letter_circumference(270, "N")

    def draw_label_arc(self, start_degrees: int, end_degrees: int) -> None:
        self.draw_arc(self.top_left, self.bottom_right, start_degrees, end_degrees)
        angle_difference = (end_degrees - start_degrees) % 360
        half_arc_point = start_degrees + angle_difference // 2
        self.draw_letter_circumference(half_arc_point, f"{angle_difference}")

    def draw_chord(self, degrees_from: int, degrees_to: int) -> None:
        self.draw_chords(degrees_from, degrees_to)

    def draw_chords(self, *args: int, back_to_start: bool = False) -> None:
        self.draw_line(*[self._circumference_point(point) for point in args], back_to_start=back_to_start)

    def draw_letter_circumference(self, degrees: int, letter: str) -> None:
        circumference_point = self._outside_circumference(degrees)
        self.draw_label(circumference_point, letter)

    def label_origin(self, above: bool = True, letter: str = "O") -> None:
        offset = Point2D(self.LABEL_OFFSET, 0)
        position = self.origin - offset if above else self.origin + offset
        self.draw_label(position, letter)

    def label_chord(self, degrees_from: int, degrees_to: int, label: str, margin: int = LABEL_OFFSET) -> None:
        chord_start = self._circumference_point(degrees_from)
        chord_end = self._circumference_point(degrees_to)
        chord_middle = chord_start + (chord_end - chord_start) * 0.5
        chord_middle_from_origin = chord_middle - self.origin
        chord_middle_from_origin.r += margin
        chord_middle_label = self.origin + chord_middle_from_origin
        self.draw_label(chord_middle_label, label)

    def draw_circle(self) -> None:
        offset = Point2D(self.radius, self.radius)
        top_left = self.origin - offset
        bottom_right = self.origin + offset
        self._drawing.ellipse([top_left.cartesian(), bottom_right.cartesian()], outline="black", width=LINE_WIDTH)

    def _circumference_point(self, degrees: int) -> Point2D:
        return self._point_from_origin(degrees, self.radius)

    def _outside_circumference(self, degrees: int, margin: int = LABEL_OFFSET) -> Point2D:
        return self._point_from_origin(degrees, self.radius + margin)

    def _point_from_origin(self, degrees: int, radius: int) -> Point2D:
        return Point2D(r=radius, a=math.radians(degrees)) + self.origin
