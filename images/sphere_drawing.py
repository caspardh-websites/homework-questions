"""Draw some basic 3D shapes."""

from PIL import Image
from point2d import Point2D

from images.drawing import Drawing
from shared.rectangle import Rectangle


class ShapeSphere(Drawing):
    def __init__(self, base_image: Image, space: Rectangle) -> None:
        """Corner and opposite corner can be any two opposing corners which define the available image space."""
        super().__init__(base_image)
        self.space = space

    def draw_sphere(self) -> None:
        self.draw_ellipse(self.space.top_left, self.space.bottom_right)
        self.draw_perspective_ellipse(self.space.centre_left, self.space.centre_right, dotted_back=True)

    def draw_hemisphere(self, bottom: bool = True, force_dotted_back_ellipse: bool = False) -> None:
        self.draw_half_ellipse(self.space.top_left, self.space.bottom_right, bottom=bottom)
        dotted_back = not bottom or force_dotted_back_ellipse
        self.draw_perspective_ellipse(self.space.centre_left, self.space.centre_right, dotted_back=dotted_back)

    def draw_hemisphere_outside(self, bottom: bool = True) -> None:
        # We want to draw a hemisphere outside that takes up the whole space.
        # The width is fixed, if we don't have enough height, do what we can.  If we have too much, trim.
        height = min(self.space.height, self.space.width // 2)
        # Now, fake up a space for which the space we've got is half.
        if bottom:
            top_left = self.space.top_left + Point2D(0, -height)
            bottom_right = self.space.top_right + Point2D(0, height)
        else:
            top_left = self.space.bottom_left + Point2D(0, -height)
            bottom_right = self.space.bottom_right + Point2D(0, height)

        fake_space = Rectangle(top_left, bottom_right)
        self.draw_half_ellipse(fake_space.top_left, fake_space.bottom_right, bottom=bottom)

    def label_radius(self, label: str) -> None:
        self.draw_dashed_line(self.space.centre, self.space.centre_right)
        location = self.space.centre + Point2D(self.space.width // 4, 0) + Drawing.LABEL_OFFSET_ABOVE_LINE
        self.draw_label(location, label)
