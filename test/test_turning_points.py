"""Test the turning point equation class."""
import pytest

from questions.turning_points import TurningPointEquation


@pytest.mark.parametrize(
    "turning_x, turning_y, multiplier, expected",
    ((1, 1, 1, "(1, 1)"), (1, 1, -5, "(1, 1)"), (53, -23, 856, "(53, -23)")),
)
def test_readable_answer(turning_x: int, turning_y: int, multiplier: int, expected: str) -> None:
    equation = TurningPointEquation(turning_x, turning_y, multiplier)
    assert equation.readable_answer(mathjax=False) == expected


@pytest.mark.parametrize(
    "turning_x, turning_y, multiplier, expected",
    ((1, 1, 1, "y = x^2 - 2x + 2"), (1, 1, 15, "y = 15x^2 - 30x + 16"), (1, 25, 1, "y = x^2 - 2x + 26")),
)
def test_readable_question(turning_x: int, turning_y: int, multiplier: int, expected: str) -> None:
    equation = TurningPointEquation(turning_x, turning_y, multiplier)
    readable_question = equation.readable_format(mathjax=False)
    assert readable_question.endswith(expected), f"{expected} not the end of {readable_question}"
