import pytest

from questions.simultaneous_quadratic import SimultaneousQuadratic


def test_basic_results() -> None:
    equation = SimultaneousQuadratic(1, 1, 1, 1)
    assert equation.result_linear == 2
    assert equation.result_quadratic == 2


def test_other_results() -> None:
    equation = SimultaneousQuadratic(-1, 1, 1, 1)
    assert equation.find_other_solution() == (1, -1)

    equation = SimultaneousQuadratic(2, 5, -2, 2)
    assert equation.find_other_solution() == (-5, -2)


def test_answer_output() -> None:
    equation = SimultaneousQuadratic(1, -5, 2, 2)
    assert equation.find_other_solution() == (-5, 1)
    assert equation.readable_answer() == "x = 1; y = -5 or x = -5; y = 1"


def test_single_result() -> None:
    equation = SimultaneousQuadratic(1, 1, 1, 1)
    with pytest.raises(ValueError):
        equation.find_other_solution()
    assert equation.readable_answer() == "x = 1; y = 1"
