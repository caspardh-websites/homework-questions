"""Test functions for the completing the square equation."""

import pytest

from questions.completing_square import CompleteSquareEquation


# This is a very light test as testing should be covered by the underlying function it uses
@pytest.mark.parametrize(
    "quadratic,linear,constant,expected",
    [(1, 1, 1, "x^2 + x + 1 = 0"), (-1, -1, -1, "-x^2 - x - 1 = 0"), (5, -1, 13, "5x^2 - x + 13 = 0")],
)
def test_readable(quadratic: int, linear: int, constant: int, expected: str) -> None:
    """Test that the formatting of the function is correct."""
    equation = CompleteSquareEquation(quadratic, linear, constant)
    assert expected in equation.readable_format()
