"""Test circle volume questions."""
from fractions import Fraction

import pytest

from questions.circle_volumes import Shape, CircleVolumeQuestion, QuestionType
from shared.units import Units


@pytest.mark.parametrize(
    "radius, expected_volume", [(1, Fraction(4, 3)), (2, Fraction(32, 3)), (6, Fraction(288))],
)
def test_sphere_volume(radius: int, expected_volume: Fraction) -> None:
    question = CircleVolumeQuestion(shape_type=Shape.SPHERE, radius=radius)
    assert question.volume_in_terms_of_pi() == expected_volume
    question = CircleVolumeQuestion(shape_type=Shape.HEMISPHERE, radius=radius)
    assert question.volume_in_terms_of_pi() == expected_volume / 2


@pytest.mark.parametrize(
    "radius, cone_height, expected_volume", [(1, 1, Fraction(1, 3)), (2, 3, Fraction(4)), (4, 2, Fraction(32, 3))],
)
def test_cone_volume(radius: int, cone_height: int, expected_volume: Fraction) -> None:
    question = CircleVolumeQuestion(shape_type=Shape.CONE, radius=radius, height=cone_height)
    assert question.volume_in_terms_of_pi() == expected_volume


@pytest.mark.parametrize(
    "radius, cylinder_height, expected_volume", [(1, 1, 1), (2, 3, 12), (4, 7, 112)],
)
def test_cylinder_volume(radius: int, cylinder_height: int, expected_volume: int) -> None:
    question = CircleVolumeQuestion(shape_type=Shape.CYLINDER, radius=radius, height=cylinder_height)
    assert question.volume_in_terms_of_pi() == expected_volume


@pytest.mark.parametrize(
    "radius, expected_area", [(1, 4), (2, 16), (7, 196)],
)
def test_sphere_surface_area(radius: int, expected_area: int) -> None:
    question = CircleVolumeQuestion(shape_type=Shape.SPHERE, radius=radius)
    assert question.surface_area_in_terms_of_pi() == expected_area


@pytest.mark.parametrize(
    "radius, expected_area", [(1, 3), (5, 75), (12, 432)],
)
def test_hemisphere_surface_area(radius: int, expected_area: int) -> None:
    question = CircleVolumeQuestion(shape_type=Shape.HEMISPHERE, radius=radius)
    assert question.surface_area_in_terms_of_pi() == expected_area


@pytest.mark.parametrize(
    "radius, cone_height, expected_area", [(3, 4, 24), (4, 3, 36), (8, 15, 200)],
)
def test_cone_surface_area(radius: int, cone_height: int, expected_area: int) -> None:
    question = CircleVolumeQuestion(shape_type=Shape.CONE, radius=radius, height=cone_height)
    assert question.surface_area_in_terms_of_pi() == expected_area


@pytest.mark.parametrize(
    "radius, cylinder_height, expected_area", [(1, 1, 4), (2, 3, 20), (6, 13, 228)],
)
def test_rubber_surface_area(radius: int, cylinder_height: int, expected_area: int) -> None:
    question = CircleVolumeQuestion(shape_type=Shape.CYLINDER, radius=radius, height=cylinder_height)
    assert question.surface_area_in_terms_of_pi() == expected_area


def test_units_radius_shown_as_is() -> None:
    question = CircleVolumeQuestion(radius=11, height=22, units=Units.CENTIMETERS)
    radius_text = question.radius_text(mathjax=False)
    assert "cm" in radius_text


def test_units_radius_in_terms_of_height() -> None:
    question = CircleVolumeQuestion(
        shape_type=Shape.CONE, radius=11, height=22, units=Units.CENTIMETERS, show_radius_as_height=True
    )
    radius_text = question.radius_text(mathjax=False)
    assert "cm" not in radius_text


def test_units_height() -> None:
    question = CircleVolumeQuestion(radius=11, height=22, units=Units.CENTIMETERS)
    height_text = question.height_text(mathjax=False)
    assert "cm" in height_text


def test_units_surface_area() -> None:
    question = CircleVolumeQuestion(
        radius=11, height=22, units=Units.CENTIMETERS, question_type=QuestionType.SURFACE_AREA
    )
    area_text = question.total_value_text(mathjax=False)
    assert "cm^2" in area_text


def test_units_volume() -> None:
    question = CircleVolumeQuestion(radius=11, height=22, units=Units.CENTIMETERS, question_type=QuestionType.VOLUME)
    area_text = question.total_value_text(mathjax=False)
    assert "cm^3" in area_text
