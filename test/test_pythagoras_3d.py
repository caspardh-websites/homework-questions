"""Test Pythagoras 3D."""
import math

import pytest

from questions.pythagoras_3d import (
    Pythagoras3dCuboid,
    Pythagoras3dPyramidHeight,
    CuboidUnknown,
    Pythagoras3dPyramidBase,
)


def test_cuboid_answer_full_diagonal() -> None:
    question = Pythagoras3dCuboid(40, 80, 60, unknown=CuboidUnknown.FULL_DIAGONAL)
    assert question.answer == pytest.approx(107.7, 0.01)


def test_cuboid_answer_half_diagonal() -> None:
    question = Pythagoras3dCuboid(6, 12, 9, unknown=CuboidUnknown.HALF_DIAGONAL)
    assert question.answer == pytest.approx(12.369, 0.01)


def test_pyramid_answer_height() -> None:
    question = Pythagoras3dPyramidHeight(230, 230, 186)
    assert question.answer == pytest.approx(90.255, 0.01)


def test_pyramid_answer_base() -> None:
    question = Pythagoras3dPyramidBase(4, 5)
    assert question.answer == pytest.approx(math.sqrt(18), 0.001)
