import pytest

from questions.sine_triangle_area import SineAreaEquation
from shared.triangle_quantities import Angle, Side


@pytest.mark.parametrize("side_b, side_c", [(10, 10), (1, 50), (25, 40), (1.4, 3.6)])
def test_area_right_angle(side_b: float, side_c: float) -> None:
    knowns = {Angle.A: 90, Side.b: side_b, Side.c: side_c}
    equation = SineAreaEquation(knowns)
    assert equation.area() == 0.5 * side_b * side_c


@pytest.mark.parametrize("angle_a, side_b, side_c, expected", [(45, 1, 2, 0.70711), (100, 7, 3.2, 11.02985)])
def test_area_right_general(angle_a: float, side_b: float, side_c: float, expected: float) -> None:
    knowns = {Angle.A: angle_a, Side.b: side_b, Side.c: side_c}
    equation = SineAreaEquation(knowns)
    assert equation.area() == pytest.approx(expected, 0.0001)
