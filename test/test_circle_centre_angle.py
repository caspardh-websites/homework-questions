"""Test the logic for the circle theorem centre angle."""

import pytest

from questions.ct_centre_angle import CircleTheoremCentreAngle


@pytest.mark.parametrize("point,b,c,expected", [(0, 90, 270, 90), (90, 225, 315, 45)])
def test_edge_angle(point: int, b: int, c: int, expected: int) -> None:
    question = CircleTheoremCentreAngle(point, b, c, CircleTheoremCentreAngle.Unknown.CIRCUMFERENCE)
    assert question.edge_angle == expected


@pytest.mark.parametrize("point,b,c,expected", [(90, 0, 180, 180), (90, 225, 315, 90)])
def test_origin_angle(point: int, b: int, c: int, expected: int) -> None:
    question = CircleTheoremCentreAngle(point, b, c, CircleTheoremCentreAngle.Unknown.CIRCUMFERENCE)
    assert question.origin_angle == expected
