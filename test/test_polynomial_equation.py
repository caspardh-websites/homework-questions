"""Test the polynomial equation function."""
from typing import Tuple

import pytest

from shared.polynomial_equation import PolynomialEquation


@pytest.mark.parametrize(
    "powers, expected",
    (
        ((1,), "1"),
        ((1, 1), "x + 1"),
        ((1, -1), "-x + 1"),
        ((-1, 1), "x - 1"),
        ((0, 1), "x"),
        ((1, 0), "1"),
        ((-1,), "-1"),
        ((1, 1, 1), "x^2 + x + 1"),
        ((1, 2, 3), "3x^2 + 2x + 1"),
        ((-1, 0, -2), "-2x^2 - 1"),
        ((1, 1), "x + 1"),
    ),
)
def test_format_polynomial(powers: Tuple[int, ...], expected: str) -> None:
    poly = PolynomialEquation(powers)
    assert poly.format_readable() == expected


def test_basic_multiply() -> None:
    # This is numpy function, so don't really need to test, but check the formatting still works OK.
    a = PolynomialEquation((1, 1))
    b = PolynomialEquation((2, 3))
    result = a * b
    assert result.format_readable() == "3x^2 + 5x + 2"


def test_random_generation() -> None:
    for _ in range(1000):
        poly = PolynomialEquation.random(0)
        assert len(poly.coef) == 1
        assert poly.coef[0] != 0


@pytest.mark.parametrize(
    "top_coefficients, bottom_coefficients",
    (
        ((1,), (1,)),
        ((1,), (-1,)),
        ((2,), (1,)),
        ((2, 2), (1, 1)),
        ((4, 2), (2, 1)),
        ((20, 15), (5,)),
        ((1, 2, 1), (1, 1)),
        ((2, 6, 6, 2), (2, 2)),
    ),
)
def test_divide_exactly(top_coefficients: Tuple[int, ...], bottom_coefficients: Tuple[int, ...]) -> None:
    top = PolynomialEquation(top_coefficients)
    bottom = PolynomialEquation(bottom_coefficients)
    assert PolynomialEquation.exactly_divide(top, bottom)


@pytest.mark.parametrize(
    "top_coefficients, bottom_coefficients",
    (
        ((1,), (2,)),
        ((2,), (3,)),
        ((2, 3), (1, 1)),
        ((2, 4), (2, 1)),
        ((20, 14), (5,)),
        ((1, 2, 1), (1, -1)),
        ((2, 6, 6, 3), (2, 2)),
    ),
)
def test_dont_divide_exactly(top_coefficients: Tuple[int, ...], bottom_coefficients: Tuple[int, ...]) -> None:
    top = PolynomialEquation(top_coefficients)
    bottom = PolynomialEquation(bottom_coefficients)
    assert not PolynomialEquation.exactly_divide(top, bottom)
