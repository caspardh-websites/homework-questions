"""Test prism volume questions and factory."""
import random
from fractions import Fraction
from typing import Tuple

import pytest

from questions.compound_shapes import CompoundShape, CompoundShapeQuestion


@pytest.mark.parametrize(
    "shape_type, expected_strings, unexpected_strings",
    [
        (CompoundShape.ICE_CREAM, ("cone", "hemisphere"), ("cylinder",)),
        (CompoundShape.UPSIDE_DOWN_ICE_CREAM, ("cone", "hemisphere"), ("cylinder",)),
        (CompoundShape.PENCIL_UP, ("cone", "cylinder"), ("hemisphere",)),
        (CompoundShape.PENCIL_DOWN, ("cone", "cylinder"), ("hemisphere",)),
        (CompoundShape.RUBBER_UP, ("hemisphere", "cylinder"), ("cone",)),
        (CompoundShape.RUBBER_DOWN, ("hemisphere", "cylinder"), ("cone",)),
    ],
)
def test_question_generation(
    shape_type: CompoundShape, expected_strings: Tuple[str], unexpected_strings: Tuple[str]
) -> None:
    question = CompoundShapeQuestion(shape_type=shape_type, radius=1, cone_height=2, cylinder_height=3)
    question_text = question.readable_format(mathjax=False)
    for string in expected_strings:
        assert string in question_text
    for string in unexpected_strings:
        assert string not in question_text


@pytest.mark.parametrize(
    "radius, cone_height, expected_volume", [(1, 1, Fraction(1)), (2, 3, Fraction(28, 3)), (4, 7, Fraction(80))],
)
def test_ice_cream_volume(radius: int, cone_height: int, expected_volume: Fraction) -> None:
    # These two should be identical apart from the drawing so just pick one at random.
    shape_type = random.choice((CompoundShape.ICE_CREAM, CompoundShape.UPSIDE_DOWN_ICE_CREAM))
    question = CompoundShapeQuestion(shape_type=shape_type, radius=radius, cone_height=cone_height)
    assert question.volume_in_terms_of_pi() == expected_volume


@pytest.mark.parametrize(
    "radius, cone_height, cylinder_height, expected_volume",
    [(1, 1, 1, Fraction(4, 3)), (2, 3, 4, Fraction(20)), (4, 2, 5, Fraction(272, 3))],
)
def test_pencil_volume(radius: int, cone_height: int, cylinder_height: int, expected_volume: Fraction) -> None:
    # These two should be identical apart from the drawing so just pick one at random.
    shape_type = random.choice((CompoundShape.PENCIL_UP, CompoundShape.PENCIL_DOWN))
    question = CompoundShapeQuestion(
        shape_type=shape_type, radius=radius, cylinder_height=cylinder_height, cone_height=cone_height
    )
    assert question.volume_in_terms_of_pi() == expected_volume


@pytest.mark.parametrize(
    "radius, cylinder_height, expected_volume",
    [(1, 1, Fraction(5, 3)), (2, 3, Fraction(52, 3)), (6, 6, Fraction(360))],
)
def test_rubber_volume(radius: int, cylinder_height: int, expected_volume: Fraction) -> None:
    # These two should be identical apart from the drawing so just pick one at random.
    shape_type = random.choice((CompoundShape.RUBBER_UP, CompoundShape.RUBBER_DOWN))
    question = CompoundShapeQuestion(shape_type=shape_type, radius=radius, cylinder_height=cylinder_height)
    assert question.volume_in_terms_of_pi() == expected_volume


@pytest.mark.parametrize(
    "radius, cone_height, expected_area", [(3, 4, 33), (4, 3, 52), (5, 12, 115)],
)
def test_ice_cream_surface_area(radius: int, cone_height: int, expected_area: int) -> None:
    # These two should be identical apart from the drawing so just pick one at random.
    shape_type = random.choice((CompoundShape.ICE_CREAM, CompoundShape.UPSIDE_DOWN_ICE_CREAM))
    question = CompoundShapeQuestion(shape_type=shape_type, radius=radius, cone_height=cone_height)
    assert question.surface_area_in_terms_of_pi() == expected_area


@pytest.mark.parametrize(
    "radius, cone_height, cylinder_height, expected_area", [(3, 4, 7, 66), (4, 3, 2, 52), (8, 15, 3, 248)],
)
def test_pencil_surface_area(radius: int, cone_height: int, cylinder_height: int, expected_area: int) -> None:
    # These two should be identical apart from the drawing so just pick one at random.
    shape_type = random.choice((CompoundShape.PENCIL_UP, CompoundShape.PENCIL_DOWN))
    question = CompoundShapeQuestion(
        shape_type=shape_type, radius=radius, cylinder_height=cylinder_height, cone_height=cone_height
    )
    assert question.surface_area_in_terms_of_pi() == expected_area


@pytest.mark.parametrize(
    "radius, cylinder_height, expected_area", [(1, 1, 5), (2, 3, 24), (6, 13, 264)],
)
def test_rubber_surface_area(radius: int, cylinder_height: int, expected_area: int) -> None:
    # These two should be identical apart from the drawing so just pick one at random.
    shape_type = random.choice((CompoundShape.RUBBER_UP, CompoundShape.RUBBER_DOWN))
    question = CompoundShapeQuestion(shape_type=shape_type, radius=radius, cylinder_height=cylinder_height)
    assert question.surface_area_in_terms_of_pi() == expected_area
