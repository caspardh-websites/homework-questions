"""Test the different pieces for fraction algebra."""
from questions.equate_fractions import EquatingFractionsFactory
from questions.adding_fractions import AddingFractionsFactory


def test_equate_factory() -> None:
    factory = EquatingFractionsFactory()
    for _ in range(100):
        factory.generate()


def test_adding_factory() -> None:
    factory = AddingFractionsFactory()
    for _ in range(100):
        factory.generate()
