from questions.simulataneous_equations import SimultaneousEquationFactory, SimultaneousEquation


def test_constrained_coefficients() -> None:
    """Check that the coefficient costraints work."""
    factory = SimultaneousEquationFactory(min_coefficient=5, max_coefficient=5)
    equation = factory.generate()
    assert equation.x1_coefficient == 5
    assert equation.x2_coefficient == 5
    assert equation.y1_coefficient == 5
    assert equation.y2_coefficient == 5


def test_readable_format() -> None:
    """Check it can be formatted as a human would understand."""
    equation = SimultaneousEquation(1, 1, 1, 5, -6, -3)
    assert "x + 5y = 6\n-6x - 3y = -9" in equation.readable_format()


def test_valid_check() -> None:
    """Check we catch the validity cases."""
    equation = SimultaneousEquation(0, 0, 1, 2, 3, 4)
    assert not equation.valid()
    equation.x = 1
    assert equation.valid()
    equation.x = 0
    equation.y = 1
    assert equation.valid()
    equation.x = 2
    assert equation.valid()
    equation.x = 0
    equation.y = 0
    assert not equation.valid()
