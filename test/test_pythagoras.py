"""Test the Pythagoras utility methods."""
import pytest

from shared.pythagoras import find_pythagoras_hypotenuse


@pytest.mark.parametrize("side_a, side_b, expected_side_c", ((3, 4, 5), (4, 3, 5), (5, 12, 13)))
def test_hypotenuse_valid(side_a: int, side_b: int, expected_side_c: int) -> None:
    side_c = find_pythagoras_hypotenuse(side_a, side_b)
    assert side_c == expected_side_c
    assert type(side_c) == int


@pytest.mark.parametrize("side_a, side_b", ((5, 4), (1, 1), (13, 5)))
def test_hypotenuse_invalid(side_a: int, side_b: int) -> None:
    with pytest.raises(ValueError):
        find_pythagoras_hypotenuse(side_a, side_b)
