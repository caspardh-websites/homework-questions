"""Test constructing and generating questions from a question set."""

from question_sets.mixed_question_set import MixedQuestionSet
from questions.cosine_rule import CosineRuleFactory, CosineRuleEquation
from questions.sine_rule import SineRuleFactory, SineRuleEquation


class ExampleMixedSet(MixedQuestionSet):
    def __init__(self) -> None:
        super().__init__()
        self.shuffle = False

    @property
    def title(self) -> str:
        return "Test question set"

    @property
    def shuffled(self) -> bool:
        return self.shuffle


def test_single_question() -> None:
    question_set = ExampleMixedSet()
    question_set.add_question_type(SineRuleFactory(), 5)
    assert len(list(question_set.generate_set())) == 5


def test_shuffling_questions() -> None:
    question_set = ExampleMixedSet()
    # Make the size big enough such that the odds of randomising into the same order is non-existent.
    number_of_each = 50
    question_set.add_question_type(SineRuleFactory(), number_of_each)
    question_set.add_question_type(CosineRuleFactory(), number_of_each)
    # Start by checking that things aren't shuffled.
    for ii, question in enumerate(question_set.generate_set()):
        if isinstance(question, CosineRuleEquation):
            assert ii >= number_of_each, "Found a cosine question in the first half"
        else:
            assert isinstance(question, SineRuleEquation), f"Unexpected question type found {type(question)}"
            assert ii < number_of_each, "Found sine question in the second half"
    # Now turn on shuffling.  Check that everything is indeed jumbled.
    question_set.shuffle = True
    for ii, question in enumerate(question_set.generate_set()):
        assert ii < number_of_each, "Failed to find a non-sine rule in a shuffled set"
        if isinstance(question, CosineRuleEquation):
            break
