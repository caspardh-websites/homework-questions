"""Test prism volume questions."""
from questions.volume_prisms import VolumeCuboid, VolumeTriangularPrism, VolumeCylinder
from shared.units import Units


def test_units_cuboid() -> None:
    question = VolumeCuboid(1, 2, 3, units=Units.CENTIMETERS)
    assert "cm^3" in question.readable_answer()


def test_units_triangular_prism() -> None:
    question = VolumeTriangularPrism(1, 2, 3, units=Units.MILLIMETERS)
    assert "mm^3" in question.readable_answer()


def test_units_cylinder() -> None:
    question = VolumeCylinder(1, 2, units=Units.METERS)
    assert "m^3" in question.readable_answer()
