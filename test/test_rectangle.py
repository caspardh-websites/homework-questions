"""Basic unit tests for the rectangle class."""
import pytest
from point2d import Point2D

from shared.rectangle import Rectangle


def assert_points_equal(point_a: Point2D, point_b: Point2D) -> None:
    assert point_a.x == pytest.approx(point_b.x)
    assert point_a.y == pytest.approx(point_b.y)


def test_creation() -> None:
    Rectangle(Point2D(0, 0), Point2D(100, 100))


def test_corners() -> None:
    rectangle = Rectangle(Point2D(0, 0), Point2D(100, 100))
    assert_points_equal(rectangle.top_left, Point2D(0, 0))
    assert_points_equal(rectangle.top_right, Point2D(100, 0))
    assert_points_equal(rectangle.bottom_left, Point2D(0, 100))
    assert_points_equal(rectangle.bottom_right, Point2D(100, 100))


def test_dimensions() -> None:
    rectangle = Rectangle(Point2D(150, 220), Point2D(30, 70))
    assert rectangle.width == 120
    assert rectangle.height == 150


def test_top_left_square() -> None:
    rectangle = Rectangle(Point2D(50, 150), Point2D(200, 400))
    square = rectangle.top_left_square()
    assert square.width == 150
    assert square.height == 150
    assert_points_equal(rectangle.top_left, square.top_left)
    assert_points_equal(square.bottom_right, Point2D(200, 300))


def test_extents() -> None:
    rectangle = Rectangle(Point2D(1, 90), Point2D(10, 5))
    assert rectangle.extents == [(1, 5), (10, 90)]
