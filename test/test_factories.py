"""Do a basic generation test for each factory."""
import pathlib
from typing import Type

import pytest

from hqapp.pages import SINGLE_QUESTIONS
from shared.base_factory import BaseFactory


@pytest.mark.parametrize("factory_type", SINGLE_QUESTIONS.values())
def test_factory_generation(factory_type: Type[BaseFactory], tmp_path: pathlib.Path) -> None:
    factory = factory_type()
    for question in factory.generate_set(100):
        question.readable_format()
        question.readable_answer()
        if question.has_image:
            path = tmp_path.with_suffix(".png")
            question.generate_image(path)
