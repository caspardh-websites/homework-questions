"""Test the various bits of maths on the cosine rule."""

import pathlib
import tempfile

import pytest

from questions.cosine_rule import CosineRuleEquation
from shared.triangle_quantities import Side, Angle


@pytest.mark.parametrize(
    "opposite_side,side_a,side_b,expected",
    [(5, 5, 5, 60), (1.3, 1.3, 1.3, 60), (5.3, 7.8, 6.2, 42.605), (1425, 1801, 2789, 26.487)],
)
def test_solve_for_angle(opposite_side: float, side_a: float, side_b: float, expected: float) -> None:
    tolerance = 0.01
    assert CosineRuleEquation.solve_for_angle(opposite_side, side_a, side_b) == pytest.approx(expected, abs=tolerance)


@pytest.mark.parametrize(
    "angle_c,side_a,side_b,expected", [(60, 5, 5, 5), (60, 2.3, 2.3, 2.3), (87, 5, 7, 8.387), (56.3, 82, 101, 87.946)],
)
def test_solve_for_side(angle_c: float, side_a: float, side_b: float, expected: float) -> None:
    tolerance = 0.01
    assert CosineRuleEquation.solve_for_side(angle_c, side_a, side_b) == pytest.approx(expected, abs=tolerance)


def test_solve_equilateral_sas():
    knowns = {Side.a: 5, Side.b: 5, Angle.C: 60}
    equation = CosineRuleEquation(knowns, Side.c)
    for angle in Angle:
        assert equation.get_value(angle) == pytest.approx(60)
    for side in Side:
        assert equation.get_value(side) == pytest.approx(5)


def test_solve_equilateral_sss():
    knowns = {Side.a: 5, Side.b: 5, Side.c: 5}
    equation = CosineRuleEquation(knowns, Angle.A)
    for angle in Angle:
        assert equation.get_value(angle) == pytest.approx(60)


def test_solve_random_triangle():
    knowns = {Side.b: 4.3, Side.c: 3.1, Angle.A: 54}
    equation = CosineRuleEquation(knowns, Side.a)
    tolerance = 0.01
    assert equation.get_value(Side.a) == pytest.approx(3.526, abs=tolerance)
    assert equation.get_value(Angle.B) == pytest.approx(80.654, abs=tolerance)
    assert equation.get_value(Angle.C) == pytest.approx(45.346, abs=tolerance)


def test_image_generation():
    knowns = {Side.a: 5, Side.b: 5, Side.c: 5}
    equation = CosineRuleEquation(knowns, Angle.A)
    # We can't actually test that the image is any good, but we can check it doesn't fall over generating one.
    file_path = pathlib.Path(tempfile.NamedTemporaryFile(suffix=".png").name)
    equation.generate_image(file_path)


def test_invalid_triangle() -> None:
    knowns = {Side.a: 5, Side.b: 5, Side.c: 10}
    with pytest.raises(ValueError):
        CosineRuleEquation(knowns, Angle.A)
