"""Test quadratic sequence equations."""

from questions.quadratic_sequences import QuadraticSequence, QuadraticSequenceFactory


def test_readable():
    sequence = QuadraticSequence(1, 0, 0)
    assert "1, 4, 9, 16, 25" in sequence.readable_format()
    sequence = QuadraticSequence(1, 2, 3)
    assert "6, 11, 18, 27, 38" in sequence.readable_format()


def test_generation():
    factory = QuadraticSequenceFactory()
    sequence = factory.generate()
    assert sequence.quadratic_coefficient != 0
    assert abs(sequence.quadratic_coefficient) <= QuadraticSequenceFactory.MAX_COEFFICIENT
    assert abs(sequence.linear_coefficient) <= QuadraticSequenceFactory.MAX_COEFFICIENT
    assert abs(sequence.constant_term) <= QuadraticSequenceFactory.MAX_COEFFICIENT


def test_difficulty_easy() -> None:
    factory = QuadraticSequenceFactory()
    sequence = factory.generate_easy()
    assert sequence.quadratic_coefficient == 1
    assert sequence.linear_coefficient == 0 or sequence.constant_term == 0


def test_difficulty_medium() -> None:
    factory = QuadraticSequenceFactory()
    sequence = factory.generate_medium()
    assert 1 <= sequence.quadratic_coefficient <= 3
