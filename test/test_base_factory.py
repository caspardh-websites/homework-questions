"""Test the base factory methods."""
import pytest

from shared.base_factory import BaseFactory

FIRST_VAR_NAME = "var_one"
FIRST_VAR_VALUES = [1, 2, 3]

SECOND_VAR_NAME = "var_two"
SECOND_VAR_VALUES = ["one", "two", "three", "four", "five"]


@pytest.fixture
def factory_single_variable() -> BaseFactory:
    base_factory = BaseFactory()
    base_factory.avoid_repeating_variable(FIRST_VAR_NAME, FIRST_VAR_VALUES, difficulty_level=None)
    return base_factory


@pytest.fixture
def factory_two_variables(factory_single_variable: BaseFactory) -> BaseFactory:
    factory_single_variable.avoid_repeating_variable(SECOND_VAR_NAME, SECOND_VAR_VALUES, difficulty_level=None)
    return factory_single_variable


def test_single_variable_generation(factory_single_variable: BaseFactory) -> None:
    returned = factory_single_variable.get_next_variables()
    assert isinstance(returned, dict)
    assert len(returned) == 1
    assert FIRST_VAR_NAME in returned
    assert returned[FIRST_VAR_NAME] in FIRST_VAR_VALUES


def test_single_variable_no_repeats(factory_single_variable: BaseFactory) -> None:
    values = [factory_single_variable.get_next_variables()[FIRST_VAR_NAME] for _ in range(len(FIRST_VAR_VALUES))]
    assert len(set(values)) == len(values), f"Found repeats in {values}"
    for value in values:
        assert value in FIRST_VAR_VALUES


def test_multi_cycles(factory_single_variable: BaseFactory) -> None:
    values = [factory_single_variable.get_next_variables()[FIRST_VAR_NAME] for _ in range(1000)]
    assert len(set(values)) == len(FIRST_VAR_VALUES)
    for value in values:
        assert value in FIRST_VAR_VALUES


@pytest.mark.parametrize("number_queries", (max(len(FIRST_VAR_VALUES), len(SECOND_VAR_VALUES)), 1000))
def test_two_variables(factory_two_variables: BaseFactory, number_queries: int) -> None:
    values = [factory_two_variables.get_next_variables() for _ in range(number_queries)]
    var_one_values = [value[FIRST_VAR_NAME] for value in values]
    var_two_values = [value[SECOND_VAR_NAME] for value in values]
    assert len(set(var_one_values)) == len(FIRST_VAR_VALUES)
    assert len(set(var_two_values)) == len(SECOND_VAR_VALUES)
