"""Test circle maths."""

import math
import pytest
from point2d import Point2D

from shared.circle_maths import Circle


@pytest.mark.parametrize(
    "degrees,expected_point", [(0, Point2D(1, 0)), (180, Point2D(-1, 0)), (90, Point2D(0, 1)), (270, Point2D(0, -1))]
)
def test_circle_circumference(degrees: int, expected_point: Point2D) -> None:
    assert_points_equal(Circle.circumference_point(degrees), expected_point)


@pytest.mark.parametrize(
    "a,b,c,angle",
    [
        (0, 0, 0, 0),
        (45, 135, 225, 90),
        (225, 135, 45, 90),
        (135, 45, 315, 90),
        (270, 180, 360, 45),
        (270, 0, 180, 45),
        (270, 360, 180, 45),
        (0, -135, 135, 68),
    ],
)
def test_angle(a: int, b: int, c: int, angle: int) -> None:
    assert Circle.angle(a, b, c) == angle


@pytest.mark.parametrize("a,b,angle", [(0, 0, 0), (0, 90, 90), (0, 270, 90), (45, 180, 135), (180, 45, 135)])
def test_origin_angle(a: int, b: int, angle: int) -> None:
    assert Circle.origin_angle(a, b) == angle


def assert_points_equal(actual: Point2D, expected: Point2D) -> None:
    tolerance = 1e-9
    assert math.isclose(actual.x, expected.x, abs_tol=tolerance)
    assert math.isclose(actual.y, expected.y, abs_tol=tolerance)
