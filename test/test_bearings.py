"""Test the bearings code."""
import pytest

from questions.bearings import BearingsQuestion, Unknown


@pytest.mark.parametrize("bearing, expected", ((15, "015"), (30, "030"), (190, "190"), (215, "215")))
def test_bearing_answer(bearing: int, expected: str) -> None:
    question = BearingsQuestion(angle_degrees=bearing, unknown=Unknown.BEARING)
    assert question.readable_answer(mathjax=False) == expected


@pytest.mark.parametrize("bearing, expected", ((45, 225), (90, 270), (135, 315), (225, 45), (270, 90), (315, 135)))
def test_back_bearing(bearing: int, expected: int) -> None:
    question = BearingsQuestion(angle_degrees=bearing)
    assert question.back_bearing == expected
