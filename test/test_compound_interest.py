"""Basic tests for compound interest."""
import re

from questions.interest_depreciation import CompoundInterestFactory


def test_round_result() -> None:
    factory = CompoundInterestFactory()
    question = factory.generate_easy()
    answer = question.readable_answer()
    regex_match = re.match(r"£\d+\.\d\d", answer)
    assert regex_match is not None, f"Answer failed regex: {answer}"
