"""Test the circle theorem - diameter triangle code."""

from questions.ct_diameter_triangle import CircleTheoremDiameterTriangle


def test_basic_angle_calculation() -> None:
    question = CircleTheoremDiameterTriangle(270, 180, CircleTheoremDiameterTriangle.Unknown.LEFT)
    assert question.left_angle == 45
    assert question.right_angle == 45
