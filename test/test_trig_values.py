"""Test generation of known trig value questions."""
from questions.trig_values import TrigValuesFactory


def test_no_repeats() -> None:
    factory = TrigValuesFactory()
    questions = list(factory.generate_set(20))
    for ii, question in enumerate(questions, start=1):
        assert question not in questions[ii:]


def test_stops_if_would_repeat() -> None:
    """Even if asked for a 1000 questions it will only provide enough for unique values."""
    factory = TrigValuesFactory()
    questions = list(factory.generate_set(2000))
    assert len(questions) < 2000


def test_stops_if_small_number() -> None:
    factory = TrigValuesFactory()
    questions = list(factory.generate_set(3))
    assert len(questions) == 3
