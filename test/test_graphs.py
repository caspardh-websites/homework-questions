"""Basic sanity check of the graphing function."""
import pathlib

import numpy

from images.graph import Graph, TrigConfig


def test_sine_wave(tmp_path: pathlib.Path) -> None:
    graph = Graph(numpy.sin, TrigConfig)
    test_output = tmp_path / "sine_wave.png"
    graph.generate_image(test_output)
    assert test_output.exists()
