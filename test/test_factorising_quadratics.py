"""Test the factorising quadratics code."""

from questions.factorising_quadratics import FactorisingQuadraticFactory


def test_generation() -> None:
    factory = FactorisingQuadraticFactory()
    for _ in range(1000):
        equation = factory.generate()
        # The constant term should never be zero.
        assert equation.expanded.coef[0] != 0


def test_easy_difficulty() -> None:
    factory = FactorisingQuadraticFactory()
    for _ in range(1000):
        equation = factory.generate_easy()
        # The quadratic term should always be 1, and the linear non-zero.
        assert equation.expanded.coef[2] == 1
        assert equation.expanded.coef[1] != 0


def test_medium_difficulty() -> None:
    factory = FactorisingQuadraticFactory()
    for _ in range(1000):
        equation = factory.generate_medium()
        # These should all be of the form (ax)^2 - c
        assert equation.expanded.coef[1] == 0
        assert equation.expanded.coef[0] < 0


def test_hard_difficulty() -> None:
    factory = FactorisingQuadraticFactory()
    for _ in range(1000):
        equation = factory.generate_hard()
        # In the expanded equation the x^2 coefficient must not be 1.
        assert equation.expanded.coef[2] != 1
