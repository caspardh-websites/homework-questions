"""Test the various bits of maths on the sine rule."""

import pathlib
import tempfile

import pytest

from questions.sine_rule import SineRuleEquation
from shared.triangle_quantities import Angle, Side


@pytest.mark.parametrize(
    "angle_a,side_a,angle_b,expected", [(50, 5, 50, 5), (23, 2.5, 23, 2.5), (34, 3, 78, 5.248), (98, 6.7, 23, 2.643)]
)
def test_solve_for_side(angle_a: int, side_a: int, angle_b: int, expected: float) -> None:
    tolerance = 0.01
    assert SineRuleEquation.solve_for_side(angle_a, side_a, angle_b) == pytest.approx(expected, abs=tolerance)


@pytest.mark.parametrize(
    "angle_a,side_a,side_b,expected",
    [(50, 5, 5, 50), (23, 2.3, 2.3, 23), (26, 8.90, 4.7, 13.385), (54, 3.4, 2.2, 31.566)],
)
def test_solve_for_angle(angle_a: int, side_a: int, side_b: int, expected: float) -> None:
    tolerance = 0.01
    assert SineRuleEquation.solve_for_angle(angle_a, side_a, side_b) == pytest.approx(expected, abs=tolerance)


def test_solve_equilateral_aas():
    knowns = {Angle.A: 60, Angle.C: 60, Side.a: 5}
    equation = SineRuleEquation(knowns, Side.c)
    for angle in Angle:
        assert equation.get_value(angle) == pytest.approx(60)
    for side in Side:
        assert equation.get_value(side) == pytest.approx(5)


def test_solve_equilateral_asa():
    knowns = {Angle.A: 60, Angle.C: 60, Side.b: 5}
    equation = SineRuleEquation(knowns, Side.c)
    for angle in Angle:
        assert equation.get_value(angle) == pytest.approx(60)
    for side in Side:
        assert equation.get_value(side) == pytest.approx(5)


def test_solve_equilateral_ssa():
    knowns = {Angle.A: 60, Side.a: 2, Side.c: 2}
    equation = SineRuleEquation(knowns, Side.c)
    for angle in Angle:
        assert equation.get_value(angle) == pytest.approx(60)
    for side in Side:
        assert equation.get_value(side) == pytest.approx(2)


def test_solve_random_triangle():
    knowns = {Angle.A: 54, Angle.C: 58, Side.b: 4}
    equation = SineRuleEquation(knowns, Side.c)
    tolerance = 0.01
    assert equation.get_value(Angle.B) == pytest.approx(68)
    assert equation.get_value(Side.a) == pytest.approx(3.490, abs=tolerance)
    assert equation.get_value(Side.c) == pytest.approx(3.659, abs=tolerance)


def test_image_generation():
    knowns = {Angle.A: 60, Angle.C: 60, Side.b: 5}
    equation = SineRuleEquation(knowns, Side.c)
    # We can't actually test that the image is any good, but we can check it doesn't fall over generating one.
    file_path = pathlib.Path(tempfile.NamedTemporaryFile(suffix=".png").name)
    equation.generate_image(file_path)


def test_invalid_triangle() -> None:
    knowns = {Angle.A: 100, Angle.C: 100, Side.b: 5}
    with pytest.raises(ValueError):
        SineRuleEquation(knowns, Side.c)
