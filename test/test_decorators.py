"""Test the utility decorators."""

from shared.decorators import mathjax


def test_mathjax_plain_str() -> None:
    @mathjax()
    def example_function() -> str:
        return "5"

    output = example_function()
    assert "5" in output
    assert r"\(" in output


def test_mathjax_non_inline_str() -> None:
    @mathjax(inline=False)
    def example_function() -> str:
        return "5"

    output = example_function()
    assert "5" in output
    assert r"\[" in output
    assert r"\)" not in output


def test_mathjax_degrees() -> None:
    @mathjax(degrees=True)
    def example_function() -> int:
        return 50

    output = example_function()
    assert "50" in output
    assert "circ" in output
