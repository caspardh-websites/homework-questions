"""Test the utility functions on the base equation class."""

import pytest

from shared.base_equation import BaseEquation


@pytest.mark.parametrize("test_input,expected", [(5, "5"), (-3, "-3"), (1, ""), (-1, "-"), (1234, "1234")])
def test_format_first_coefficient(test_input: int, expected: str) -> None:
    """Check that we format the first coefficient sensibly."""
    assert BaseEquation.format_first_coefficient(test_input) == expected


@pytest.mark.parametrize("test_input,expected", [(7, " + 7"), (-4, " - 4"), (1, " + "), (-1, " - "), (8765, " + 8765")])
def test_format_second_coefficient(test_input: int, expected: str) -> None:
    """Check that formatting subsequent coefficients works as expected."""
    assert BaseEquation.format_non_first_coefficient(test_input) == expected


def test_format_constant_coefficient():
    """Check that we correctly handle the special case of a constant term."""
    assert BaseEquation.format_first_coefficient(1) == ""
    assert BaseEquation.format_first_coefficient(1, constant_term=True) == "1"
    assert BaseEquation.format_first_coefficient(-1) == "-"
    assert BaseEquation.format_first_coefficient(-1, constant_term=True) == "-1"
    assert BaseEquation.format_non_first_coefficient(1) == " + "
    assert BaseEquation.format_non_first_coefficient(1, constant_term=True) == " + 1"
    assert BaseEquation.format_non_first_coefficient(-1) == " - "
    assert BaseEquation.format_non_first_coefficient(-1, constant_term=True) == " - 1"


@pytest.mark.parametrize(
    "quadratic,linear,constant,equals,expected",
    [
        (1, 1, 1, 0, "x^2 + x + 1 = 0"),
        (-1, -1, -1, 0, "-x^2 - x - 1 = 0"),
        (3, 18, -6, 0, "3x^2 + 18x - 6 = 0"),
        (1, -1, -1, 0, "x^2 - x - 1 = 0"),
        (5, 2, 0, 0, "5x^2 + 2x = 0"),
        (8, 0, 0, 0, "8x^2 = 0"),
        (0, -1, -1, 0, "-x - 1 = 0"),
        (1, 2, 3, 4, "x^2 + 2x + 3 = 4"),
    ],
)
def test_format_single_variable_equation(
    quadratic: int, linear: int, constant: int, equals: int, expected: str
) -> None:
    """Check that formatting subsequent coefficients works as expected."""
    values = [(quadratic, 2), (linear, 1), (constant, 0)]
    assert BaseEquation.format_single_variable_equation(values, equals, False) == expected
