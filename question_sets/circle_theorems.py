"""Standard circle theorem question sets."""

from questions.ct_centre_angle import CircleTheoremCentreAngleFactory
from questions.ct_chord_triangles import CircleTheoremChordTrianglesFactory
from questions.ct_diameter_triangle import CircleTheoremDiameterTriangleFactory
from questions.ct_quad import CircleTheoremQuadrilateralFactory
from question_sets.mixed_question_set import MixedQuestionSet


class CircleTheoremSet(MixedQuestionSet):
    def __init__(self) -> None:
        super().__init__()
        self.add_question_type(CircleTheoremDiameterTriangleFactory(), 3)
        self.add_question_type(CircleTheoremCentreAngleFactory(), 2)
        self.add_question_type(CircleTheoremChordTrianglesFactory(), 3)
        self.add_question_type(CircleTheoremQuadrilateralFactory(), 2)

    @property
    def title(self) -> str:
        return "Basic Circle Theorems"
