"""This pulls together a few questions of different types into one set."""

from dataclasses import dataclass
import random
from typing import List, Iterator

from shared.base_equation import BaseEquation
from shared.base_factory import BaseFactory


@dataclass
class QuestionType(object):
    question_factory: BaseFactory
    number_of_questions: int


class MixedQuestionSet(object):
    def __init__(self) -> None:
        self._question_types = []  # type: List[QuestionType]

    def generate_set(self) -> Iterator[BaseEquation]:
        questions: List[BaseEquation] = []
        for question_type in self._question_types:
            questions.extend(
                question_type.question_factory.generate_set(total_questions=question_type.number_of_questions)
            )
        if self.shuffled:
            random.shuffle(questions)
        yield from questions

    @property
    def title(self) -> str:
        raise NotImplementedError

    @property
    def shuffled(self) -> bool:
        return False

    def add_question_type(self, factory: BaseFactory, number_of_questions: int) -> None:
        self._question_types.append(QuestionType(factory, number_of_questions))
