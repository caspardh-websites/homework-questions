"""Duck typed interface for something which can generate a set of questions at runtime."""

from typing import Iterable, Protocol

from shared.base_equation import BaseEquation


class QuestionSet(Protocol):
    @property
    def title(self) -> str:
        raise NotImplementedError

    def generate_set(self) -> Iterable[BaseEquation]:
        raise NotImplementedError
