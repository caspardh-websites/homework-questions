"""Revision set to cover questions from previous lessons."""
from question_sets.mixed_question_set import MixedQuestionSet
from questions.adding_fractions import AddingFractionsFactory
from questions.completing_square import CompleteSquareFactory
from questions.cosine_rule import CosineRuleFactory
from questions.ct_centre_angle import CircleTheoremCentreAngleFactory
from questions.ct_chord_triangles import CircleTheoremChordTrianglesFactory
from questions.ct_diameter_triangle import CircleTheoremDiameterTriangleFactory
from questions.ct_quad import CircleTheoremQuadrilateralFactory
from questions.equate_fractions import EquatingFractionsFactory
from questions.expanding_brackets import ExpandingBracketsFactory
from questions.graph_recognition import RecogniseGraphFactory
from questions.pythagoras_3d import Pythagoras3dFactory
from questions.quadratic_sequences import QuadraticSequenceFactory
from questions.simulataneous_equations import SimultaneousEquationFactory
from questions.simultaneous_quadratic import SimultaneousQuadraticFactory
from questions.sine_rule import SineRuleFactory
from questions.sine_triangle_area import SineAreaFactory
from questions.volume_prisms import PrismVolumeFactory


class RevisionSet(MixedQuestionSet):
    def __init__(self) -> None:
        super().__init__()
        self.add_question_type(SimultaneousEquationFactory(), 1)
        self.add_question_type(SimultaneousQuadraticFactory(), 1)
        self.add_question_type(CompleteSquareFactory(), 1)
        self.add_question_type(QuadraticSequenceFactory(), 1)
        self.add_question_type(CircleTheoremCentreAngleFactory(), 1)
        self.add_question_type(CircleTheoremDiameterTriangleFactory(), 1)
        self.add_question_type(CircleTheoremChordTrianglesFactory(), 1)
        self.add_question_type(CircleTheoremQuadrilateralFactory(), 1)
        self.add_question_type(SineRuleFactory(), 1)
        self.add_question_type(CosineRuleFactory(), 1)
        self.add_question_type(SineAreaFactory(), 1)
        self.add_question_type(RecogniseGraphFactory(), 1)
        self.add_question_type(Pythagoras3dFactory(), 1)
        self.add_question_type(PrismVolumeFactory(), 1)
        self.add_question_type(ExpandingBracketsFactory(), 1)
        self.add_question_type(EquatingFractionsFactory(), 1)
        self.add_question_type(AddingFractionsFactory(), 1)

    @property
    def title(self) -> str:
        return "Revision Set"

    @property
    def shuffled(self) -> bool:
        return True
