"""Revision set to cover questions from previous lessons."""
from question_sets.mixed_question_set import MixedQuestionSet
from questions.completing_square import CompleteSquareFactory
from questions.quadratic_sequences import QuadraticSequenceFactory


class RevisionSet200729(MixedQuestionSet):
    def __init__(self) -> None:
        super().__init__()
        self.add_question_type(CompleteSquareFactory(), 2)
        self.add_question_type(QuadraticSequenceFactory(), 2)

    @property
    def title(self) -> str:
        return "29th July - Revision Set"
