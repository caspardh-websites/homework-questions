"""Standard circle theorem question sets."""

from questions.cosine_rule import CosineRuleFactory
from questions.sine_rule import SineRuleFactory
from question_sets.mixed_question_set import MixedQuestionSet


class SineCosine(MixedQuestionSet):
    def __init__(self) -> None:
        super().__init__()
        self.add_question_type(SineRuleFactory(), 5)
        self.add_question_type(CosineRuleFactory(), 5)

    @property
    def title(self) -> str:
        return "Sine and Cosine Rule"

    @property
    def shuffled(self) -> bool:
        return True
